import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import CoreuiVue from '@coreui/vue'
import CIcon from '@coreui/icons-vue'
import { iconsSet as icons } from '@/assets/icons'
// import DocsCallout from '@/components/DocsCallout'
// import DocsExample from '@/components/DocsExample'

//Pre-loader
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/css/index.css';


//DataTable
import Vue3EasyDataTable from 'vue3-easy-data-table';
import 'vue3-easy-data-table/dist/style.css';

//Date-Filter
import Datepicker from 'vue3-datepicker'

import SmartTable from 'vuejs-smart-table'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;

import timepicker from 'vue3-time-picker-plugin';
import 'vue3-time-picker-plugin/dist/style.css';

import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';

import { createPinia } from 'pinia';

import mitt from 'mitt';
const emitter = mitt();

const urlApi = "http://casafortuna.com.ve:8041/";

const blockDevs = false;

const app = createApp(App)
app.config.globalProperties.$apiPrueba = 'https://rickandmortyapi.com/api/character/233'
app.config.globalProperties.$apiIdentificador = 'http://localhost:5947/identificador'
app.config.globalProperties.$apiUsuario = urlApi + 'servicelotteryusers/ServicioUsuario.svc/ServicioUsuario/'
app.config.globalProperties.$apiAdministrativo = urlApi + 'servicelotteryadministrative/ServicioAdministrativo.svc/ServicioAdministrativo/'
app.config.globalProperties.$apiGeneral = urlApi + 'servicelotterygeneral/ServicioGeneral.svc/ServicioGeneral/'
app.config.globalProperties.$apiLoteria = urlApi + 'servicelottery/ServicioLoterias.svc/servicelottery/'
app.config.globalProperties.$apiEscrutinio = urlApi + 'servicelotteryscrutiny/ServicioEscrutinio.svc/ServicioEscrutinio/'
app.config.globalProperties.$apiReporte = urlApi + 'servicelotteryreports/ServicioReporte.svc/ServicioReporte/'
app.config.globalProperties.$apiGestionSaldo = urlApi + 'servicelotterymanagerbalance/ServicioGestionSaldo.svc/ServicioGestionSaldo/'
app.config.globalProperties.$blockDevs = blockDevs
app.use(store)
app.use(createPinia())
app.use(router)
app.use(VueLoading)
app.use(CoreuiVue)
app.use(SmartTable)
app.use(VueSweetalert2)
app.use(timepicker)
app.provide('emitter', emitter);
app.provide('icons', icons)
app.component('CIcon', CIcon)
app.component('EasyDataTable', Vue3EasyDataTable);
app.component('Datepicker', Datepicker);
app.component('vSelect', vSelect);
// app.component('DocsCallout', DocsCallout)
// app.component('DocsExample', DocsExample)
app.mount('#app')