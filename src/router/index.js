// import { h, resolveComponent } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import DefaultLayout from '@/layouts/DefaultLayout'
import LayoutLoggedOut from '@/layouts/LayoutLoggedOut'
import { useAuth } from '@/store/auth'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: DefaultLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        meta: { requiresAuth: true },
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard.vue'),
      },
      {
        path: '/loteria',
        name: 'Loteria',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/loteria/loteria.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar este chunk por separado
      },
      {
        path: '/loteria2',
        name: 'Loteria2',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/loteria/loteria.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar este chunk por separado
      },
      // ESQUEMA NEGOCIACION
      {
        path: '/condicion-negociacion',
        name: 'EsquemaNegociacion',
        meta: { requiresAuth: true },
        component: () => import('@/views/esquema-negociacion/esquemaNegociacion.vue'),
      },

      // ESQUEMA NEGOCIACION PROGRAMADA
      {
        path: '/condicion-programada',
        name: 'EsquemaNegociacionProgramada',
        meta: { requiresAuth: true },
        component: () => import('@/views/admin/esquemaN-programada/index.vue'),
      },
      // GRUPO - LISTERO
      {
        path: '/grupo-listero',
        name: 'GrupoListero',
        meta: { requiresAuth: true },
        component: () => import('@/views/grupo-listero/grupoListero.vue'),
      },
      // SUBGRUPO
      {
        path: '/subgrupo',
        name: 'Subgrupo',
        meta: { requiresAuth: true },
        component: () => import('@/views/subgrupo/subGrupo.vue'),
      },
      // PUNTO DE VENTA
      {
        path: '/centro-apuesta',
        name: 'centroApuesta',
        meta: { requiresAuth: true },
        component: () => import('@/views/punto-venta/centroApuesta.vue'),
      },
      // TAQUILLA SIST. ADMINISTRATIVO
      {
        path: '/taquilla-sist-administrativo',
        name: 'TaquillaSistAdministrativo',
        meta: { requiresAuth: true },
        component: () => import('@/views/admin/taquilla-sistema-administrativo/taquillaSistAdministrativo.vue'),
      },
      {
        path: '/ticket-repeat',
        name: 'TicketRepeat',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/ticketRepeat/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/ticket-validate',
        name: 'TicketValidate',
        meta: { requiresAuth: false },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/ticket-validate/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/anular-ticket',
        name: 'anularTicket',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/cancelTicket/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/registrar-resultado',
        name: 'escrutarResultado',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/escrutinio/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/generar',
        name: 'generar',
        meta: { requiresAuth: false },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/generar/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/gestionar-abono',
        name: 'gestionar',
        meta: { requiresAuth: false },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/gestionar-abono/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/venta-por-fecha',
        name: 'ventaPorFecha',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/ventaPorFecha/index.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/result-ticket',
        name: 'resultTicket',
        meta: { requiresAuth: true },
        component: () => import('@/views/resultTicket/index.vue'),
      },
      {
        path: '/abonar',
        name: 'abonar',
        meta: { requiresAuth: false },
        component: () => import('@/views/abonar/index.vue'), //Recordar colocar el requiresAuth en true
      },
      {
        path: '/winner-ticket',
        name: 'winnerTicket',
        meta: { requiresAuth: true },
        component: () => import('@/views/winnerTicket/index.vue'),
      },
      {
        path: '/daily-result',
        name: 'dailyResult',
        meta: { requiresAuth: true },
        component: () => import('@/views/dailyResult/index.vue'),
      },
      {
        path: '/reporte-cuadre-caja',
        name: 'reporteCuadreCaja',
        meta: { requiresAuth: true },
        component: () => import('@/views/reporte-cuadre-caja/index.vue'),
      },
      {
        path: '/reporte-estado-cuenta',
        name: 'reporteEstadoCuenta',
        meta: { requiresAuth: true },
        component: () => import('@/views/reporte-estado-cuenta/index.vue'),
      },
      {
        path: '/reporte-liquidacion',
        name: 'reporteLiquidacion',
        meta: { requiresAuth: true },
        component: () => import('@/views/reporte-liquidacion/index.vue'),
      },
      {
        path: '/reporte-liquidacion-general',
        name: 'reporteLiquidacionGeneral',
        meta: { requiresAuth: true },
        component: () => import('@/views/reporte-liquidacion-general/index.vue'),
      },
      {
        path: '/reporte-resultados',
        name: 'reporteResultados',
        meta: { requiresAuth: true },
        component: () => import('@/views/reporte-resultados/index.vue'),
      },
      {
        path: '/gestion-componente',
        name: 'gestionComponente',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-productos/componente.vue'),
      },
      {
        path: '/gestion-modalidad',
        name: 'gestionModalidad',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-productos/modalidad.vue'),
      },
      {
        path: '/gestion-producto',
        name: 'gestionProducto',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-productos/producto.vue'),
      },
      {
        path: '/gestionar-horario',
        name: 'gestionarHorario',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-sorteo/gestionarHorario.vue'),
      },
      {
        path: '/configurar-sorteo',
        name: 'configurarSorteo',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-sorteo/configurarSorteo.vue'),
      },
      {
        path: '/generar-sorteo',
        name: 'generarSorteo',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-sorteo/generarSorteo.vue'),
      },
      {
        path: '/reporte-ticket',
        name: 'listadoTicketAnulado',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/listado-tickets/listadoTicketAnulado.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/listado-ticket-premiado',
        name: 'listadoTicketPremiado',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/listado-tickets/listadoTicketPremiado.vue'), // quitar --> /* webpackChunkName: "dashboard" */ <-- si se quiere cargar el chunk de dashboard por separado
      },
      {
        path: '/registrar-abono',
        name: 'registrarAbono',
        meta: { requiresAuth: true },
        component: () => import('@/views/registrar-abono/registrarAbono.vue'), 
      },
      //Cambiar clave
      {
        path: '/cambiar-clave',
        name: 'cambiarClave',
        meta: { requiresAuth: false },
        component: () => import('@/views/cambiar-clave/index.vue'), 
      },
      {
        path: 'gestion-usuario',
        name: 'gestionUsuarios',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-usuario/index'), 
      },
      {
        path: 'registrar-movimiento',
        name: 'registrarMovimiento',
        meta: { requiresAuth: true },
        component: () => import('@/views/registrar-movimiento/index'), 
      },
      {
        path: '/anular-ticket-administrativo',
        name: 'anularTicketAdmin',
        meta: { requiresAuth: true },
        component: () => import('@/views/anularTicketAdmin/index.vue'),
      },
      {
        path: '/procesar-movimiento',
        name: 'procesarMovimiento',
        meta: { requiresAuth: true },
        component: () => import('@/views/aprobar-rechazar-movimiento/index.vue'),
      },
      {
        path: '/bloquear-numero',
        name: 'bloquearNumero',
        meta: { requiresAuth: true },
        component: () => import('@/views/bloquear-numero/index.vue'),
      },
      {
        path: '/monitor-venta',
        name: 'monitorVenta',
        meta: { requiresAuth: true },
        component: () => import('@/views/monitor-venta/index.vue'),
      },
      {
        path: '/gestion-grupo',
        name: 'gestionGrupo',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-grupo/index.vue'),
      },
      {
        path: '/configurar-cupos',
        name: 'configurarCupos',
        meta: { requiresAuth: true },
        component: () => import('@/views/configurar-cupos/index.vue'),
      },
      {
        path: '/gestion-usuario-admin',
        name: 'gestionUsuarioAdmin',
        meta: {requiresAuth: true},
        component: () => import('@/views/gestion-usuario/admin/index.vue'),
      },
      {
        path: '/gestion-comprador',
        name: 'gestionComprador',
        meta: { requiresAuth: true },
        component: () => import('@/views/gestion-comprador/index.vue'),
      },
      {
        path: '/buzon-mensajes',
        name: 'buzonMensajes',
        meta: { requiresAuth: true },
        component: () => import('@/views/buzon-mensajes/index.vue'),
      },
    ],
  },
  {
    path: '/pages',
    redirect: '/pages/404',
    name: 'Pages',
    component: LayoutLoggedOut/* {
      render() {
        return h(resolveComponent('router-view'))
      },
    } */,
    children: [
      {
        path: '404',
        name: 'Page404',
        component: () => import('@/views/pages/Page404'),
      },
      {
        path: '500',
        name: 'Page500',
        component: () => import('@/views/pages/Page500'),
      },
      {
        path: 'login',
        name: 'Login',
        component: () => import('@/views/pages/Login'),
      },
      {
        path: 'register',
        name: 'Register',
        component: () => import('@/views/pages/Register'),
      },
      {
        path: 'recuperar-clave',
        name: 'recuperarClave',
        component: () => import('@/views/pages/recuperar-clave/index'), 
      },
    ],
  },
  { path: '/:pathMatch(.*)*', name: 'not-found', redirect: '/' },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

router.beforeEach(async (to, from) => {
  const { auth_usuarioLogueado } = useAuth()
  if (
    to.meta.requiresAuth &&
    !auth_usuarioLogueado &&
    to.name !== 'Login'
  )
    return { name: 'Login' }
  else if (
    auth_usuarioLogueado &&
    ['Login','Register'].includes(to.name)
  )
    return { name: from.name || 'Dashboard' }
})

export default router
