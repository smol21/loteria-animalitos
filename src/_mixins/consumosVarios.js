import { fetchRequest } from "@/_helpers/fetchToolkit";

// cargar comprador comercial
async function loadEsquemas (typeView, pre = '') {
    const self = this;
    const res = await fetchRequest({ 
        api: this.$apiAdministrativo+'ListarEsquemaNegociacion/'+this.auth_tokenUsuario+'/'+typeView, 
        promiseReturn: true, self,
        success: (rjson) => {
            self[pre + (pre ? 'E' : 'e') + 'squemaNegociacionOptions'] = rjson.datos.map(esq => ({ label: window.atob(esq.nombreEsquema), value: esq.codigoEsquema }));
        }, 
        error: async (rjson) => {
            console.error('error: ', rjson);
            self[pre + (pre ? 'E' : 'e') + 'squemaNegociacionOptions'] = [];
            return await self.fireAlertMsg('Hubo problemas al cargar los esquemas de negociación, ¿desea intentarlo de nuevo?', {});
        },
    });
    if (res && res.isConfirmed) await this.loadEsquemas(typeView, pre);
    else return null;
}

// cargar comprador comercial
async function loadListeros () {
    const self = this;
    const res = await fetchRequest({ 
        api: this.$apiAdministrativo+'ListarCompradorComercial/'+this.auth_tokenUsuario, 
        promiseReturn: true, self,
        success: (rjson) => {
            self.listerOptions = rjson.datos.map(lstr => ({ label: window.atob(lstr.nombreComprador), value: lstr.codigoComprador }));
        }, 
        error: async (rjson) => {
            console.error('error: ', rjson);
            self.listerOptions = [];
            return await self.fireAlertMsg('Hubo problemas al cargar los compradores comerciales, ¿desea intentarlo de nuevo?', {});
        },
    });
    if (res && res.isConfirmed) await this.loadListeros();
    else return null;
}

// cargar centros de apuesta
async function loadPuntosVenta () {
    const self = this;
    const res = await fetchRequest({ 
        api: this.$apiAdministrativo+'ListarPuntoVenta/'+this.auth_tokenUsuario, 
        promiseReturn: true, self,
        success: (rjson) => {
            self.betCenterOptions = rjson.datos.map(pv => ({ label: window.atob(pv.nombrePunto), value: pv.codigoPunto }));
        }, 
        error: async (rjson) => {
            console.error('error: ', rjson);
            self.betCenterOptions = [];
            return await self.fireAlertMsg('Hubo problemas al cargar los centros de apuesta, ¿desea intentarlo de nuevo?', { confirmButtonText: 'Reintentar', cancelButtonText: 'Cancelar' });
        },
    });
    if (res && res.isConfirmed) await this.loadPuntosVenta();
    else return null;
}

// detalle esquema de negociacion o subgrupo
async function loadDetail (cod, param) {
    const self = this;
    const apiDet = param === 'esquema' ? 'ConsultarEsquemaNegociacion' : 'ConsultarSubGrupo';
    const res = await fetchRequest({ 
        api: `${this.$apiAdministrativo}${apiDet}/${this.auth_tokenUsuario}/${cod}`,
        promiseReturn: true, self,
        success: (rjson) => rjson.datos, 
        error: async (rjson) => {
            console.error('error: ', rjson);
            const respDet = param === 'esquema' ? 'esquema de negociación' : 'subGrupo';
            return await self.fireAlertMsg(`Hubo problemas al cargar el detalle del ${respDet}, ¿desea intentarlo de nuevo?`, { confirmButtonText: 'Reintentar', cancelButtonText: 'Cancelar' });
        },
    });
    if (res && res.isConfirmed) return this.loadDetail(cod, param);
    else return res;
}

export default {
    methods: {
        loadEsquemas,
        loadListeros,
        loadPuntosVenta,
        loadDetail,
    },
}