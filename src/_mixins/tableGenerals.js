function setTableState (e, tableN = '') {
    this['tableState'+tableN] = e;
    this['selectedOnesFromTable'+tableN] = e.selectedRows; // LISTA DE ITEMS SELECCIONADOS EN LA TABLA (en caso que este activa la seleccion en tabla)
}

// MUSTRA U OCULTA PAGINACION DE LA TABLA DE SORTEOS
function setShowPagination (bool, tableN = '') {
    this['showPagination'+tableN] = bool;
}

// METODO PARA EL ORDENADO DE SORTEOS EN LA TABLA CORRESPONDIENTES, TOMANDO EN CUENTA PRIMERO POR EL NOMBRE DEL SORTEO
function sortingMethods (...sorters) {
    const theSorts = sorters && sorters.length ? {
      ...(sorters.includes('sorteo') ? {
        sorteo: function (row) {
            const zero = row.dateTime.split(':')[0].length < 2 && '0' || '';
            return row.nombreSorteo(zero)
        }
      } : {}),
    } : {};

    return theSorts
}

function setTableDeselected (deselect = true, tableN = '', row = null) {
  const tRef = this.$refs[this['tRef'+tableN]];
  const tRefV = tRef instanceof Array ? tRef : [tRef];
  if (!deselect) tRefV[0].getTableRef().selectAll();
  else if (row) tRefV[0].getTableRef().deselectRow(row);
  else tRefV[0].getTableRef().deselectAll();
}

// METODO PARA ELIMINADO DE ITEMS EN LA TABLA
function removeFromTable (all, item = null, tableN = '', byWhat = 'id') {
    // const tRef = this.$refs[this['tRef'+tableN]];
    // const tRefV = tRef instanceof Array ? tRef : [tRef];
    const byId = item instanceof Array;

    if (all || (byId && this['tableData'+tableN].length == item.length)) {
      if (this.isLoteryView) this.resetIndex();
      this['tableData'+tableN] = [];
      this.setTableDeselected(true, tableN);
      // tRefV[0].getTableRef().deselectAll();
    }
    else {
      const selectedOnesCopy = [...this['tableData'+tableN]];
      if (!byId) {
        const inx = selectedOnesCopy.findIndex(item_ => item_.guid === item.guid);
        if (inx > -1) selectedOnesCopy.splice(inx, 1);
        // console.log(tRefV[0].getTableRef());
        if (!byId) this.setTableDeselected(true, tableN, item);/* tRefV[0].getTableRef().deselectRow(item); */
        this['tableData'+tableN] = [...selectedOnesCopy];
      }
      else { // borrado por actualizacion de sorteos (si byIds -> true, se borra por guid)
        const separator = '/';
        const selectedInds = selectedOnesCopy.reduce((ids, selected, i) => ({...ids, [i+separator+selected[byWhat]]: selected}), {});
        item.forEach(itm => {
          const copySelectedInds = {...selectedInds};
          for (const key_ in copySelectedInds) {
            if (key_.split(separator)[1] === `${itm}`) {
              delete selectedInds[key_];
              if (byWhat !== 'id') break;
            }
          }
        });
        this['tableData'+tableN] = Object.values(selectedInds);
        const doDeselect = !!this['selectedOnesFromTable'+tableN].length;
        if (doDeselect) this.setTableDeselected(true, tableN);/* tRefV[0].getTableRef().deselectAll(); */
      }
      if (this.isLoteryView && !this['tableData'+tableN].length) this.resetIndex();
      const cdt = this['canDelete'+tableN];
      if (!byId && cdt === 'componente') this.deleteComponente(item.guid);
      if (!byId && cdt === 'esquemanegociacion') this.deleteEsquema(item.guid); //MOMENTANEO
      if (!byId && cdt === 'taquillasistadmin') this.deleteTaquillaSistAdmin(item.guid); //MOMENTANEO
    }
    if (this['setOnModify'+tableN] && (this.v$.enGamblingType || this.v$.gamblingType)) (this.v$.enGamblingType || this.v$.gamblingType).$touch(); // para habilitar el modificado en formulario
}

// LLAMA AL METODO DE BORRAR ELEMENTOS DE LA TABLA Y LE PASA LA LISTA DE ELEMENTOS A BORRAR
// ESTO SI EL USUARIO LO CONFIRMA
async function eraseFromTable (data = [], msgs, ask = true, showAl = true, tableN = '', ...funcStruct) {
    const isArr = data instanceof Array;
    if (isArr && !data.length) {
      if (msgs && msgs[4]) this.fireToastMsg(msgs[4], {});
      return;
    }
    const doMulti = this['tableMultidelete'+tableN] && this['selectedOnesFromTable'+tableN].length;
    const msg = isArr ? (doMulti ? msgs[5] : msgs[0]) : msgs[1];
    const result = ask ? await this.fireAlertMsg(msg, {}) : { isConfirmed: true };
    if (result.isConfirmed) {
      if (funcStruct.length) {
        const prms = [...funcStruct];
        const fnct = prms.shift();
        const resp = await this[fnct](...prms);
        if (!resp) return;
      }
      if (isArr || !ask) this['currentPage'+tableN] = 1;
      let dta_;
      if (doMulti) dta_ = this['selectedOnesFromTable'+tableN].map(jug => jug[this.idForMultiElim]);
      this.removeFromTable(isArr && !doMulti, !isArr ? data : dta_, tableN, isArr && doMulti ? this.idForMultiElim : 'id');
      if (showAl) this.fireToastMsg(isArr ? (doMulti ? msgs[6] : msgs[2]) : msgs[3], { icon: 'success' });
    }
}

export default {
    data() {
      return {
        idForMultiElim: 'idJugada',
      }
    },
    // otra data a utilizar aca:
    //    tableData
    //    tableState
    //    selectedOnesFromTable
    //    filters
    //    currentPage
    //    pageSize
    //    showPagination
    //    tableDataLoading
    //    tRef
    //    setOnModify
    //    canDelete
    //    tablesCount
    //    tableMultidelete
    mounted() {
        this.showPagination = this.tableData.length > this.pageSize;
    },
    unmounted() {
      for(let i = 0; i < this.tablesCount; i++) {
        const n = !i ? '' : (i + 1);
        try {
          this['canDelete'+n] = '';
          this['currentPage'+n] = 1;
          this['filters'+n] = {}
          this['pageSize'+n] = 100;
          this['selectedOnesFromTable'+n] = [];
          this['setOnModify'+n] = false
          this['showPagination'+n] = false;
          this['tableData'+n] = []
          this['tableDataLoading'+n] = false
          this['tableState'+n] = null;
          this['tRef'+n] = '';
          this['tableMultidelete'+n] = false; 
        } catch (err) {
          console.error(err)
        }
      }
      this.tablesCount = 0;
    },
    methods: {
        setTableState,
        setShowPagination,
        sortingMethods,
        removeFromTable,
        eraseFromTable,
        setTableDeselected,
    }
}