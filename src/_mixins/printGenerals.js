// PRINT PDF
// import print from 'print-js'
// import pdfMake from "pdfmake/build/pdfmake";
// import pdfFonts from "pdfmake/build/vfs_fonts";
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
// import { saveAs } from 'file-saver';
// import { theBreak } from '@/_helpers/formatters';
// import { fetchRequest } from '@/_helpers/fetchToolkit';
import { WebPrint } from '@/impresion/WebPrint';
import { useLoading } from "vue-loading-overlay";

function webPrintService ({
    seekWebprint = 'Verificando servicio de impresión', 
    extra = '',
    paragraphs = '',
    showUpdating = true,
    serviceRight = () => {},
    serviceWrong = () => {},
}) {
    const self = this;
    const theWebPrint = new WebPrint();
    const $loading = useLoading();

    const loader = $loading.show({
        loader: "dots",
        color: "#1c4c96",
        width: 100,
        height: 100,
    });

    theWebPrint.messages = {
        noWebPrint: /* "No se ha detectado WebPrint" */'La impresora no está disponible'+extra,
        updating: /* "Buscando WebPrint" */seekWebprint,
    };
        
    theWebPrint.settings = {
        discover: true,
        retryCount: 4,
    };

    if (showUpdating) self.fireToastMsg(theWebPrint.messages.updating, { icon: 'info' });
    theWebPrint.getStatus(async function (st) {
        if (st.Status !== '0') {
            if (showUpdating) self.fireToastMsg(st.Description, {});
            serviceWrong(false);
        }
        else {
            if (paragraphs.length) theWebPrint.print(window.encodeURIComponent(paragraphs));
            // console.log(`LO QUE SE IMPRIME${theBreak}`,theBreak+paragraphs);
            serviceRight(true);
        }
        loader.hide();
    });
}

async function checkPrintService (extra = '') {
    const self = this;
    const newPrms = new Promise((resolve) => {
        self.webPrintService({
            extra,
            serviceRight: resolve,
            serviceWrong: resolve
        })
    });
    const prms = await newPrms;
    return prms
}

async function saveAndPrintFile({
    dataPrint = [''],
    // nombreArchivo = 'sistema_de_loteria',
    // api = '',
    ticketPrev = null,
    continua = false,
    // showDownloading = true,
    showPrinting = true,
    shouldAwait = false,
    onLoader = () => {}, 
    onDone = () => {},
}) {
    const self = this;
    onLoader(true);
    const paragraphsStr = dataPrint.join('');

    this.webPrintService({
        seekWebprint: 'Realizando proceso de impresión',
        showUpdating: showPrinting,
        paragraphs: paragraphsStr,
        serviceRight: async () => {
            if (continua) onDone(paragraphsStr);
            else {
                onLoader(false);
                const toast = self.fireToastMsg('Proceso de impresión realizado con éxito', { icon: 'success' });
                if (shouldAwait) await toast;
                onDone(ticketPrev, paragraphsStr);
            }
        },
        serviceWrong: () => {
            onLoader(false);
            onDone();
        }
    });
}

export default {
    data() {
        return {
            printing: false,
        }
    },
    methods: {
        saveAndPrintFile,
        webPrintService,
        checkPrintService,
    },
}

// METODO VIEJO DE IMPRESION
// const api_ = api || 'https://rickandmortyapi.com/api/character/233';
// const blob = new Blob(dataPrint, {type: 'text/plain;charset=utf-8', endings: 'native'});
// const reader = new FileReader();
// reader.onloadstart = () => {
//     if (showDownloading) self.fireToastMsg('Descargando', { icon: 'info' });
// };
// reader.onloadend = async () => {
//     if (continua){
//         onDone(paragraphsStr);
//         loader.hide();
//     } 
//     else {
//         onLoader(false);
//         const toast = self.fireToastMsg('Proceso de impresión realizado con éxito', { icon: 'success' });
//         if (shouldAwait) await toast;
//         loader.hide();
//         onDone(ticketPrev, paragraphsStr);
// }
// };
// saveAs(blob, `${nombreArchivo}.txt`);
// reader.readAsDataURL(blob); 