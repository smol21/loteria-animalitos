function openModal (mode = null) {
    if (mode) this[this.modalPre+'Mode'] = mode;
    this[this.modalPre+'ModalVisible'] = true;
}

function actionFromTable (data, mode = null, param = '') {
    if (!this.modalPre) this.modalPre = param;
    this[this.modalPre+'ModalDta'] = data;
    this.openModal(mode);
}

function closeModal (param = '') {
    this[this.modalPre+'ModalVisible'] = false;
    this.modalDecorator(this, 'modal'+this.modalPre+'Decorator');
    this[this.modalPre+'Mode'] = 'register';
    this[this.modalPre+'ModalDta'] = {};
    if (param && typeof param === 'string') this.modalPre = '';
}

export default {
    // esquemaMode: 'register',
    // esquemaModalVisible: false,
    // esquemaModalDta: {},
    // modalsCount
    unmounted() {
        for(let i = 0; i < this.tablesCount; i++) {
            this[this.modalsCount[i]+'Mode'] = {};
            this[this.modalsCount[i]+'ModalVisible'] = false;
            this[this.modalsCount[i]+'ModalDta'] = {};
            this.modalPre = '';
        }
        this.modalsCount = 0;
    },
    methods: {
        openModal,
        actionFromTable,
        closeModal,
    },
}