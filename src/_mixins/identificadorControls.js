import { caseWrong, fetchRequest } from "@/_helpers/fetchToolkit";

async function identificador () {
    const api = this.$apiIdentificador;
    var urlActual = window.location.href;
    // console.log('asking intentifier'); 
    const self = this;
    fetchRequest({ 
        api,
        mock: 'gralIniciarSesion',
        method: 'POST',
        self,
        notAuthSist: true,
        body: {
          url: urlActual,
        },
        success: (rjson) => {
          const rdatos = rjson;
          // console.log('datos identicador', rdatos.datos.identificador);
          if (rdatos?.datos?.identificador) this.identificadorCode = rdatos.datos.identificador;
        //   console.log('asked'); 
        }, 
        error: (rjson) => {
          this.identificadorCode = '';
        //   console.log('unasked'); 
          return caseWrong({ self, error: rjson.msg || rjson, passMsg: Boolean(rjson.msg), boolError: true, notAlert: true });
        },
    });
}

export default {
    data() {
        return {
            identificadorCode: '',
        }
    },
    computed: {
        identifierOrDefault: function () {
            return window.btoa(this.identificadorCode || '00000000000000000000')
        },
    },
    methods: {
        identificador,
    },
    mounted() {
        this.identificador();
    },
}