import { darkGray, cancelGray } from '@/_helpers/general';
import CIcon from '@coreui/icons-vue';

//Metodos:
// RETORNA REFERENCIA A ELEMENTO DE FORMA GENERICA
function getRef (self, name, arr = true) {
  const destineEl = arr && self.$refs[name] instanceof Array ? self.$refs[name] : [self.$refs[name]];
  if (!destineEl) return null;
  return destineEl[0]?.getInputRef ? destineEl[0]?.getInputRef() : destineEl[0]?.$el
}

// FUNCION PARA SIMULAR ESPERA DE CARGA (TOMA EN CUENTA LA EJECUCION DEL JS EN EL DOM)
function awaiter (time = 1) {
  return new Promise((resolve) => {
    setTimeout(() => {resolve()}, time);
  })
}

/**
 * PASOS PARA IMPLEMENTAR FIX DE MODAL EN LOS COMPONENTES
 * 
 * - EN EL LLAMADO DE COMPONENTE DE MODAL EN FASE ETIQUETA (< />), AGREGAR ref 
    ej:
    <ModalComponentePersonalizado 
      ref="modalComponentePersonalizadoDecorator" <-- (annadir esto)
      ..
    />
 * - EN LAS PARTES DEL CODIGO DONDE SE CIERRE EL MODAL HACER USO DE ESTA FUNCION
    ej:
    ..
    this.modalComponentePersonalizadoVisible = false;
    this.modalDecorator(this,'modalComponentePersonalizadoDecorator'); <-- (annadir esto)
    .. 
 */
async function modalDecorator (self, name) {
  const ref = self.getRef(self, name);
  if (ref) {
    const nxt = ref.nextElementSibling || null;
    if (nxt?.classList) {
      await self.modalTimingExec();
      nxt.classList.add('decorator-modal');
    }
  }
}

// Characters min: 14, max: 160. Seconds min: 2.5, max: 8
function getMsgSeconds (str = '') {
  const len = str.length < 14 ? 14 : str.length > 160 ? 160 : str.length;
  const seconds = (( ( len * 55000 ) + 2880000 ) / 1460);
  return Math.round(seconds)
}

function fireToastMsg (msg, {icon = "warning", asHtml = false}) {
  return this.$swal.fire({
    position: !asHtml ? "top-end" : "center",
    toast: !asHtml,
    icon,
    title: !asHtml ? (msg || '') : '',
    html: !asHtml ? '' : (msg || ''),
    showConfirmButton: false,
    showCloseButton: true,
    customClass: {
      closeButton: 'not-outline',
    },
    timer: this.getMsgSeconds(msg || ''),
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', this.$swal.stopTimer)
      toast.addEventListener('mouseleave', this.$swal.resumeTimer)
    },
  });
}

function fireAlertMsg (
  msg, 
  {
    icon = "question", 
    showConfirmButton = true,
    showCancelButton = true,
    confirmButtonColor = darkGray,
    cancelButtonColor = cancelGray,
    confirmButtonText = "Sí",
    cancelButtonText = "No",
    timer = undefined,
    timerProgressBar = false,
    html = false,
    iconConfirm = "",
    iconCancel = "",
    controlCancel = false,
  }
) {
  const showImg = ['warning','question'].includes(icon);
  const img = showImg ? require(`@/assets/images/${icon == 'warning' ? 'advertencia' : 'interrogacion'}.png`) : '';
  return this.$swal.fire({
    ...(html ? {
      html: msg,
    } : {
      text: msg,
      ...(!['warning','question'].includes(icon) ? { icon } : {
        imageUrl: img,
        imageWidth: 96,
        imageHeight: 88,
        imageAlt: icon,
      }),
    }),
    ...(!html || controlCancel ? {
      showCancelButton,
      cancelButtonColor,
      cancelButtonText,
    } : {}),
    showConfirmButton,
    confirmButtonColor,
    confirmButtonText,
    ...(iconConfirm || iconCancel ? {
      willOpen: () => {
        const { createApp } = require('vue');
        const { iconsSet: icons } = require('@/assets/icons');

        const manageBtnIcon = (theIcon, typeSwal) => {
          const span = createApp(CIcon, {
            icon: theIcon,
          });
          const container = document.createElement('span');
          span.provide('icons', icons)
          span.mount(container);
          const confirmBtn = document.querySelector('.swal2-'+typeSwal);
          confirmBtn.prepend(container, document.createTextNode(' '));
        };
        if (iconConfirm) manageBtnIcon(iconConfirm, 'confirm');
        if (iconCancel) manageBtnIcon(iconCancel, 'cancel');
      },
    } : {}),
    timer,
    timerProgressBar,
  });
}

// EJECUTADO POR LOS INPUTS PARA SABER SI DEJAR TIPEAR EN EL INPUT, Y SI ES TIPO SERIE DEJA TIPEAR ASTERISCOS
function numOnlySpecial (e, value = '', isMonto = false, isFigura = false, isInputInTable = false, shouldShowMensajeError = () => {}) {
  if (e instanceof ClipboardEvent || e.type == 'paste') {
    e.preventDefault();
    return;
  }
  const theCode = e?.keyCode || e?.which || 1;
  if (isInputInTable && theCode === 13 && shouldShowMensajeError) shouldShowMensajeError();
  else {
    const isSerie = !isMonto && this.mode && this.mode == 's';
    const val_ = `${value}`;
    const pass = [...((isSerie && !isFigura && ['*']) || (isFigura && [',']) || ((isMonto || isInputInTable) && ['.'/* ,',' */]) || []), ...Array.from(Array(10), (_, i) => `${i}`)].includes(e.key);
    const prevThis = isSerie ? (val_.length && [val_, e.key].every(v => v.includes('*'))) : /* (isFigura ? [val_, e.key].every(v => v.includes(',')) :  */((isMonto || isInputInTable) && [val_, e.key].every(v => v.includes('.')/*  || v.includes(',') */))/* ) */;
    if (!pass || prevThis) e.preventDefault();
  }
}

// HACE QUE LAS ACCIONES DE  ESCUCHA DEL TECLADO NO SE REALICEN HASTA QUE NO SE 
// CUMPLE EL TIEMPO DE EJECUCION DE TRANSICION DEL MODAL
async function modalTimingExec () {
  const id_ = new Date().getTime() + Math.random();
  this.modalTimingExecuting = {[id_]: true};
  await this.awaiter(this.modalFadeTime);
  if (this.modalTimingExecuting && this.modalTimingExecuting[id_]) 
    this.modalTimingExecuting = null;
}

function onResize (e = null) {
  const width = e?.target?.innerWidth || window.innerWidth || 0;
  this.pageWidthSize = width;
  if (width) this.isMobile = width < 992;
}

function customValidations (validators) {
  return validators.some(val => val.slice(1).includes(this[val[0]]))
}
function customVuelidations (validators) {
  const self = this;
  return validators.some(val => {
    const val_ = val.split('.').reduce((theVal, attr) => theVal[attr], self.v$);
    return val_.$invalid
  })
}

function getOptions (options, optionsObj = null) {
  const returnable = typeof options === 'string' ? (optionsObj ? optionsObj[options] : this[options]) : options;
  return returnable
}

function getModality (sorteo) {
  const modality = /* sorteo.isFigure ? "07" :  *//* sorteo.isAstral ? (sorteo.typeBet.includes('triple') ? (+sorteo.sign ? "05" : "01") : (+sorteo.sign ? "06" : "02")) :  */sorteo.isAB && sorteo.typeBet.includes(' y ') ? "04" : sorteo.typeBet.includes('triple') ? "01" : sorteo.typeBet.includes('cola') ? "02" : "03";
  return modality
}

function awaitFetchDone () {
  const self = this;
  const now = new Date().getTime();
  return new Promise((resolve) => {
    setTimeout(() => {
      const passedDate = new Date().getTime() - now > 20000; // 20 segundos
      if (!self.doingFetch || passedDate) resolve();
    }, 1);
  })
}

export default {
  data() {
    return {
      modalTimingExecuting: null, 
      modalFadeTime: 400,
      isMobile: false,
      pageWidthSize: null,
      doingFetch: false,
    }
  },
  methods: {
    getRef,
    getMsgSeconds,
    fireToastMsg,
    fireAlertMsg,
    numOnlySpecial,
    awaiter,
    modalTimingExec,
    onResize,
    modalDecorator,
    customValidations,
    customVuelidations,
    getOptions,
    getModality,
    awaitFetchDone,
    captureKDown: function (e) {
      if (!this.isMobile && this.execLoteryKeyDown) this.execLoteryKeyDown(e, false, true);
    },
    captureKUp: function (e) {
      if (!this.isMobile && this.execLoteryKeyUp) this.execLoteryKeyUp(e, true);
    },
    nowDate: function () {
      const now = new Date();
      return new Date(now.setMonth(now.getMonth() - 6))
    },
  },
  mounted() {
    window.addEventListener('resize', this.onResize, true);
    this.pageWidthSize = window.innerWidth || 0;
    this.onResize();
    ['keydown','keyup'].forEach((ev, i) =>
      document.addEventListener(
        ev,
        this['captureK'+(!i ? 'Down' : 'Up')],
        false
      )
    );
  },
  updated() {
    window.removeEventListener('resize', this.onResize, true);
    window.addEventListener('resize', this.onResize, true);
    Array(2).fill(['keydown','keyup']).forEach((evs, i) =>
      evs.forEach((ev, j) => 
        document[`${!i ? 'remove' : 'add'}EventListener`](
          ev,
          this['captureK'+(!j ? 'Down' : 'Up')],
          false
        )  
      )
    );
  },
  unmounted() {
    window.removeEventListener('resize', this.onResize, true);
    ['keydown','keyup'].forEach((ev, i) =>
      document.removeEventListener(
        ev,
        this['captureK'+(!i ? 'Down' : 'Up')],
        false
      )
    );
  }
}