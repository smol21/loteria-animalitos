function cleanEsq () {
    // this[(this.cleanEsqPre ? (this.cleanEsqPre+'G') : 'g')+'amblingType'] = '';
    this[(this.cleanEsqPre ? (this.cleanEsqPre+'E') : 'e')+'squemaNegociacion'] = '';
    // this[(this.cleanEsqPre ? (this.cleanEsqPre+'E') : 'e')+'squemaNegociacionOptions'] = [];
}

function addEsquemaNegociacion (e) {
    if (e) e.preventDefault();
    const eN_ = this[(this.cleanEsqPre ? (this.cleanEsqPre+'E') : 'e')+'squemaNegociacion'];
    if (this.tableData.some(esq => esq.id === eN_)) {
      this.fireToastMsg('Este Esquema de Negociación ya se enecuentra añadido en la tabla', { icon: 'warning' });
      this.cleanEsq();
      return;
    }
    const esquemas_ = this[(this.cleanEsqPre ? (this.cleanEsqPre+'E') : 'e')+'squemaNegociacionOptions'];
    const selected = esquemas_.find(esq => esq.value === eN_);
    if (selected) {
      this.tableData = [...this.tableData, { description: selected.label, id: selected.value, guid: selected.value }];
      this.fireToastMsg('se añadió el Esquema de Negociación con éxito.', { icon: 'success' });
      this.cleanEsq();
    } else this.fireToastMsg('Disculpe, no se pudo añadir a la tabla.', { icon: 'warning' });
}

export default {
    data() {
        return {
            cleanEsqPre: '',
        }
    },
    methods: {
        cleanEsq,
        addEsquemaNegociacion,
    },
}