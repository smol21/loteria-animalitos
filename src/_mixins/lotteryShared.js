import { Campo, Sorteo } from "@/_helpers/classes";
import { epoched, hasStar, noStar, nowEpoched, objEvery, objFilter, objForEach, objMap, objOrNotAtobrToDash, objReduce, objSome } from "@/_helpers/formatters";

//#region COMPUTED
/**
 * retorna la cantidad de tipo de secciones
 */
function tipoSeccionesCount () {
    return Object.keys(this.tipoSecciones).length
}

function sorteos () { // editado para nuevo tipo de asociado de sorteos por tipo de seccion
    if (this.showAllOfSorteos) {
      const srts = objReduce(this.tipoSeccionSorteos, (allSrts, tipSec) => [...allSrts, ...(this.tipoSeccionSorteos[tipSec] || [])], []);
      return srts;
    }
    return this.tipoSeccionSeleccionada && this.tipoSeccionSorteos[this.tipoSeccionSeleccionada] || []
}

function sorteosList () {
    const sorteos = this.tipoSeccionSeleccionada && this.sorteosListAll[this.tipoSeccionSeleccionada] || [];
    const srtsHere = this.sorteos.reduce((srts, srt) => ({...srts, [srt.product]: true}), {});
    return sorteos.filter(sorteo => srtsHere[sorteo.code] || sorteo.code == 'todos')
}

function sorteos_por_productos () {
    const sorteos = this.slideActive.reduce((sorteosMatch, slide) => [...sorteosMatch, ...(this.sorteos_filtered_acc[slide] || [])], []);
    return sorteos
}

function sorteos_filtered_acc () {
  return this.make_sorteos_acc(this.sorteosFilteredShared())
}

function sorteos_filtered_acc_all () {
  return this.make_sorteos_acc(this.sorteos)
}

function sorteos_filtered_acc_arr () {
  return this.make_sorteos_arr(this.sorteos_filtered_acc)
}

function sorteos_filtered_acc_arr_all () {
  return this.make_sorteos_arr(this.sorteos_filtered_acc_all)
}

function selected_sorteos () {
  return this.sorteos.filter(srt => srt.checked)
}

function threeway () {
    return this.tipoSeccionesCount > 4 || (this.tipoSeccionesCount > 1 && this.tipoSeccionesCount % 2 == 1)
}

function figureRangeRes () {
    const range = /* this.isMobile ? this.forms.figures.range :  */[this.forms.figures.range[0]];
    return range
}

function visibleFieldsObj () {
    return this.visibleFields.reduce((all, vf) => ({...all, [vf.codigo]: vf}), {})
}

/**
 * retorna los codigos de los campos mostrados en la vista encima de la tabla de apuestas 
 */
function visibleFieldsCodes () {
  return this.visibleFields.reduce((all, vf) => [...all, ...(vf.codigo ? [vf.codigo] : [])], [])
}
//#endregion

//#region METHODS
function dataRequest ({
  self,
  rjson,
  rdatos,
  blockSiders = () => {},
  blockGenerals = () => {},
}) {
  self.sorteoDataLoading = false;
  self.sorteoDataNull = 'data';
  
  if (rdatos.moneda && !self.monedaSeteada) {
    self.setMonedas(rjson.mock ? rdatos.moneda : rdatos.moneda/* monedaStatic.moneda */);
    self.monedaSeteada = true;
  }

  blockSiders();

  self.apiLists = {
    productos: rdatos.productos.reduce((prods, prod) => ({...prods, [prod.codigoProducto]: prod}), {}),
    loterias: rdatos.loterias ? rdatos.loterias.reduce((lots, lot) => ({...lots, [lot.codigoLoteria]: [window.atob(lot.nombreLoteria), window.atob(lot.nombreResumido || lot.nombreLoteria)]}), {}) : {},
    turno: rdatos.turno ? rdatos.turno.reduce((hors, hor) => ({...hors, [window.atob(hor.codigoTurno)]: hor.nombreTurno}), {}) : {},
    tipoFuncion: rdatos.tipoFuncion.reduce((tipsFunc, tipFunc) => ({...tipsFunc, [window.atob(tipFunc.codigoTipoFuncion)]: tipFunc.nombreTipoFuncion}), {}),
    modalidad: rdatos.modalidades.reduce((tipsMod, tipMod) => ({...tipsMod, [window.atob(tipMod.codigoModalidad)]: tipMod}), {}),
    secciones: rdatos.secciones.reduce((secs, sec) => ({...secs, [sec.codigoSeccion]: {nombreSeccion: window.atob(sec.nombreSeccion), nombreResumido: window.atob(sec.nombreResumido), codigoTipoSeccion: window.atob(sec.codigoTipoSeccion)}}), {}),
    tipoSeccion: rdatos.tipoSorteo.reduce((tipSecs, tipSec) => ({...tipSecs, [window.atob(tipSec.codigo)]: tipSec.descripcion}), {}),
  };

  self.tipoSecciones = rdatos.tipoSorteo.reduce((tipoSecs, tipoSec, i) => {
    if (!self.tipoSeccionSeleccionada && !i) self.tipoSeccionSeleccionada = tipoSec.codigo;
    tipoSecs[tipoSec.codigo] = tipoSec.descripcion;
    return tipoSecs
  }, {});

  self.modalidadComponentes = rdatos.componenteModalidad.reduce((modalidades, modalidad) => ({...modalidades, [modalidad.codigoModalidad]: modalidad.componente}), {});

  self.componentesDta = rdatos.componente.reduce((els, el) => ({...els, [el.codigoElemento]: el}), {});

  // objeto con los campos de cada seccion
  const seccionesComponentes = rdatos.componenteSeccion.reduce((camposSeccion, seccion) => ({
    ...camposSeccion,
    [seccion.codigoSeccion]: seccion.elemento.map(el => ({
      ...self.componentesDta[el.codigoElemento],
      ...el
    }))
  }), {});

  // secciones y los codigos de sus elementos
  self.seccionCodigosElementos = objMap(seccionesComponentes, (v) => v.map(e => window.atob(e.codigoElemento)), true);

  const simbolos = rdatos.listsimbolo ? rdatos.listsimbolo.reduce((tiposSimbolo, simbolo) => ({
    ...tiposSimbolo,
    [simbolo.codigoElemento]: simbolo.simbolo.map(simb => ({
        ...simb, 
        urlSimbolo: window.atob(simbolo.urlSimbolo).replace(/\{[^{}]*\}/g, window.atob(simb.codigoSimbolo)).replace(/\s/g, '')
      }))
  }), {}) : {};

  const objTipSecs = (obj) => Object.keys(self.tipoSecciones).reduce((tiposSec, tipoSec) => ({ ...tiposSec, [tipoSec]: obj ? {} : [] }), {});
  const [ seccionCampos/* , seccionCodigoTipoSeccion */ ] = rdatos.secciones.reduce((secCampos, sec) => [
    { // se establecen las secciones
      // ...tiposSecSecs[0][sec.codigoTipoSeccion],
      ...secCampos[0],
      [sec.codigoSeccion]: seccionesComponentes[sec.codigoSeccion] && 
        seccionesComponentes[sec.codigoSeccion].reduce((campos, campo) => ({
          ...campos,
          [window.atob(campo.codigoElemento)]: new Campo({
            codigo: window.atob(campo.codigoElemento),
            tipo: window.atob(campo.tipoElemento) == '01' ? 'input' : 'img',
            etiqueta: window.atob(campo.descripcionEtiqueta),
            obligatorio: Boolean(+campo.obligatorio),
            longitudElemento: +window.atob(campo.longitudElemento) || 0,
            ordenJer: campo.ordenJerarquia,
            dropdown: !self.isBlockedOnesPage ? ((campo.dropdown && window.atob(campo.dropdown)) || '0') : '1',
            permiteEscritura: (campo.permiteEscritura && window.atob(campo.permiteEscritura)) || '0',
            ...(simbolos[campo.codigoElemento] ? { 
              tabla: simbolos[campo.codigoElemento].map(fgr => ({
                codigo: fgr.codigoSimbolo, 
                nombre: /* window.atob( */fgr.nombreSimbolo/* ) */ || '', 
                nombreResumido: /* window.atob( */fgr.nombreResumido/* ) */ || '', 
                img: fgr.urlSimbolo
              })),
              maxSimbolo: window.atob(campo.maximoSimbolo) || '0', // -1: seleccion multiple, 0: sin seleccion, N: cantidad maxima de seleccion, *N estricto
            } : {}),
          })
        }), {}) || 
        {}
    }/* ,
    { // se obtienen los tipos de seccion de cada seccion
      ...tiposSecSecs[1],
      [sec.codigoSeccion]: sec.codigoTipoSeccion
    } */
  ], [ {/* ...objTipSecs(true) */}/* , {} */ ]);

  self.seccionCampos = seccionCampos;

  self.camposTodos = Object.values(seccionCampos).reduce((camposTodos, campos) => ({...camposTodos, ...campos}), {});

  // se asignan sorteos a sus respectivos tipo de seccion, pueden haber sorteos duplicados debido a que combinan secciones
  // se retorna un objeto de sorteos por tipo de seccion para poder suprimir los sorteos duplicados.
  const tipoSeccionSorteos_ = rdatos.sorteos.reduce((tiposSecSorteos, sorteo) => {
    // sorteo.seccion.forEach(codSec => {
      const tSSsCodTipSec = tiposSecSorteos[sorteo.tipoSorteo/* seccionCodigoTipoSeccion[codSec] */];
      if (tSSsCodTipSec) { // se obtienen los sorteos por tipo de seccion
        const sec0 = sorteo.seccion instanceof Array ? sorteo.seccion[0] : sorteo.seccion;
        const idSorteo = objOrNotAtobrToDash(sorteo,'codigoProducto',[sec0 || 'MA=='],'numeroSorteo','codigoTurno');
        const loopSorteo = tSSsCodTipSec[idSorteo];
        // if (loopSorteo) loopSorteo.resetAttr('section', [...loopSorteo.section, codSec]); // no se le agregan las secciones que no correspondan a su tipo de seccion
        tiposSecSorteos[sorteo.tipoSorteo/* seccionCodigoTipoSeccion[codSec] */] = {
          ...tSSsCodTipSec,
          ...(!loopSorteo ? {
            [idSorteo]: new Sorteo({
              id: idSorteo,
              name: sorteo.nombreSorteo,
              sorteoNumber: sorteo.numeroSorteo,
              checked: false,
              dateTime: sorteo.hora,
              codigoTurno: window.atob(sorteo.codigoTurno),
              product: sorteo.codigoProducto,
              allTypes: sorteo.modalidad.reduce((tipsMod, tipMod) => ({
                ...tipsMod, 
                [window.atob(self.isBlockedOnesPage ? tipMod : tipMod.codigoModalidad)]: {
                  ...(self.isBlockedOnesPage ? {montoMinimo: null, montoMultiplo: null} : tipMod.monto.reduce((montos, moneda)=> objMap(montos, (mk) => ({...montos[mk], [window.atob(moneda.codigoMoneda)]: moneda[mk]})), {montoMinimo:{}, montoMultiplo:{}})),
                  nombreModalidad: window.atob(self.apiLists.modalidad[window.atob(self.isBlockedOnesPage ? tipMod : tipMod.codigoModalidad)]?.nombreModalidad || ''),
                  nombreResumido: window.atob(self.apiLists.modalidad[window.atob(self.isBlockedOnesPage ? tipMod : tipMod.codigoModalidad)]?.nombreResumido || ''),
                }}), {}),
              // type: !isFigura ? (sorteo.modalidad.some(tipMod => ["01","04","05"].includes(window.atob(tipMod.codigoModalidad))) ? 'triple' : 'cola') : 'figura',
              // isTerminal: !isFigura && !sorteo.modalidad.some(tipMod => ["01","04","05"].includes(window.atob(tipMod.codigoModalidad))), // Boolean(+sorteo.terminal),
              associated: sorteo.codigoLoteria,
              /* ...(isFigura ? { isFigure: true } : { 
                isAstral: sorteo.modalidad.some(tipMod => ["05","06"].includes(window.atob(tipMod.codigoModalidad))), 
                isAB: sorteo.modalidad.some(tipMod => window.atob(tipMod.codigoModalidad) === "04"),
              }), */
              bet: '',
              asked: '',
              played: '',
              section: sorteo.seccion/* [codSec] */,
              functionType: '',
              sign: '',
              fechacierre: sorteo.fechaCierre || ((new Date().getTime() + 86400000) / 1000), // OR: un dia despues
              currentModality: null,
              tipoSorteo: sorteo.tipoSorteo,
              moneda: (sorteo.moneda || []).map(mon => window.atob(mon)),
              atajo: sorteo.codigoAtajo ? (sorteo.codigoAtajo instanceof Array ? sorteo.codigoAtajo.map(shortCut => window.atob(shortCut)) : [window.atob(sorteo.codigoAtajo)]) : [],
            })
          } : {})
        };
      }
    // });

    return tiposSecSorteos
  }, {...objTipSecs(true)});

  self.sorteosAllByID = {};
  // aca se convierten en array los sorteos de los tipo de seccion
  self.tipoSeccionSorteos = Object.keys(tipoSeccionSorteos_).reduce((tipsSecs, tipSec) => {
    const srtss = tipoSeccionSorteos_[tipSec];
    self.sorteosAllByID = {...self.sorteosAllByID, ...srtss};
    tipsSecs[tipSec] = Object.values(srtss);
    return tipsSecs
  }, {...objTipSecs(false)});

  // se setean los tipos de campos que habran y su model
  self.confForms(true, self.camposTodos, !!self.isBlockedOnesPage);

  blockGenerals();

  self.sorteosListAll = objReduce(self.tipoSeccionSorteos, (sorteosObj, tipSec) => {
    const products = self.tipoSeccionSorteos[tipSec].reduce((products, sorteo) => ({...products, [sorteo.product]: self.apiLists.productos[sorteo.product].nombreProducto}), {});
    sorteosObj[tipSec] = [{code: 'todos', name: 'todos'}, ...objReduce(products, (productsArr, product) => [...productsArr, {code: product, name: window.atob(products[product] || '')}], [])];
    return sorteosObj
  }, {});

  // selecciona como marcados todos los productos
  self.seleccionarTodosProductos();
}

function cambiarTipoSeccion (codigo) {
    const self = this;
    if (!this.sorteoDataLoading && this.tipoSeccionSeleccionada !== codigo) {
      this.tipoSeccionSeleccionada = codigo;
      if (this.$refs?.slidesCarousel && !this.isMobile) {
        if (this.isLoteryView) self.showOneProductOnly = true;
        this.$refs.slidesCarousel.setOverFlowFalse();
        setTimeout(() => {
          self.$refs.slidesCarousel.slideUpdate();
          if (self.isLoteryView) setTimeout(() => { self.showOneProductOnly = false });
        });
      }
      if (this.resetLottery) this.resetLottery(true);
    }
}

function sorteosFilteredShared () {
    const self = this;
    let filtered = this.sorteos.filter(sorteo => {
      if (
        (self.productActive === 'todos' || sorteo.product == self.productActive) &&
        (self.sorteoHorario === '0' || sorteo.codigoTurno == self.sorteoHorario)
      ) return true;
      return false
    });
    return filtered
}

// DESELECCIONA SORTEO(S) SI SE SELECCIONO UNA OPCION DONDE ESTE(OS) YA NO ESTE(N)
function uncheckWhichShould (byMon = false) {
    const self = this;
    let fireAlert;
    const srts_flt_obj = this.sorteos_por_productos.reduce((srtsId, srt) => {
      const monValid = !this.allOfLot || srt.moneda.includes(self.moneda.codigoMoneda);
      if (!fireAlert) fireAlert = byMon && srt.checked && !monValid;
      return ({...srtsId, [srt.id]: !byMon || monValid ? srt.checked : false})
    }, {});
    this.sorteos.forEach(srt => srt.checked = srts_flt_obj[srt.id] || false);
    if (fireAlert) this.fireToastMsg('Se descartaron algunos sorteos porque no admiten la moneda seleccionada' , { icon: 'info' });
}

function setActiveProduct (n) {
    if (!this.slideActive.includes(n)) this.slideActive = [...this.slideActive, n];
    else {
      const ind = this.slideActive.indexOf(n);
      if (ind > -1) this.slideActive.splice(ind, 1);
    }
    this.uncheckWhichShould();
}

function seleccionarTodosProductos (all = true) {
    this.slideActive = !all ? [] : this.sorteos_filtered_acc_arr.slice();
    this.uncheckWhichShould();
}

function refr (f,fV) {
    return 'apuesta'+f.codigo+fV.code;
}

function pointBreaker (manageSideFigures = true, incr = false, wide = false, figure = false, ri = 0) {
    const custm = (n) => figure ? (!ri ? ('custom-nro n'+n) : ('custom-figura n'+n)) : ('custom-3xl n'+n);
    const breakpoints = 
      !wide && this.sidebarVisible && !this.sidebarUnfoldable && manageSideFigures && this.sideFiguresVisible 
        ? (!figure 
          ? `col-md-6 custom-lg-12 custom-xl-${incr ? '6' : '4'} col-xxl-${incr ? '4' : '3'} custom-xxl-${incr ? '6' : '4'} col-xxxl-${incr ? '4' : '3'} ${custm(incr? '1-2' : 1)}` 
        : custm(1)) : // esta abierto el menu y la tabla de figuras
      !wide && this.sidebarVisible && !this.sidebarUnfoldable 
        ? (!figure ? `col-md-6 col-xl-${incr ? '4' : '3'} ${custm(incr? '2-2' : 2)}` 
        : custm(2)) : // esta abierto el menu y NO la tabla de figuras
      !wide && manageSideFigures && this.sideFiguresVisible 
        ? (!figure ? `col-md-${incr ? '4' : '3'} col-lg-6 col-xl-${incr ? '4' : '3'} ${custm(incr? '3-2' : 3)}` 
        : custm(3)) : // NO esta abierto el menu y SI la tabla de figuras
      (!figure ? `col-md-${incr ? '4' : '3'} col-xl-${incr ? '4' : '3'} col-xxl-${incr ? '3' : '2'} ${custm(incr? '4-2' : 4)}` 
      : custm(4)); // No esta abierto el menu y ni la tabla de figuras
    return breakpoints
}

function wronger () {
    objForEach(this.figsCheck, (figFieldK) => this.figsCheck[figFieldK] = false); // para solventar error de codigo no valido en campo de figura
}

function getTablaFigura (fcode, figId = null) {
    if (!fcode) return {};
    const figuras = this.camposTodos[fcode]?.tabla?.length ? this.camposTodos[fcode].tabla.reduce((figs, fig) => ({...figs, [window.atob(fig.codigo)]: fig}), {}) : null;
    if (figId && figuras) return figuras[figId];
    return figuras
}

function clearFiguraDta (fcode) {
    if (fcode) {
      Object.keys(this.form).filter(fkey => ['figura',fcode].every(kw => fkey.includes(kw))).forEach(ffig => this.form[ffig].figura = '');
      this.selectedFigures = {};
    }
}

function formApuestafiguraNro (val, fcode = null) {
    const self = this;
    // self.filterSelected = '';
  
    setTimeout(() => {
      if (val && fcode) {
        const nros = /* !+val ? val : `0${+ */val/* }` */;
        // const figure = self.animalitos && self.animalitos[nro];
        const figures = nros.reduce((all,nr) => [...all, ...(!all.includes(nr) ? [nr] : [])], []).map(nro => self.getTablaFigura(fcode, nro));
        // if (figure?.name) {
        if (figures.every(figure => figure?.nombre)) {
          const fByCode = self.visibleFieldsObj[fcode];
          const maxSimbolo = fByCode?.maxSimbolo;
          self.form['apuesta'+fcode+'figura'].figura = JSON.stringify(figures.map(fig => ({codigo: window.atob(fig.codigo), nombre: fig.nombre, nombreResumido: fig.nombreResumido})));
          if (figures.length >= +noStar(maxSimbolo) && self.checkFocusInput) setTimeout(self.checkFocusInput); // PARA DAR FOCO
          // self.selectedFigures = {...self.selectedFigures, [fcode]: [nro]};
        } else self.clearFiguraDta(fcode);
      } else self.clearFiguraDta(fcode);
    });
}

function setFiguras (id, data, close = false, figurasObj = null, asModal = true, derandomize = false) { 
    this.wronger();
    const self = this;
    let codesArr, isMax, close_ = close;
    const fromNro = Boolean(figurasObj);
    
    if (id && !fromNro) {
      codesArr = JSON.parse(this.codigosFigurasSel || '[]').map(cfs => cfs.codigo);
      if (!codesArr.includes(id)) {
        const max = +noStar(data.fMax);
        if (max) {
          isMax = close_ || max < 0 || (codesArr.length + 1) < max ? 'not' : (codesArr.length + 1) == max ? 'yes' : '';
          if (isMax) {
            codesArr = close_ ? [id] : [...codesArr, id];
            if (isMax == 'yes') close_ = true;
          }
          else this.fireToastMsg(`Se pueden seleccionar ${max} figuras como máximo`, { icon: 'info' });
        }
      }
      else {
        const ind = codesArr.indexOf(id); 
        if (ind > -1) {
          codesArr.splice(ind, 1);
          close_ = false;
        }
      }
    }
    else if (id && fromNro) {
      const ids = id.split(',')
      if (ids.every(id_ => figurasObj[id_])) codesArr = ids;
    }
  
    const len = codesArr && codesArr.length || 0;
    const codesArrFig = len ? codesArr.map(cA => ({codigo: cA, nombre: (figurasObj || this.figurasAhora)[cA].nombre, nombreResumido: (figurasObj || this.figurasAhora)[cA].nombreResumido})) : null;
    this.codigosFigurasSel = codesArrFig ? JSON.stringify(codesArrFig) : '';
    if (fromNro) this.formApuestafiguraNro(codesArr, data.fCode);
    else {
      this.form['apuesta'+data.fCode+'figura'].nro = codesArr ? `${codesArr}` : '';
      this.form['apuesta'+data.fCode+'figura'].figura = `${this.codigosFigurasSel}`;
      // if (codesArrFig) setTimeout(this.checkFocusInput); // PARA DAR FOCO
    }
    if (derandomize) this.randomized = false;
    
    if (close_ && asModal && self.closeFiguresModal) setTimeout(() => {
      self.closeFiguresModal(false);
    });
}

function setFocoDesdeDropdown () {
    this.randomized = false;
    if (this.checkFocusInput) setTimeout(this.checkFocusInput);
}

function maxrGetter (sec) {
  const max = Math.max(...objReduce(this.seccionCampos[sec], (lens, srt) => [...lens, srt.tipo != 'img' ? +srt.longitudElemento : 0], [], true));
  return max
}

async function considerarApuesta (e, sorteo, maxDigits = false, byAllProductsSorteos = false) {
  const selectdSrts = +`${this.selected_sorteos.length}`;
  if (!selectdSrts && this.allOfLot) for (const attr in this.siders) this.siders[attr] = false;
  const self = this;
  // console.log('sorteo', sorteo);
  let canCheck;
  // const { /* combinaSeccion,  */seccion } = this.apiLists.productos[sorteo.product];
  // const seccionCods = seccion.map(sec => sec.codigoSeccion);

  const secsC = this.seccionesCombinables, modsC = this.modalidadesCombinables/* , secs = sorteo.section */;
  // const camposSorteo_ = objReduce(sorteo.allTypes, (all, mod) => {
  //   const campos = self.modalidadComponentes[window.btoa(mod)] || [];
  //   campos.forEach(c => all[window.atob(c.codigoElemento)] = true);
  //   return all
  // }, {});
  // const camposSorteo = Object.keys(camposSorteo_);
  // const camposSeccionSorteo = Object.keys(this.seccionCampos[sorteo.section[0]]);
  // OLD: pregunta si no hay sorteos tildados pasa, si el q se esta tildando no combina seccion los q ya estan tildados deben tener la misma seccion, o si se permite combinar secciones q las del que se va a incluir esten contempladas.
  // si no hay sorteos seleccionados pasa, sino, deben estar la seccion o secciones del sorteo en secciones combinales.
  // canCheck = !secsC.length || /* (!+combinaSeccion &&  */secs.every(sc => secsC.includes(sc)) || secsC.every(sc_ => secs.includes(sc_))/* secs.length == 1 && secsC.length == 1 && secs[0] == secsC[0]) || (+combinaSeccion && ([...seccionCods,...secs].every(s_ => secsC.includes(s_)) || secsC.every(s_ => [...seccionCods,...secs].includes(s_)))) */;
  // pregunta que las modalidades del sorteo esten presentes en la seccion de apuesta
  const passMon = !this.allOfLot || sorteo.moneda.includes(this.moneda.codigoMoneda);
  const canSel = !secsC.length;
  let launchedExp;
  // PARA CHECKEO DE MAX DIGITS
  const maxes = maxDigits ? {
    maxInSecctions: Math.max(...secsC.map(sec => this.maxrGetter(sec))),
    srtMaxDigits: this.maxrGetter(sorteo.section[0])
  } : null;

  if (!passMon) canCheck = false;
  else if (canSel) canCheck = true;
  else {
    const passMod = objEvery(sorteo.allTypes, (atk) => modsC[atk]) || objEvery(modsC, (mck) => sorteo.allTypes[mck]);
    const passMaxDigits = !maxes || maxes.srtMaxDigits == maxes.maxInSecctions;
    // const passCamposMod = camposSorteo.every(cs => this.visibleFieldsCodes.includes(cs)) || this.visibleFieldsCodes.every(vfc => camposSorteo.includes(vfc));
    canCheck = passMod && passMaxDigits/* passCamposMod *//*  && (camposSeccionSorteo.every(css => this.visibleFieldsCodes.includes(css)) || this.visibleFieldsCodes.every(vfc => camposSeccionSorteo.includes(vfc))) */;
  }

  const wasCkecked = Boolean(sorteo.checked);
  sorteo.checked = wasCkecked ? false : canCheck;
  const cantComb = !wasCkecked && !sorteo.checked && !canCheck;
  const multOn0 = !this.isBlockedOnesPage && this.multSelOn[0];
  const targetCheck = !multOn0 && e?.target?.checked;

  const checkAhead = (deselectedOthers = false) => {
    if (targetCheck) {
      if (maxDigits) self.maxDigitsForFieldSelected = Math.max(...[...(!launchedExp ? [maxes.srtMaxDigits] : []), ...(deselectedOthers ? [] : [maxes.maxInSecctions])]);
      e.target.checked = sorteo.checked;
    }
  };

  if (cantComb && (!multOn0 || !this.multSelOn[1])) {
    const checkAnyway = !e ? false : (!this.isThereTyped && !canCheck && passMon);
    if (!passMon || multOn0 || !checkAnyway) {
      launchedExp = true;
      if (!byAllProductsSorteos) 
        this.fireToastMsg(!passMon 
          ? `Moneda no permitida para est${multOn0 ? 'os' : 'e'} sorteo${multOn0 ? 's' : ''}`
          : multOn0 
            ? 'Los sorteos a seleccionar no se pueden combinar'
            : 'Este sorteo no se puede combinar con la selección actual', 
          { icon: 'info' }
        );
    }
    else if (checkAnyway) {
      if (!this.isBlockedOnesPage) this.shorcutCases(89, false, sorteo.id); // deseleccionar sorteos
      else this.sorteos.forEach(srt => { if (!sorteo.id || srt.id != sorteo.id) srt.checked = false });
      await this.awaiter();
      sorteo.checked = true;
      checkAhead(true);
      return;
    }
  }
  if (!this.isBlockedOnesPage) this.multSelOn[1] = cantComb;
  checkAhead();
}

function setearCampoDefault (section = '') {
  if (section) return this.seccionCampos[section];
  this.confForms(false, null, !!this.isBlockedOnesPage);
}

function clearAllForms (self, form, veryFirst = false) {
  if (veryFirst) {
    self.setModeSize('all', 1);
    self.randomized = false;
  } 
  if (form) Object.keys(form).forEach(fkey => { 
    if (typeof form[fkey] == 'string') form[fkey] = ''; 
    if (form[fkey] instanceof Array) form[fkey] = [];
    if (form[fkey]?.codigo) form[fkey] = null;
    else self.clearAllForms(self, form[fkey], false) 
  }) 
}

function getModObj (mod) {
  return this.apiLists.modalidad[mod] || 0
}

function selectedSorteosF (selectedOnes) {
  const self = this;
  if (!this.isBlockedOnesPage) this.changeValsMonto(['montoMinimo', this.maxTicket], ['montoMultiplo', Infinity]);

  if (!selectedOnes.length) {
    if (!this.isBlockedOnesPage) this.focusInput = '';
    this.clearAllForms(this, this.form, true);
    /* if (this.isMobile)  */this.confForms(true, this.camposTodos, !!this.isBlockedOnesPage);
    this.setearCampoDefault();
    this.seccionesCombinables = [];
    this.modalidadesCombinables = {};
    this.seccionesCamposMostrados = [];
  }
  else {
    const {campos: camposChecked, secciones: seccionesOfFields} = selectedOnes.reduce((all, sorteo) => {
      const sorteoCampos = Object.keys(sorteo.allTypes).reduce((flds, mod) => {
        self.modalidadComponentes[window.btoa(mod)].forEach(({codigoElemento}) => {
          const montosSorteoMod = objMap({montoMinimo: null, montoMultiplo: null}, (k) => !this.allOfLot ? Infinity : sorteo.allTypes[mod][k][self.moneda?.codigoMoneda]);
          if (flds[codigoElemento]) {
            const wch = (srt = true, min = true) => (srt ? montosSorteoMod : flds[codigoElemento])[min ? 'montoMinimo' : 'montoMultiplo'];
            const min = wch(), mul = wch(true, false);
            if (min < wch(false))        flds[codigoElemento].montoMinimo =   min; // toma el monto minimo menor
            if (mul < wch(false, false)) flds[codigoElemento].montoMultiplo = mul; // toma el multiplo menor
          }
          else flds[codigoElemento] = montosSorteoMod;
        });
        return flds
      }, {});

      // const secciones = +self.apiLists.productos[sorteo.product].combinaSeccion && self.apiLists.productos[sorteo.product].seccion || sorteo.section;
      self.seccionesCombinables = /* [...secciones, ... */sorteo.section/* ] */.reduce((allSecs, sec) => [...allSecs, ...(!allSecs.includes(sec) ? [sec] : [])], self.seccionesCombinables);
      self.modalidadesCombinables = {...self.modalidadesCombinables, ...objMap(sorteo.allTypes, true)};
      sorteo.section.forEach(s => {
        const fields = objFilter(self.setearCampoDefault(s), (cmp) => sorteoCampos[window.btoa(cmp)], false, false);
        if (fields) objForEach(fields, (f) => {
          const getMin = (allTypes, rv) => objReduce(allTypes, (min, mod) => {
            const mod_ = self.getModObj(mod);
            const ordSel = mod_.ordenSeleccion.trim();
            if (mod_ && self.rgxSiders.test(ordSel)) {
              let [side1, side2] = ordSel.split(',');
              side1 = Math.abs(side1);
              if (!isNaN(Number(side2)) && (!min || side2 - side1 < min)) return side2 - side1;
            }
            return min
          }, rv);
          
          if (!all.secciones[s]) all.secciones[s] = true;
          if (!all.campos[f]) {
            const mode_ = fields[f].tipo == 'input' ? 'x' : 'a';
            const lenP = Object.keys(sorteo.allTypes).reduce((lengthP, mod) => self.getModObj(mod).maximoPermuta > lengthP ? self.getModObj(mod).maximoPermuta : lengthP, self.modeSize.p);
            if (fields[f].longitudElemento > self.modeSize[mode_]) self.setModeSize(mode_, mode_ == 'a' ? -1 : fields[f].longitudElemento);
            self.setModeSize('p', !self.isBlockedOnesPage || self.maximoPermuta >= lenP ? lenP : self.maximoPermuta);
            const toGetMin = self.modeSize['l'] || fields[f].longitudElemento;
            if (mode_ == 'x' && (!self.modeSize['l'] || fields[f].longitudElemento < self.modeSize['l'])) self.setModeSize('l', getMin(sorteo.allTypes, toGetMin), true);
          }
          else { // ya se encuentra el campo, pero se debe decidir que etiqueta utilizar en el label y que longitud del input
            const ordJer = (obj) => obj[f].ordenJer && +window.atob(obj[f].ordenJer) || ''; 
            const [allC, flds] = [ordJer(all.campos), ordJer(fields)];
            const changeLabel = flds && allC ? (flds < allC ? flds : '') : flds;
            const toGetMinElse = self.modeSize['l'] || fields[f].longitudElemento;
            if (fields[f].tipo == 'input' && fields[f].longitudElemento < self.modeSize['l']) self.setModeSize('l', getMin(sorteo.allTypes, toGetMinElse), true);
            all.campos[f] = {
              ...all.campos[f], 
              ...(fields[f].longitudElemento > all.campos[f].longitudElemento ? {longitudElemento: fields[f].longitudElemento} : {}),
              ...(changeLabel ? {etiqueta: fields[f].etiqueta} : {})
            };
          }

          fields[f].resetAttr('utils', sorteoCampos[window.btoa(f)]);
          if (!self.isBlockedOnesPage && ['montoMinimo','montoMultiplo'].some(attr => (fields[f].utils[attr] ?? Infinity) < self.formMonto[attr])) 
            self.changeValsMonto(...([
              ...(fields[f].utils?.montoMinimo   < self.formMonto.montoMinimo   ? [['montoMinimo',   fields[f].utils.montoMinimo]]   : []), 
              ...(fields[f].utils?.montoMultiplo < self.formMonto.montoMultiplo ? [['montoMultiplo', fields[f].utils.montoMultiplo]] : [])
            ]));
          all.campos[f] = fields[f];
        });
      });
      
      return all
    }, {campos:{}, secciones:{}});

    if (Object.keys(camposChecked).length) {
      this.seccionesCamposMostrados = Object.keys(seccionesOfFields);
      this.confForms(false, camposChecked, !!this.isBlockedOnesPage);
      const theresFiguresTable = !self.isBlockedOnesPage && this.visibleFields.some(f => f.tipo == 'img' && f.dropdown == '0');
      if (theresFiguresTable && !this.sideFiguresVisible) this.shorcutCases('FiguresSide');
    } 
    else {
      this.seccionesCombinables = [];
      this.modalidadesCombinables = {};
      this.seccionesCamposMostrados = [];
    }

    // setTimeout(this.checkFocusInput); // PARA DAR FOCO
  }
}

function refreshMeth (all = false) {
  if (all) this.fetchSorteos();
  else this.seleccionarTodosProductos();
  // this.astralShown = 'both';
  this.showRefresh = false;
  // this.slideActive = [0];
}

function resetLottery (complete = false, clearAll = false, refreshAll = false) {
  // if (complete) this.shortcuts.figuresTable = clearAll ? false : !this.shortcuts.figuresTable;
  ['sorteoActive', 'productActive'].forEach(dataItem => {
    const assing = this[dataItem] ?? 'nullUndef';
    if (assing != 'nullUndef') this[dataItem] = 'todos';
  });
  this.selectedFigures = {};
  if (!this.isBlockedOnesPage) {
    this.filterSelected = '';
    if (!complete || clearAll || this.mode === 'figures') for (const attr in this.siders) this.siders[attr] = false;
  }
  if (!this.isBlockedOnesPage && !refreshAll) this.shorcutCases('Refrescar');
  else this.refreshMeth(!this.isBlockedOnesPage);
  this.sorteos.forEach(srt => srt.checked = false);
  // if (!this.shortcuts.figuresTable && this.shortcuts.figuresTableShowed) this.shorcutCases('TablaFiguras');
  this.sorteoHorario = "0";
  if (!this.isBlockedOnesPage && (!complete || clearAll)) {
    this.form.apuestamonto = '';
    // this.astralId = '';
    this.removeFromTable(true);
  }
  // this.slideActive = [0];
}

function aceptarDisabled () {
  // const formsAttr = this.forms[this.mode];
  // const formAttrs = this.visibleFieldsCodes.reduce((vfs, vf) => [...vfs, this.form['apuesta'+vf+formsAttr.code]], []);
  // const formAttr = this.form['apuesta'+formsAttr.code];
  const vals = [
    this.tableDataLoading,
    ...(!this.isBlockedOnesPage ? [ !this.form.apuestamonto.length ] : []),
    // ...(!['figures','c'].includes(this.mode) ? formAttrs.map(fa => !fa.length)
    //   : this.mode === 'c' ? Array.from(Array(2), (_,i) => formAttrs.map(fa => !fa[formsAttr.range[i]].length)) 
    //   : [!Object.keys(this.selected_figures).length]
    // )
    ...(this.form ? [ !objSome(this.form, (fk) => fk != 'apuestamonto' && (typeof this.form[fk] == 'string' ? this.form[fk].length : objEvery(this.form[fk], (sfk) => this.isBlockedOnesPage ? (sfk == 'figura' || this.form[fk][sfk]?.length || this.form[fk][sfk]?.codigo) : (sfk == 'nro' || this.form[fk][sfk].length)))) ] : [])
  ].some(val => val);
  return vals
}

/**
 * 
 * @param {any[] | object} obj 
 * @param  {string[]} keys 
 * 
 * si obj es un array, la primera posision es un objeto y la segunda es un complemento para concaternarselo a las keys
 * asi, si es un array se hace una asignacion de false directa, sino se hace asignacion de false a todos los keys que contengan el key de parametro
 */
function falser (obj, ...keys) {
  keys.forEach(key => {
    const [obj_, code] = obj instanceof Array ? [obj[0], obj[1]] : [obj, null];
    const falseAsignr = (k) => obj_[k] instanceof Array ? [false, false] : false;
    if (code) obj_[key+code] = falseAsignr(key+code);
    else Object.keys(obj_).forEach(k_ => { if (k_.toLowerCase().includes(key)) obj_[k_] = falseAsignr(k_) });
  });
}

function checkListElems (campoRef, code, attr) {
  const fByCode = this.visibleFieldsObj[code];
  let formRefVal_ = this.form[campoRef][attr];
  formRefVal_ = typeof formRefVal_ === 'string' ? formRefVal_ : (formRefVal_?.length 
    ? formRefVal_.reduce((codes, fig) => !codes ? window.atob(fig.codigo) : (codes+','+window.atob(fig.codigo)), '')
    : formRefVal_?.codigo ? window.atob(formRefVal_.codigo) : '');
  if (!fByCode || !formRefVal_) return false;
  // if (!formRefVal_) return 'empty';
  const formRefVal = formRefVal_.split(',').reduce((notRepeated, code) => [...notRepeated, ...(!notRepeated.includes(code) ? [code] : [])], []);
  const theresEmpty = formRefVal.some(cde => !cde);
  const tablaF = this.getTablaFigura(code);
  const figsArr = Object.keys(tablaF);
  const { maxSimbolo, etiqueta } = fByCode;
  const estricto = maxSimbolo.includes('*') || maxSimbolo == '1' ? +noStar(maxSimbolo) : maxSimbolo;

  if (!theresEmpty && typeof estricto == 'number' && formRefVal.length < estricto) {
    this.fireToastMsg(`Se deben seleccionar ${estricto} figuras de ${etiqueta}`, { icon: 'info' });
    return null; 
  }
  else if (!theresEmpty && formRefVal.length > +estricto) {
    const solo = +estricto == 1;
    this.fireToastMsg(`Se puede${solo ? '' : 'n'} seleccionar ${solo ? 'solo una' : estricto} figura${solo ? '' : 's'} de ${etiqueta}${solo ? '' : ' como máximo'}`, { icon: 'info' });
    return null; 
  }
  const theresErr = theresEmpty || (formRefVal?.length && !formRefVal.every(frV => figsArr.includes(frV)));
  return theresErr
}

function camposValidatorFull (paramFigCode = null) {
  const self = this;
  const aplyAll = !this.isBlockedOnesPage;
  const figAttr = this.forms.figures.range[0];
  const rgxFig = new RegExp(`apuesta|${this.forms.figures.code}`,'g');
  const camposFallo = {};
  let activeCFI;

  let simbsFailed; // SE CHEQUEA SI FALLAN LOS SIMBOLOS POR EL CODIGOTIPEADO
  const maxSim = paramFigCode ? this.visibleFieldsObj[paramFigCode].maxSimbolo : null;
  const asRegular = !paramFigCode || hasStar(maxSim);
  Object.keys(this.figsCheck).forEach(formK => {
    const code = formK.replace(rgxFig, '');
    if (code && self.visibleFieldsObj[code]) {
      const val = self.checkListElems(formK, code, figAttr);
      const esNuloOindef = (val && 'paso') ?? 'nulo';
      // const valEmpty = typeof val == 'string' && val == 'empty';
      if (esNuloOindef != 'nulo') self.figsCheck[formK] = /* valEmpty ||  */asRegular || paramFigCode == code ? val : false;
      if (!simbsFailed) simbsFailed = esNuloOindef == 'nulo'/*  || valEmpty */ || val;
      if (simbsFailed && !asRegular && paramFigCode != code) activeCFI = true;
    }
  });
  
  if (activeCFI && this.checkFocusInput) setTimeout(() => {self.checkFocusInput(true)});
  if (simbsFailed) return null;

  // Validaciones campo monto----------------------------------------------------------------------------------------------------
  if (aplyAll) {
    const monto = this.form.apuestamonto.split('.');
    const montoJ = +monto.join('.');
    const prec = this.formMonto.precision;
    const maxLenPrecision = monto[0].length > this.formMonto.maxLen || (monto[1] && monto[1].length > prec),
          minMult = montoJ < this.formMonto.montoMinimo || !!this.modForJS(montoJ, this.formMonto.montoMultiplo)/* this.getTruncated(montoJ % this.formMonto.montoMultiplo, prec) */;

    // const maxTickt = this.maxTicket - this.ticketTotal;
    // const mxT = maxTickt > 0 ? maxTickt : 0;
    // const byMaxMin = montoJ < this.minTicket || montoJ > mxT, 
    const byLenPrecMinMult = maxLenPrecision || minMult;
    if (asRegular) {
      if (/* byMaxMin || byLenPrecMinMult */byLenPrecMinMult) {
        if (!camposFallo['apuestamonto']) camposFallo['apuestamonto'] = true;
        this.formMonto.wrong = true;
        if (this.form.apuestamonto) {
          /* if (!byMaxMin && (maxLenPrecision || minMult))  */this.formMonto.errLabel = maxLenPrecision ? 'precision' : 'minMult';
        }
        else this.formMonto.required = true;
      }
      else this.falser(this.formMonto, 'required', 'wrong');
    }
    else if (byLenPrecMinMult && this.checkFocusInput) {
      this.checkFocusInput(true);
      return null;    
    }
  }
  // ----------------------------------------------------------------------------------------------------------------------------

  // Validaciones campos visibles-----------------------------------------------------------------------------------------------------------------------------------
  const figCode = this.forms.figures.code;
  const thisString = (fk) => typeof this.form[fk] == 'string';
  let msg;

  const [camposApuestaInput, camposApuestaImg] = this.visibleFields.reduce((fields, vf) => {
    const str = (sffx) => `apuesta${vf.codigo}${sffx}`;
    if (vf.tipo == 'img') {
      const isCantOblig = vf.maxSimbolo.includes('*');
      const theNro = self.form[str(figCode)].nro;
      const nroArr = theNro instanceof Array;
      const isFromDropdown = nroArr || theNro?.codigo;
      let figsArr = isFromDropdown ? (nroArr ? theNro.slice() : [{...theNro}]) : JSON.parse(self.form[str(figCode)][figCode] || '[]');
      if (isFromDropdown) figsArr = figsArr.map(fg => ({ codigo: window.atob(fg.codigo), nombre: fg.nombre, nombreResumido: fg.nombreResumido }));
      if (!figsArr.length) figsArr = [""];
      fields[1].push([str(figCode), figsArr, isCantOblig ? '*' : '']);
    }
    else if (self.mode != self.specialCase) fields[0].push([str(self.forms[self.mode].code), [self.form[str(self.forms[self.mode].code)]], vf.longitudElemento]);
    else {
      const objVals = Object.values(self.form[str(self.forms[self.mode].code)]);
      if (!msg && objVals[0] && objVals[1]) {
        if (+objVals[1] <= +objVals[0]) msg = "El campo 'Desde' debe ser superior al campo 'Hasta'.";
        else if (self.isBlockedOnesPage && Math.abs((+objVals[1] + 1) - +objVals[0]) > self.maximoCorrida) msg = `Solo se permite un máximo de ${self.maximoCorrida} apuestas.`;
      }
      fields[0].push([str(self.forms[self.mode].code), objVals, vf.longitudElemento]);
    }
    return  fields
  }, [[], []]);

  const/* let */ min = this.modeSize.l;
  // const mx = this.forms[this.mode].len;
  // const max = this.mode == 'p' ? this.forms.n.len : mx;
  if (camposApuestaInput.length && (!aplyAll || Object.values(this.siders).every(sider => !sider))) {
    // const moreThan2 = max > min;
    const lessThan = camposApuestaInput.some(cDta => {
      // if (moreThan2) min = Math.min(min, ...cDta[1].reduce((vs_, v_) => [...vs_, ...(v_.length ? [v_.length] : [])], []));
      return cDta[1].some(val_ => val_.length && val_.length /* >= */< /* self.modeSize.l */min/*   && (moreThan2 ? (val_.length && val_.length < max) : val_.length == 2) */)
    });
    if (lessThan) {
      // const noSider = !this.sidersTotal;
      // const optionDesc = this.sidersTotal > 1 ? 'punta y/o terminal' : objSome(this.siders, (sk) => sk == 'terminal') ? 'terminal' : 'punta';
      // msg = `${moreThan2 ? `La longitud máxima permitida por los campos es de ${mx} dígitos, ${noSider ? 'n' : 'p'}` : noSider ? 'N' : 'P'}${noSider ? 'o esta permitido' : 'ara poder'} añadir apuestas de solo ${min} dígitos${noSider ? '' : (' debe tildar la opción de '+optionDesc)}.`;
      msg = `No ${aplyAll ? 'está permitido añadir' : 'se permiten'} apuestas inferiores a ${min} dígitos.`;
    }
  }

  const sorteosValidados = [];
  const fallosAplicados = {};
  const passed = !msg ?
    this.selected_sorteos.reduce((sumPassed, srt) => {
      const mods = Object.keys(srt.allTypes);
      let leastOneModSucceed, leastOneIsTyped;

      for (let i = 0; i < mods.length; i++) {
        const campos = self.modalidadComponentes[window.btoa(mods[i])];
        let allModFieldsPassed = true;

        if (campos) campos.forEach(({codigoElemento, obligatorio}) => {
          const oblig = window.atob(obligatorio);
          const f_ = window.atob(codigoElemento);
          const formKeyDta = [...camposApuestaInput, ...camposApuestaImg].find(ca => ca[0].includes(f_));

          if (formKeyDta) {
            const fkd0 = formKeyDta[0], fkd1 = formKeyDta[1];
            const isStr = thisString(fkd0), isFig = fkd0.slice(-6) == figCode;
            const isCorrida = !isStr && !isFig;

            if (fkd1.some(val => isFig ? typeof val == 'string' : (!val.length || val.length < self.modeSize.l))) {
              if (+oblig) allModFieldsPassed = false;
              if (!isFig && (isCorrida ? fkd1.some((val, vi) => (val.length || (!vi ? fkd1[1] : fkd1[0]).length) && val.length < self.modeSize.l) : fkd1.some(val => val.length && val.length < self.modeSize.l))) { // si uno de los campos que falla al menos tiene un digito tipeado se inhabilitara el añadido en tabla
                leastOneModSucceed = false;
                leastOneIsTyped = true;
              }
              const formsCodeGett = (suf, wch) => {
                const sufLC = suf.toLowerCase();
                const bool = (vl, isF = false) => isF ? typeof vl == 'string' : ((wch == 1 && (!vl.length || vl.length < self.modeSize.l)) || (wch == 2 && !vl.length));
                if (isStr && bool(fkd1[0])) {
                  if (!asRegular && sufLC == 'required') activeCFI = true;
                  fallosAplicados[`${self.mode}-${sufLC+f_}`] = true;
                }
                else if (!isStr) fkd1.forEach((val_, ai) => {
                  if (bool(val_, isFig)) {
                    if (!asRegular && sufLC == 'required') activeCFI = true;
                    fallosAplicados[`${isFig ? 'figures' : self.mode}-${'range'+suf+f_}-${fkd1.length == 1 ? /* 1 */'*' : ai}`] = true;
                  }
                });
              };
              if (+oblig || fkd1[0].length){
                if (!camposFallo[fkd0]) camposFallo[fkd0] = f_;
                formsCodeGett('Wrong', 1);
                formsCodeGett('Required', 2);
              }
            }
            else Object.keys(self.forms).forEach(frmK => self.falser([self.forms[frmK], f_], isStr ? 'required' : 'rangeRequired', isStr ? 'wrong' : 'rangeWrong'));
          }
          else if (+oblig) allModFieldsPassed = false;
        });
        if (allModFieldsPassed && !leastOneIsTyped) leastOneModSucceed = true;
      }

      if (leastOneModSucceed) {
        sorteosValidados.push(srt);
        return true;
      }
      return sumPassed;
    }, false) : null;
  // ---------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (activeCFI && self.checkFocusInput) {
    self.checkFocusInput(true);
    return null;    
  }

  if (!passed) Object.keys(fallosAplicados).forEach(fallo => {
    const dtaArr = fallo.split('-');
    if (dtaArr.length < 3) self.forms[dtaArr[0]][dtaArr[1]] = true;
    else {
      if (dtaArr[2] != '*') self.forms[dtaArr[0]][dtaArr[1]][dtaArr[2]] = true;
      else self.forms[dtaArr[0]][dtaArr[1]] = [true, true];
    }
  });

  // console.log('camposFallo', camposFallo);
  if ((!passed && Object.keys(camposFallo).length) || camposFallo['apuestamonto']) return null;
  else if (msg) {
    this.fireToastMsg(msg, {});
    return null;
  }

  // const camposFallidos = Object.values(camposFallo);
  // console.log('Campos que fallaron', camposFallidos);

  const rgxRplce = (attrName, isFig = false) => {
    const rgx = new RegExp(`${isFig ? figCode : self.forms[self.mode].code}`);
    return attrName.slice(7).replace(rgx, '');
  };

  const camposValidados = {
    ...camposApuestaInput.reduce((camposLlenos, c) => ({...camposLlenos, ...(c[1][0] ? {[rgxRplce(c[0])]: (self.mode == 'p' ? [c[1], c[2]] : c[1])} : {})}), {}),
    ...camposApuestaImg.reduce((camposLlenos, c) => ({...camposLlenos, ...(c[1][0] ? {['*'+c[2]+rgxRplce(c[0], true)]: c[1]} : {})}), {})
  };
  // console.log('CAMPOS TODOS', camposValidados);

  const apuestas = objReduce(camposValidados, (apuestas, cmp) => {
    const star = cmp.includes('*');
    if (self.mode != 'figures' || star) {
      const isP = self.mode == 'p', cmpVald = camposValidados[cmp];
      if (!aplyAll) apuestas[cmp] = isP && !star ? cmpVald[0] : cmpVald; 
      else apuestas[cmp] = self.mode == 'n' || star ? cmpVald : 
        isP ? self.getPermutations(...cmpVald[0], cmpVald[1] < 3 ? 2 : 3) :
        self[self.mode == 's' ? 'getSerie' : 'getUntil'](...cmpVald);
    }
    return apuestas
  }, {});

  const {sorteosYaValidados, falloPorFecha} = sorteosValidados.reduce((data, sorteo) => {
    if (self.notPassed(sorteo)) data.sorteosYaValidados.push(sorteo);
    else if (!data.falloPorFecha) data.falloPorFecha = true;
    return data
  }, {sorteosYaValidados: [], falloPorFecha: false});

  // console.log('sorteos validados', sorteosYaValidados);
  
  const toastMsg = !aplyAll ? '' : falloPorFecha ? `${sorteosYaValidados.length ? 'Algunos' : 'Todos los'} sorteos fueron descartados por superar su fecha de cierre` : 
    !sorteosYaValidados.length ? 'Todos los sorteos fueron descartados' : 
    sorteosYaValidados.length < this.selectedOnesUse ? 'Algunos sorteos fueron descartados' : '';

  const yesBets = !aplyAll || objSome(apuestas, (v) => v && v.length, true);
  if (!yesBets) {
    this.fireToastMsg('Debe realizar al menos una apuesta', { icon: 'info' });
    return null;
  }

  return { apuestas, sorteosYaValidados, toastMsg, falloPorFecha }
}

function make_sorteos_acc (theSorteos) {
  const asObj = theSorteos.reduce((sorteos, sorteo) => ({...sorteos, [sorteo.product]: [...(sorteos[sorteo.product] || []), sorteo]}), {});
  return asObj
}

function make_sorteos_arr (sorteos_filtered) {
  const asArr = Object.keys(sorteos_filtered)
    .sort((a, b) => {
      const a_ = this.apiLists.productos[a];
      const b_ = this.apiLists.productos[b];
      const ordProdA = a_ && a_.ordenProducto ? window.atob(a_.ordenProducto) : 0;
      const ordProdB = b_ && b_.ordenProducto ? window.atob(b_.ordenProducto) : 0;
      return !a_ ? 1 : !b_ ? -1 : +ordProdA < +ordProdB ? -1 : 1
    });
  return asArr
}
//#endregion

//#region WATCHERS
function selected_sorteosW (selectedOnes) {
  this.selectedSorteosF(selectedOnes);
}

function modeW () {
  const self = this;
  this.clearAllForms(this, this.form); // NUEVO
  if (!this.isBlockedOnesPage) setTimeout(() => self.captureKeyDown(), 1);
}

function visibleFieldsW (vfs) {
  const toN = vfs.some(f => !f.codigo);
  this.mode = vfs.some(f => f.tipo == 'input') ? (toN || this.mode == 'figures' ? 'n' : this.mode) : 'figures';
}
//#endregion

export default {
  data() {
    return {
        doingMultiSelection: false,
        allOfLot: true,
        isBlockedOnesPage: false,
        showAllOfSorteos: false,
        sorteoDataLoading: false,
        randomized: false,
        monedaSeteada: false,
        tipoSeccionSeleccionada: '',
        codigosFigurasSel: '',
        sorteosListAll: {},
        figurasAhora: {},
        modalidadComponentes: {},
        seccionCodigosElementos: {},
        selectedFigures: {}, // Figuras seleccionadas, filtradas por codigo de campo
        productActive: 'todos', // FILTRO ACTUAL PARA LISTADO DE SORTEOS DISPONIBLES
        sorteoHorario: "0"/* 'd' */, // FILTRO DE HORARIO PARA LISTADO DE SORTEOS DISPONIBLES
        mode: 'n', // 'n', 's', 'p', 'c', o 'figures' --> INDICA EL MODO DE SELECCION ACTUAL
        sorteoDataNull: 'data',
        sorteosListMsg: 'No hay sorteos disponibles.',
        noProductsSelectedMsg: 'Por favor seleccione al menos un producto.',
        slideActive: [], // slides del carrusel de productos
        seccionCampos: {},
        camposTodos: {},
        componentesDta: {},
        seccionesCombinables: [],
        modalidadesCombinables: {},
        seccionesCamposMostrados: [], // secciones con representacion de componentes en el area de apuesta
        showRefresh: false,
        apiLists: {
            productos: {},
            loterias: {},
            turno: {},
            tipoFuncion: {},
            modalidad: {},
            secciones: {},
            tipoSeccion: {},
        },
        /** tipoSecciones:
         * { 
         *    tipoSeccion1: nombreTipoSeccion1, 
         *    ... , 
         *    tipoSeccionN: nombreTipoSeccionN 
         * }
         * son los tipos de secciones, se puede filtrar los sorteos por tipo de seccion
         * tambien se almacenan las secciones asociadas
         */
        tipoSecciones: {},
        /** tipoSeccionSorteos: (filtrado por tipo de seccion)
         * { 
         *    tipoSeccion1: [ sorteo1, ... , sorteoN ], 
         *    ... , 
         *    tipoSeccionN: [...]
         * }
         * se almacenan los sorteos segun su tipo de seccion
         */
        tipoSeccionSorteos: {},
        sorteosAllByID: {},
        maxDigitsForFieldSelected: 0,
    }
  },
  computed: {
    tipoSeccionesCount,
    sorteosList,
    sorteos,
    sorteos_por_productos,
    sorteos_filtered_acc,
    sorteos_filtered_acc_all,
    sorteos_filtered_acc_arr,
    sorteos_filtered_acc_arr_all,
    selected_sorteos,
    threeway,
    figureRangeRes,
    visibleFieldsObj,
    visibleFieldsCodes,
    isThereTyped: function () {
      const theresTyped = objSome(this.form, (fv) => {
        const valOf = typeof fv == 'string' ? !!fv : fv;
        const val = typeof valOf == 'boolean' ? valOf : objSome(valOf, (subValVal) => !!(subValVal instanceof Array ? subValVal.length : subValVal), true);
        return val
      }, true);
      return theresTyped
    },
    valForInfoText: function () {
      return (this.mode == 'p' && this.maximoPermuta < Infinity) || (this.mode == 'c' && this.maximoCorrida < Infinity)
    },
    sameSorteosSelected: function () {
      const ss = this.selected_sorteos;
      const differs = ss.some(s=> s.product !== ss[0].product);
      return !differs
    },
  },
  methods: {
    dataRequest,
    cambiarTipoSeccion,
    uncheckWhichShould,
    setActiveProduct,
    sorteosFilteredShared,
    seleccionarTodosProductos,
    refr,
    wronger,
    pointBreaker,
    getTablaFigura,
    setFiguras,
    formApuestafiguraNro,
    clearFiguraDta,
    setFocoDesdeDropdown,
    maxrGetter,
    considerarApuesta,
    selectedSorteosF,
    clearAllForms,
    setearCampoDefault,
    getModObj,
    refreshMeth,
    resetLottery,
    aceptarDisabled,
    falser,
    camposValidatorFull,
    notPassed: function (sorteo) {
      return nowEpoched() < epoched(sorteo.fechacierre)
    },
    checkListElems,
    make_sorteos_acc,
    make_sorteos_arr,
  },
  watch: {
    selected_sorteos: selected_sorteosW,
    mode: modeW,
    visibleFields: visibleFieldsW,
  },
}