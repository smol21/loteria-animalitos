import { LABELS as labels } from "@/_helpers/labels";

async function callFiller (evnt, callNext = 'fillPOSData') {
    if (evnt instanceof Array && evnt[0] === 'fillSelect') {
      if (evnt[1]) {
        const ev1 = evnt[1] !== 'none' ? evnt[1] : '';
        let geoLabels = evnt[2].map(evn2 => evn2.toLowerCase().includes('cities') ? 'City' : 'Municipality');
        if (!ev1) geoLabels = geoLabels.map(geoL => geoL.toLowerCase());
        geoLabels.forEach(geo => this[ev1 + geo] = '')
      }
      this.cleanGeo(...evnt[2]);
      if (evnt[3] !== 'seleccione') {
        const msgs = await Promise.allSettled(evnt[2].map(async (eachE) => await this.setGeo(eachE, evnt[3])));
        if (this.doingFetch) this.doingFetch = false;
        const msgs_ = msgs.filter(messag => messag.value[0]);
        const [labls, cods] = [
          msgs_.map((itm1, i) => (i ? ' ' : '') + itm1.value[0]), 
          msgs_.map(itm2 => itm2.value[1])
        ];
        if (msgs_.length) {
          const res = await this.fireAlertMsg(
            labels.defaultErrorMsgPlus(('obtener la información de' + (msgs_.length > 1 ? ': ' : ' ') + labls), false, ' ¿Quiere intentarlo de nuevo?'), 
          {});
          if (res.isConfirmed) this.callFiller([evnt[0], evnt[1], cods, evnt[3]]);
        }
      }
    }
    else if (!this.isMobile && this[callNext]) this[callNext](null, evnt);
}

async function consultState () {
    const stateMsg = await this.setGeo('states'); // almacena los Estados para uso global
    if (this.doingFetch) this.doingFetch = false;
    const labl = stateMsg[0];
    if (labl) {
      const resAlert = await this.fireAlertMsg(
        labels.defaultErrorMsgPlus(('obtener la información de ' + labl), false, ' ¿Quiere intentarlo de nuevo?'),
      {});
      
      if (resAlert.isConfirmed) this.consultState();
    }
}

export default {
    data() {
      return {
        geoHas2: false,
      }
    },
    methods: {
        callFiller,
        consultState,
    },
    mounted: async function () {
        await this.consultState();
    },
    unmounted () {
        this.cleanGeoAll(this.geoHas2);
    }
  }