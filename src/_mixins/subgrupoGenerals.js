function cleanSubG (cleanOptions = true) {
    this.sgSubGrupo = '';
    if (cleanOptions) this.sgSubGrupoOptions = [];
}

function addSubGrupo (e) {
    if (e) e.preventDefault();
    const sG_ = this.sgSubGrupo;
    if (this.tableData2.some(subg => subg.rif === sG_)) {
      this.fireToastMsg('Este SubGrupo ya se enecuentra añadido en la tabla', { icon: 'warning' });
      this.cleanSubG(false);
      return;
    }
    const subgrupos_ = this.sgSubGrupoOptions;
    const selected = subgrupos_.find(subg => subg.value === sG_);
    if (selected) {
      this.tableData2 = [...this.tableData2, { name: selected.label, rif: selected.value, guid: selected.value }];
      this.fireToastMsg('se añadió el SubGrupo con éxito.', { icon: 'success' });
      this.cleanSubG(false);
    } else this.fireToastMsg('Disculpe, no se pudo añadir a la tabla.', { icon: 'warning' });
}

export default {
    // data() {
    //     return {
    //         cleanEsqPre: '',
    //     }
    // },
    methods: {
        cleanSubG,
        addSubGrupo,
    },
}