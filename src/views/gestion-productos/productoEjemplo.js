export default function (id = null, name = "CHANCE 2", short = "chance2") {
    return {
        ...(id ? {
            "codigo": id,
            "guid": id
        } : {}),
        "nombre": name,
        "nombreResumido": short,
        "caducidadTicket": 30,
        "caducidadReintegro": 5,
        "combinaSeccion": "1",
        "status": "0",
        "secciones": [
            {
                "nombreSeccion": "ASTRAL",
                "tipoSeccion": "01",
                "tieneSimbolo": "0",
                "cantidadTriples": "1",
                "cantidadSimbolos": "1",
                "statusSeccion": "0",
                "guid": "169099450575102665073911078848"
            }
        ],
        "seccionComponentes": [
            {
                "seccion": "169099450575102665073911078848",
                "componente": "01",
                "longitudCampo": "3",
                "nombreEtiqueta": "triple",
                "maxSimbolosLista": 0,
                "ordenJerarquia": "1",
                "guid": "169099455541109397357429899282"
            },
            {
                "seccion": "169099450575102665073911078848",
                "componente": "03",
                "longitudCampo": "2",
                "nombreEtiqueta": "signo",
                "maxSimbolosLista": "1",
                "ordenJerarquia": 0,
                "guid": "169099456997704983193628628468"
            }
        ],
        "seccionModalidades": [
            {
                "seccion": "169099450575102665073911078848",
                "modalidad": "01",
                "montoMinimo": 1,
                "multiplo": 1,
                // "componentes": [
                //     {
                //         "modalidad": "01",
                //         "guid": "01"
                //     }
                // ],
                "guid": "169099461738901047032996494757"
            }
        ]
    }
}