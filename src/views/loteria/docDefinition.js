import { arrow, blanksShape, boldBreak, nameCode, nameDetailed, objForEach, objMap, objReduce, parrafr, toMoney } from '@/_helpers/formatters';
const isHeadDate = (str) => /[0-9]:([0-9]{2})\s?(AM|PM)/.test(str);
const expHeadDate = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
const elLastSlice = (elemLast, elem, defs) => (elemLast ? elemLast.slice(defs.length) : '') + elem;
const spaceFill = (center, blankS, num, start = true) => (center == 3 && blankS > num ? '*'.padEnd(blankS == (num + 1) ? 1 : (center - 2), '*') : '')['pad'+(start ? 'Start' : 'End')](Math[start ? 'ceil' : 'floor'](blankS), center == 4 ? '*' : center == 5 ? '=' : center == 6 ? ':' : ' ');
const applySpace = +'0';
const spcr = (n, force = false) => {// Espaceador
    const len = n+(force ? 0 : 1);
    return applySpace || force ? Array(len).fill(' ').join('') : ''
}

// DEFINICIONES DE PRIMEROS CARACTERES DE LINEA
// ABCD
// A --> 0: no es linea de fecha header, 
//       1: si lo es
// B --> 0: sin negrita, 
//       1: negrita, 
//       2: negrita y letra mas 
// C --> 0: alineacion izquierda a derecha, 
//       1: centrado, 
//       2: derecha a izquierda, 
//       3: centrado entre asteriscos, 
//       4: centrado entre asteriscos x 2, --> cambio a rellene los espacios de *
//       5: centrado con relleno
//       6: derecha a izquierda con relleno (:) 
// D --> 0: no ignora linea
//       1: ignora linea
const cantDefs = 4;
const defr = (defn) => Array.from(Array(cantDefs), (_, i) => defn[i] || '0').join('');
// 

const ticketContentStruct = (first, ...args) => {
    let structure = first;
    for (let i = 0; i < args.length; i++) {
        structure = structure && structure[args[i]];
        if (!structure) break;
    }
    return structure
};

const objStruct = (n, labels, attr, val = null) => {
    return {
        [n]: {
            ...(labels[n] || {}),
            [attr]: val ?? attr
        }
    }
};

const makeFechaHead = (textleft, textRight, lineSize, negrita, numeric) => {
    let lineasN;
    if (textleft.length > lineSize) lineasN = parrafr(textleft, numeric, lineSize, negrita);
    else {
        lineasN = [textleft];
        const spaceRemaining = lineSize - textleft.length;
        if (textRight.length < spaceRemaining) {
            const spaceBetween = spaceRemaining - textRight.length;
            lineasN = [boldBreak(lineasN[0] + arrow(spaceBetween, true, ' ', ' ') + textRight, negrita)];
        }
        else {
            lineasN = [boldBreak(lineasN[0], negrita)];
            const newLinex = parrafr(textRight, numeric, lineSize, negrita);
            lineasN.push(...newLinex);
        }
    }
    const theLines = [...lineasN];
    return theLines
};

const orderBeginToEnd = (str) => {
    const fin = str.split('**');
    const comienzo = fin.shift();
    return comienzo + '**' + fin.sort().join('**');
};

const groupedBetsFormattr = (jugadas, labels, inx4, starter) => {
    // se clasifican las jugadas entre sencillas y variadas (ejercen multiplicacion)
    const { sencillas, varias } = objReduce(jugadas, (rv, k) => {
        const { sencillas, varias } = rv;
        if (k.includes('***')) {
            const label = labels[inx4][k];
            const ini = '0' + label.slice(0, starter);
            let mainKey;
            label.slice(starter).split('***').forEach((multiplicador, i, arr) => {
                if (!mainKey) {
                    const shrtNm = arr[0].split('**')[0];
                    mainKey = ini + (shrtNm + Array(jugadas[k]).fill(multiplicador.slice(shrtNm.length)).join(''));
                }
                if (!i) varias[mainKey] = varias[mainKey] || {};
                else {
                    const objMain = varias[mainKey];
                    let [nombreCorto, simbolo] = multiplicador.split('**');
                    nombreCorto = ini + nombreCorto;
                    varias[mainKey] = {
                        ...objMain,
                        [nombreCorto]: (objMain[nombreCorto] || (nombreCorto)) + '**' + simbolo
                    };
                }
            }); 
        }
        else sencillas[k] = jugadas[k];

        return { sencillas, varias }
    }, { sencillas: {}, varias: {} });

    const variasOrdered = objReduce(varias, (rv, vk) => {
        const mainKey = orderBeginToEnd(vk);
        rv[mainKey] = {
            ...(rv[mainKey] || {}),
            ...objMap(varias[vk], (vVk) => orderBeginToEnd(varias[vk][vVk]))
        };
        return rv
    }, {});

    // se le dan formato a las jugadas que ejercen multilicacion entre ellas
    const variasFrmttd = objReduce(variasOrdered, (rv, k, i, arr) => {
        const multiplicadores2ndrs = objReduce(variasOrdered[k], (rv2, k2, i2) => {
            const desc = variasOrdered[k][k2];
            if (!i2) return desc;
            return rv2 + '***' + desc.slice(starter + 1);
        }, '');
        const simbolos = k.split('**'); 
        const nombreCorto = simbolos.splice(0, 1)[0];
        rv[multiplicadores2ndrs] = (rv[multiplicadores2ndrs] || nombreCorto) + '**' + simbolos.join('**');

        if (i == (arr.length - 1)) 
            rv = objReduce(rv, (rvLast, kLast) => {
                const chain = (rv[kLast] + '***' + kLast.slice(starter + 1)).split('***');
                const total = chain.reduce((tot, subChain) => tot * subChain.split('**').slice(1).length, 1);
                const camposLen = chain.reduce((obj, group, i) => ({...obj, [i]: {...(obj[i] || {}), ...group.split('**').reduce((obj2, cmp) => ({...obj2, [cmp]: (obj2[cmp] || 0) + 1}), {})}}), {});
                const frmttdChain = chain.reduce((resChain, grp, i_) => resChain + (i_ ? '***' : '') 
                    + grp.split('**').reduce((resCmp, cmp, j) => {
                        const componentCount = camposLen[i_][cmp];
                        const strChain = resCmp + (!componentCount ? '' : ((j ? '**' : '') + cmp + (componentCount > 1 && j/* cmp.includes('*') */ ? ('('+componentCount+')') : '')));
                        camposLen[i_][cmp] = 0;
                        return strChain
                    }, '')
                , '')
                rvLast[frmttdChain] = total;

                return rvLast
            }, {});
        
        return rv
    }, {});

    // se integran las jugadas sencillas con las formateadas
    const jugadasFinal = {... sencillas, ...variasFrmttd};

    // se le da formato a las jugadas sencillas, las que ejercen multiplicacion se dejas tal cual
    // y se reagrupa por jugadas unicas y las que ejercen multiplicacion enttre ellas
    const { fromOne, several } = objReduce(jugadasFinal, (rv, k) => {
        const cant = jugadasFinal[k];
        if (k.includes('***')) rv.several[k] =  cant;
        else {
            const [nombreCorto, simbolo] = k.slice(starter).split('**'); 
            const fo = rv.fromOne['1'+nombreCorto] || [k.slice(0, starter) + nombreCorto, 0];
            rv.fromOne['1'+nombreCorto] = [
                fo[0] + '**' + simbolo + (cant > 1 ? `(${cant})` : ''),
                fo[1] + cant
            ];
        }

        return rv
    }, { fromOne: {}, several: {} });

    // se reintegran todas las jugadas
    const resultObj = { ...fromOne, ...several };

    // se define la estructura de las jugadas la cual es: descripcion-jugada: cantidad
    const jugadasFmtd = objReduce(resultObj, (rv, k) => {
        const resK = resultObj[k];
        const isNum = typeof resK == 'number';
        const resOk0 = isNum ? k.slice(1) : resK[0];
        rv[resOk0] = isNum ? resK : resK[1];
        // labels[inx4][resOk0] = resOk0;
        return rv
    }, {});

    // NUEVO-----------------------------
    const [jugadasFmtdReducedK, jugadasFmtdReducedV] = objReduce(jugadasFmtd, (reducedBets, jugK, i) => [
        reducedBets[0] + (!i ? '' : '****') + jugK.slice(!i ? 0 : starter), 
        reducedBets[1] + jugadasFmtd[jugK]
    ], ['', 0]);
    const jugadasFmtdReduced = {
        [jugadasFmtdReducedK]: jugadasFmtdReducedV
    };
    labels[inx4][jugadasFmtdReducedK] = jugadasFmtdReducedK;
    // FIN DE LO NUEVO------------------------------

    return jugadasFmtdReduced
}

// NEW
const preTicketData = (data) => {
    const {tableData, info: { header, footer, listas, lineSize }} = data;
    
    const {asociados, labels} = tableData.reduce((todaData, jugada) => {
        const {multiplicador} = jugada;
        const secTime = jugada.nombreSorteo().replaceAll(' ', '')/* `${jugada.section[0]}${jugada.dateTime.replaceAll(' ', '')}` */;
        let {asociados, labels} = todaData;
        const existentData = asociados[jugada.associated];
        const theresNum = multiplicador.some(mI => typeof mI[1] == 'string');
        const someArr = multiplicador.some(mI => mI[1] instanceof Array);
        const unir = multiplicador.length <= 2 && !someArr/*  && !theresNum */;
        const jugadaIden = multiplicador
            .map(m => m[0][0] + '**' + (
                typeof m[1] == 'string' ? m[1] 
                : (m[1] instanceof Array ? m[1] : [m[1]])
                    .map(m1 => /* theresNum ? nameDetailed(m1) :  */nameCode(m1, '*', theresNum)).join('**')
            ))
            .join('***');
        const descAttr = unir ? '1' : '0'; // 1 agrupa, 0 no agrupa
        const descJugada = `${descAttr}${theresNum ? '1' : '0'}&${jugadaIden}`;
        asociados[jugada.associated] = { // se agregan primeramente los asociados (loterias)
            ...(existentData || {}),
            [jugada.product]: { // se agregan los productos
                ...(ticketContentStruct(existentData, jugada.product) || {}),
                [secTime]: { // se agregan las secciones
                    ...(ticketContentStruct(existentData, jugada.product, secTime) || {}),
                    [jugada.typeBet] : { // se agregan las modalidades
                        ...(ticketContentStruct(existentData, jugada.product, secTime, jugada.typeBet) || {}),
                        [jugada.played]: { // se agregan las jugadas
                            ...(ticketContentStruct(existentData, jugada.product, secTime, jugada.typeBet, jugada.played) || {}),
                            [descAttr]: { // se agregan las jugadas por union o no
                                ...(ticketContentStruct(existentData, jugada.product, secTime, jugada.typeBet, jugada.played, descAttr) || {}),
                                [descJugada]: (ticketContentStruct(existentData, jugada.product, secTime, jugada.typeBet, jugada.played, descAttr, descJugada) || 0) + 1
                            }
                        }
                    }
                }
            }
        };

        const loteryAso = listas.loterias[jugada.associated];
        const prod = listas.productos[jugada.product]?.nombreResumido || listas.productos[jugada.product].nombreProducto;
        // const secc = listas.secciones[jugada.section[0]]?.nombreResumido || listas.secciones[jugada.section[0]].nombreSeccion;
        labels = {
            ...objStruct(0, labels, jugada.associated, loteryAso && loteryAso[1]), // asociados
            ...objStruct(1, labels, jugada.product, window.atob(prod)), // productos
            ...objStruct(2, labels, secTime, jugada.nombreSorteo()/* `${secc} ${jugada.dateTime}` */), // secciones
            ...objStruct(3, labels, jugada.typeBet, jugada.typeBetName(true)), // modalidades
            ...objStruct(4, labels, descJugada), // jugadas
        }

        return { asociados, labels }
    }, { asociados: {}, labels: {} });

    return {
        header,
        body: {
            asociados, 
            labels
        },
        footer,
        lineSize
    };
};

export const forTicketData = (data) => {
    const ticketData = preTicketData(data);
    // const {body: {asociados, labels}} = ticketData;
    return {...ticketData}
};

export const docDef = ({ 
    header, 
    body: { asociados, labels }, 
    footer: { total, totalBets, fee, trunc, moneda, expirationDays, qrCode, mensajeInformativo }, 
    lineSize 
}) => {
    const inicio = [], contenido = [], fin = [];
    let jugada = 1;

    // HEADER
    header.forEach(h => inicio.push(`${defr(isHeadDate(h) ? '1' : '0')}${h}`));

    // CONTENT
    const [inx0,inx1,inx2,inx3,inx4] = [0,1,2,3,4];
    objForEach(asociados, ([asoK, asoV]) => {
        contenido.push(`${defr('0011')}${spcr(inx0)}${labels[inx0][asoK]}`);

        const productos = asoV;
        objForEach(productos, ([proK, proV]) => {
            contenido.push(`${defr('024')}${spcr(inx1)}${labels[inx1][proK]}`);

            const secciones = proV;
            objForEach(secciones, ([secK, secV]) => {
                contenido.push(`${defr('015')}${spcr(inx2)}${labels[inx2][secK]}`);

                const modalidades = secV;
                objForEach(modalidades, ([modK, modV]) => {
                    contenido.push(`${defr('0')}${spcr(inx3)}${labels[inx3][modK]}`);

                    const montos = modV;
                    objForEach(montos, ([monk, monV]) => {
                        const jugadasParesObjOno = monV;
                        objForEach(jugadasParesObjOno, ([jpoonoK, jpoonoV]) => {
                            const agrupa = +jpoonoK;
                            let jugadas = jpoonoV;
                            const starter = 3; // 3 --> agrupa-numerico-&

                            if (agrupa) jugadas = groupedBetsFormattr(jugadas, labels, inx4, starter);

                            objForEach(jugadas, ([jugK, jugV]) => {
                                const numerico = !!+jugK[1];
                                const defs = defr('0') + spcr(inx4);
                                const numJugada = 'J'+jugada+')';
                                const bases = (numJugada + labels[inx4][jugK].slice(starter)).split('****');
                                const right = `${jugV > 1 ? ('('+jugV+')') : ''}x${moneda || ''}${toMoney(monk)}`;
                                const longName = numerico && jugV == 1;

                                let elemLast;
                                const contentPusher = (lineArrN, split = true, continued = false, comma = false, pushMulti = true, isLast = false) => {
                                    const preLines = (parrafr(lineArrN, numerico, lineSize, defs, false, split, continued, isLast, longName)).map(pl => blanksShape(pl));
                                    let lastLine = preLines.pop();
                                    lastLine = blanksShape(lastLine)/* .replaceAll('+', '|') */;
                                    if (split) elemLast = lastLine + (comma ? ',' : '');
                                    const thaLines = pushMulti ? [...preLines] : [lastLine];
                                    const deformatted = thaLines.map(theL => theL.replaceAll('+', ','));
                                    contenido.push(...deformatted);
                                };

                                for (let leftInd = 0; leftInd < bases.length; leftInd++) {
                                    if (leftInd) elemLast = elemLast.replaceAll(',', '+') + ',/'; 
                                    let left = bases[leftInd].split('***');

                                    left = left.map(
                                        campo => campo.split('**').map((descC, di) => {
                                            const splitt = descC.split('*');
                                            const keepLong = longName && splitt.length > 2;
                                            return !di 
                                                ? (descC && !keepLong ? (descC + ':') : '') 
                                                : ((descC ? (di > 1 ? ',' : '') : '') + (keepLong ? nameDetailed(splitt[2], true) : splitt.slice(0,2).join('-')))
                                        }).join('')
                                    );
                                    
                                    const leftCopy = [...left];
                                    left = leftCopy.pop();

                                    // pushing lefts
                                    leftCopy.forEach(elem => contentPusher(elLastSlice(elemLast, elem, defs), true, !!elemLast, true));
                                    // pushing last left
                                    contentPusher(elLastSlice(elemLast, left, defs), true, !!elemLast, false, true, true);
                                    if (leftInd && leftInd == bases.length - 1) elemLast = elemLast.replaceAll('+', ','); 
                                }

                                const spc = lineSize - (elemLast.length - defs.length) - right.length;
                                if (spc > 1) contenido.push(`${elemLast}${arrow(spc)}${right}`);
                                else {
                                    const dashLen = right.length + spc;
                                    contenido.push(`${elemLast}${dashLen > 0 ? arrow(right.length + spc, true, '-') : ''}`);
                                    const spcRight = lineSize - right.length;
                                    if (spcRight > 1) contenido.push(`${defs}${arrow(spcRight)}${right}`);
                                    else contentPusher(`->${right}`, false, false, false, false);
                                }
                                jugada++;
                            }, 'kv');

                        }, 'kv');

                    }, 'kv');
                    
                }, 'kv');

            }, 'kv');

        }, 'kv');
        
    }, 'kv');

    // FOOTER
    if (fee && totalBets)   fin.push(`${defr('015')}TOTAL JUGADAS ${moneda || ''}${toMoney(totalBets, trunc)}`);
    if (fee)                fin.push(`${defr('015')}IMPUESTO ${moneda || ''}${toMoney(fee, trunc)}`);
    if (total)              fin.push(`${defr('015')}TOTAL ${moneda || ''}${toMoney(total, trunc)}`);
    if (expirationDays)     fin.push(`${defr('003')}${expirationDays}`);
    if (qrCode)             fin.push(`${defr('001')}${qrCode}`);
    if (mensajeInformativo) fin.push(`${defr('0')}${mensajeInformativo}`);

    // RESULT
    const posTaquiSerAlt = inicio.length - 2;
    const lines = [...inicio,...contenido,...fin]
        .reduce((allLines, l, li) => {
            const notPass = li === posTaquiSerAlt;
            const makeLeftRight = li === posTaquiSerAlt + 1;
            const lineChars = l.split('');
            const [fechaHead, negrita_, center, skipLine] = lineChars.splice(0, cantDefs).map(ch => +ch);
            const showNegritaMarker = +'0'; // MOMENTANEO
            const negrita = showNegritaMarker ? negrita_ : '';
            let theLine = [lineChars.join('')];
            const theLine0 = `${theLine[0]}`.trim();
            theLine = [...(theLine0.length > lineSize ? parrafr(theLine0, false, lineSize, negrita) : [boldBreak(theLine0, negrita)])];
            // CONDICIONALES
            if (fechaHead || makeLeftRight) {
                const [textleft, textRight] = makeLeftRight 
                    ? [`${inicio[posTaquiSerAlt]}`.trim().slice(cantDefs), theLine0]
                    : theLine0.split(expHeadDate).map((text, i) => !i ? (text + theLine0.match(expHeadDate)[0]) : text.trim());
                theLine = makeFechaHead(textleft, textRight, lineSize, negrita, false);
            }
            else if (center) theLine = theLine.map(l => {
                const len = l.length;
                // const lenM1 = len - 1;
                const ltr = center == 6;
                const lenM1 = len - 2;
                const lenQ = lineSize - (len - (showNegritaMarker ? 3 : 2));
                const blankSpaces = lenQ > 0 ? (lenQ / (ltr ? 1 : 2)) : 0; // menos negrita y salto de linea
                const bold = showNegritaMarker ? l.charAt(0) : '', text = l.slice(showNegritaMarker ? 1 : 0, lenM1), breakr = l./* charAt */slice(lenM1),
                spaces1 = ltr ? '' : spaceFill(center, blankSpaces, 0), spaces2 = spaceFill(center, blankSpaces, 1, false);

                return `${bold}${spaces1}${text}${spaces2}${breakr}`
            });
            //
            if (!skipLine && !notPass) allLines.push(...theLine);
            return allLines
        }, []);

    return lines
}