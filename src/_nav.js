// const user = +(localStorage.getItem('user') || 'NaN');

// const test = [{
//   desMenu: 'Gestionar Esquema de Negociacion',
//   urlMenu: 'esquema-negociacion',
//   icoMenu: 'cic-negotiation',
//   codMenu: 900,
//   codPadre: 0,
// },
// {
//   desMenu: 'Gestion de Grupo - Listero',
//   urlMenu: 'grupo-listero',
//   icoMenu: 'cic-group-listers',
//   codMenu: 901,
//   codPadre: 0,
// },
// {
//   desMenu: 'Gestion de SubGrupo',
//   urlMenu: 'subgrupo',
//   icoMenu: 'cil-group',
//   codMenu: 902,
//   codPadre: 0,
// },
// {
//   desMenu: 'Gestion de Punto de Venta',
//   urlMenu: 'punto-venta',
//   icoMenu: 'cic-pos',
//   codMenu: 903,
//   codPadre: 0,
// },
// {
//   desMenu: 'Gestion de Taquilla - Sist. Administrativo',
//   urlMenu: 'taquilla-sist-administrativo',
//   icoMenu: 'cic-ticket-office',
//   codMenu: 904,
//   codPadre: 0,
// },
// {
//   desMenu: 'Repetir Ticket',
//   urlMenu: 'ticket-repeat',
//   icoMenu: 'cil-loop',
//   codMenu: 905,
//   codPadre: 0,
// },
// {
//   desMenu: 'Resultado Ticket',
//   urlMenu: 'result-ticket',
//   icoMenu: 'cil-clipboard',
//   codMenu: 906,
//   codPadre: 0,
// },
// {
//   desMenu: 'Listado de ventas por fecha',
//   urlMenu: 'venta-por-fecha',
//   icoMenu: 'cil-list',
//   codMenu: 907,
//   codPadre: 0,
// },
// {
//   desMenu: 'Tickets Premiados',
//   urlMenu: 'winner-ticket',
//   icoMenu: 'cic-award',
//   codMenu: 908,
//   codPadre: 0,
// }];

export default function menuMaker(menu, regular = false) {
  if (!menu) return [];
  
  const menuish = (mItem) => {
    const urlMenu = regular ? '' : window.atob(mItem.urlMenu);
    const itemDesc = regular ? mItem 
      : {
        component: 'CNavItem',
        name: /* window.atob( */mItem.desMenu/* ) */,
        to: '/'+urlMenu,
        icon: window.atob(mItem.icoMenu),
        callLoader: !urlMenu.includes('-') ? urlMenu : urlMenu.split('-').map((txt,i) => i ? (txt.slice(0,1).toUpperCase()+txt.slice(1)) : txt).join(''),
      };
    return itemDesc
  };

  const elme = [];
  const [codMenu, codPadre] = regular ? ['codigoMenu', 'idMenuPadre'] : ['codMenu', 'codPadre'];
  const menuDef = menu.reduce((todos, cada) => ({...todos, [cada[codMenu]]: cada}), {});
  let posEl = 0, del = [], [newArr, defArr] = [{...menuDef}, {...menuDef}];
  let arrNotObj = Object.values(newArr);
  while (arrNotObj.length) {
    arrNotObj.forEach((arrI) => {
      const padreCod = +window.atob(arrI[codPadre]); // se desencripta el codigo padre para obtener el strin numero y transformarlo en entero para la validacion de if
      if ((!posEl && !padreCod) || (posEl && elme[posEl - 1].includes(arrI[codPadre]))) {
        elme[posEl] = [...(elme[posEl] || []), arrI[codMenu]];
        del.push(arrI[codMenu]);
      }
    });
    if (del.length) del.forEach(elim => delete newArr[elim]);
    arrNotObj = Object.values(newArr);
    del = [];
    posEl++;
  }

  const menuFinal = elme.reduceRight((all_, each_, inx) => {
    all_ = each_.reduce((todou, cda) => {
      const itm = menuish(defArr[cda]);
      if (all_[cda]) {
        itm['items'] = [...all_[cda]];
        if (!inx) delete all_[cda];
      }
      if (!inx) todou[cda] = itm;
      else todou[defArr[cda][codPadre]] = [...(todou[defArr[cda][codPadre]] || []), itm];

      return todou
    }, {});

    return all_
  }, {});

  return Object.values(menuFinal)
}

const fatherChildren = (itm_, lvl) => {
  const itm = {...itm_, nivel: lvl};
  let childrn = itm.items ? itm.items.slice() : [];
  if (childrn.length) {
      delete itm.items;
      childrn = childrn.reduce((allItems, itemCh) => [...allItems, ...fatherChildren(itemCh, lvl+1)], []);
  }
  const theSet = [itm, ...childrn];
  return theSet
};

export const menuFinal = (menu) => menuMaker(menu, true).reduceRight((wholeMenu, menuItem) => [...wholeMenu, ...fatherChildren(menuItem, 0)], []);
// [
//   {
//     component: 'CNavItem',
//     name: 'Dashboard',
//     to: '/dashboard',
//     icon: 'cil-bar-chart',
//     callLoader: 'dashboard',
//   },
//   {
//     component: 'CNavItem',
//     name: 'Loteria',
//     to: '/loteria',
//     icon: 'cic-lotto',
//     callLoader: 'loteria',
//   },
//   {
//     component: 'CNavItem',
//     name: 'Loteria 2',
//     to: '/loteria2',
//     icon: 'cic-lotto',
//     callLoader: 'loteria2',
//   },
  // // ...(user === 1 ? [
    // {
    //   component: 'CNavItem',
    //   name: 'Gestionar Esquema de Negociacion',
    //   to: '/esquema-negociacion',
    //   icon: 'cic-negotiation',
    //   callLoader: 'esquemaNegociacion',
    // },
    // {
    //   component: 'CNavItem',
    //   name: 'Gestion de Grupo - Listero',
    //   to: '/grupo-listero',
    //   icon: 'cic-group-listers',
    //   callLoader: 'grupoListero',
    // },
    // {
    //   component: 'CNavItem',
    //   name: 'Gestion de SubGrupo',
    //   to: '/subgrupo',
    //   icon: 'cil-group',
    //   callLoader: 'subgrupo',
    // },
    // {
    //   component: 'CNavItem',
    //   name: 'Gestion de Punto de Venta',
    //   to: '/punto-venta',
    //   icon: 'cic-pos',
    //   callLoader: 'puntoVenta',
    // },
    // {
    //   component: 'CNavItem',
    //   name: 'Gestion de Taquilla - Sist. Administrativo',
    //   to: '/taquilla-sist-administrativo',
    //   icon: 'cic-ticket-office',
    //   callLoader: 'taquillaSistAdministrativo',
    // },
  // // ] : []),
  // {
  //   component: 'CNavItem',
  //   name: 'Repetir Ticket',
  //   to: '/ticket-repeat',
  //   icon: 'cil-loop',
  //   callLoader: 'ticketRepeat',
  // },
  // {
  //   component: 'CNavItem',
  //   name: 'Anular Ticket',
  //   to: '/anular-ticket',
  //   icon: 'cil-ban',
  //   callLoader: 'anularTicket',
  // },
  // {
  //   component: 'CNavItem',
  //   name: 'Resultado Ticket',
  //   to: '/result-ticket',
  //   icon: 'cil-clipboard',
  //   callLoader: 'resultTicket',
  // },
  // {
  //   component: 'CNavItem',
  //   name: 'Listado de ventas por fecha',
  //   to: '/venta-por-fecha',
  //   icon: 'cil-list',
  //   callLoader: 'ventaPorFecha',
  // },
  // {
  //   component: 'CNavItem',
  //   name: 'Tickets Premiados',
  //   to: '/winner-ticket',
  //   icon: 'cic-award',
  //   callLoader: 'winnerTicket',
  // },
// ]



// const menuStructured = [...menu/* , ...test */].reduce((menu_, mItem) => {
//   if (!mItem.codPadre) {
//     if (!menu_[`${mItem.codMenu}`]) menu_[mItem.codMenu] = menuish(mItem);
//     else 
//       menu_[mItem.codMenu] = {
//         ...menu_[mItem.codMenu],
//         ...menuish(mItem),
//       };
//   }
//   else {
//     if (!menu_[mItem.codPadre])
//       menu_[mItem.codPadre] = {
//         items: [ menuish(mItem) ]
//       };
//     else 
//       menu_[mItem.codPadre] = {
//         ...menu_[mItem.codPadre],
//         items: [ 
//           ...(menu_[mItem.codPadre].items || []),
//           menuish(mItem) 
//         ]
//       };
//   }
//   return menu_
// }, {});