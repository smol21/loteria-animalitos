import { useMultimoneda } from '@/store/multimoneda';
import { storeToRefs } from 'pinia';

export function loadMultiMon () {
    const multimonedaStore = useMultimoneda()
    const { moneda, monedaBase, monedas, minTicket, maxTicket, permiteDecimal } = storeToRefs(multimonedaStore)
    const { setMonedas } = multimonedaStore

    return {
      moneda, 
      monedaBase, 
      monedas, 
      minTicket, 
      maxTicket,
      permiteDecimal,
      setMonedas,
    }
}