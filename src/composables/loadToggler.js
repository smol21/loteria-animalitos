import { useGeneral } from '@/store/general';
import { onMounted } from 'vue';

export function loadToggler () {
    const { setGralVal } = useGeneral()

    onMounted(() => {
        setGralVal(['generalLoading', false]);
    })
}