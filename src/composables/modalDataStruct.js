import { ref } from "vue";

export function modalSetupData(...modals) {
    const lastOne = modals[modals.length - 1];
    const isPlain = typeof lastOne === 'boolean' ? modals.pop() : false;
    const data = modals.reduce((attrs, modal) => ({
        ...attrs,
        ...(!isPlain ? { [modal+'Mode']: ref('register') } : {}),
        [modal+'ModalVisible']: ref(false),
        [modal+'ModalDta']: ref({}),
    }), { modalsCount: modals.slice() });

    const wholeData = {
        modalPre: ref(modals.length > 1 ? '' : modals[0]),
        ...data,
    };

    return wholeData
}