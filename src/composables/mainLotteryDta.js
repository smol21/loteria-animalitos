import { Campo } from '@/_helpers/classes'
import { shortcutsSet } from '@/_helpers/general'
import { useSidebar } from '@/store/sidebar'
import { storeToRefs } from 'pinia'
import { ref } from 'vue'

export function loadMainLotDta (onlyDropDr = false) {
    const sidebarStore = useSidebar()
    const { sidebarVisible, sidebarUnfoldable } = storeToRefs(sidebarStore)
    const modeSize = ref({
        'x': 1,//3, // max to call next input
        'a': 1,//2, // all
        // 'n': 2,
        // 's': 2,
        'p': 1,//5,
        // 'c': 2,
        'm': -1,//7, //monto
        'l': 0, // para la longitud minima
    })
    const specialCase = 'c'

    const form = ref({}) // PARA MANEJO DE VALUE DE INPUTS DE APUESTA
    const forms = ref({})
    const figsCheck = ref({}) // PARA MANEJO DE ERROR DE INPUTS DE SIMBOLO
    const visibleFields = ref([]) // Los campos a mostrar
    const formMonto = ref({ len: modeSize.value.m, code: 'monto', wrong: false, maxLen: 100, precision: 4, montoMinimo: Infinity, montoMultiplo: Infinity, errLabel: 'precision', required: false })

    const confForms = (isForform = true, campos = null, onlyDropD = false) => {
        const cValues = campos && Object.values(campos)
        const [inputs, figures] = campos && cValues.reduce((cs, c, i, arr) => { 
            cs[c.tipo == 'input' ? 0 : 1].push(...(i == arr.length - 1 ? [c] : [c, null]))
            return cs
        }, [[], []]) || [[null], [null]]

        if (!isForform) { 
            const [inputs_, figures_] = [inputs, figures].map(fs => fs.filter(f => f))
            const theresInputs = inputs[0] && inputs_ 
            const theresFigures = figures[0] && figures_ 
            visibleFields.value = [
            ...(theresInputs || (!theresFigures && [new Campo({
                codigo: '',
                tipo: 'input',
                etiqueta: '',
                obligatorio: true,
                dropdown: '0',
            })] || [])), 
            ...(theresFigures || (!theresInputs && [new Campo({
                codigo: '',
                tipo: 'img',
                etiqueta: '',
                obligatorio: true,
                dropdown: onlyDropD ? '1' : '0',
            })] || []))
            ]
        }
        else {
            form.value = {
            ...['corrida','numero','serie','permuta'].reduce((attrs, attr, i) => (
                {...attrs, ...inputs.reduce((subAttrs, c) => ({...subAttrs, ['apuesta'+(c && c.codigo || '')+attr]: i ? '' : { desde: '', hasta: ''}}), {})}
            ),{}),
            ...figures.reduce((subFigs, c) => ({
                ...subFigs,
                ['apuesta'+(c && c.codigo || '')+'figura']: {
                nro: '',
                figura: '',
                },
            }), {}),
            apuestamonto: '',
            }

            figsCheck.value = figures.reduce((subFigs, c) => ({
                ...subFigs,
                ['apuesta'+(c && c.codigo || '')+'figura']: false,
            }), {})

            const wronger = (fields, range = false) => fields && fields.length ? fields.reduce((wrongs, input) => ({...wrongs, [(range ? 'rangeWrong' : 'wrong')+(input && input.codigo || '')]: (range ? [false, false] : false)}), {}) : {[range ? 'rangeWrong' : 'wrong']: (range ? [false, false] : false)}
            const requir = (fields, range = false) => fields && fields.length ? fields.reduce((reqs, input) => ({...reqs, [(range ? 'rangeRequired' : 'required')+(input && input.codigo || '')]: (range ? [false, false] : false)}), {}) : {[range ? 'rangeRequired' : 'required']: (range ? [false, false] : false)}
            forms.value = {
                'n': { len: modeSize.value.x, code: 'numero', jsonKey: '0', label: 'Número', ...wronger(inputs[0] && inputs), key: shortcutsSet.ModoApuesta[1], ...requir(inputs[0] && inputs) },
                's': { len: modeSize.value.x, code: 'serie', jsonKey: '1', label: 'Serie', ...wronger(inputs[0] && inputs), key: shortcutsSet.ModoApuesta[3], ...requir(inputs[0] && inputs) },
                'p': { len: modeSize.value.p, code: 'permuta', jsonKey: '2', label: 'Permuta', ...wronger(inputs[0] && inputs), key: shortcutsSet.ModoApuesta[2], ...requir(inputs[0] && inputs) },
                'c': { len: modeSize.value.x, code: 'corrida', jsonKey: '3', label: 'Corrida', range: ['desde', 'hasta'], ...wronger(inputs[0] && inputs, true), key: shortcutsSet.ModoApuesta[0], ...requir(inputs[0] && inputs, true) },
                'figures': { len: modeSize.value.m/* a */, code: 'figura', jsonKey: '-1', labl: 'Valor', label: 'Figura', range: ['nro', 'figura'], ...wronger(figures[0] && figures, true), addDot: false, ...requir(figures[0] && figures, true) },
            }
        }
    }

    const setModeSize = (attr, val, justModeSize = false) => {
        modeSize.value = {
          ...modeSize.value,
          ...(attr == 'all' ? { x: 1, a: 1, p: 1, l: 0 } : { [attr]: val }),
        }
    
        if (!justModeSize) 
          forms.value = {
            ...forms.value,
            ...`${attr=='all'?'n s p c figures': attr=='a'?'figures': attr!='p'?'n s c': attr}`.split(' ').reduce((attrs, attr_) => ({
              ...attrs, 
              [attr_]: {
                ...forms.value[attr_],
                len: val
              }
            }), {}),
          }
    }

    confForms(true, null, onlyDropDr)
    confForms(false, null, onlyDropDr)

    const changeValsMonto = (...vals) => {
        formMonto.value = { 
          ...formMonto.value, 
          ...vals.reduce((finalObj, currVal) => ({...finalObj, [currVal[0]]: currVal[1]}), {})
        }
    }

    const rgxSiders = new RegExp("^0,2$|^-0,2$")

    return {
        rgxSiders,
        sidebarVisible, 
        sidebarUnfoldable,
        form,
        forms,
        modeSize,
        formMonto,
        visibleFields,
        figsCheck,
        specialCase,
        confForms,
        setModeSize,
        changeValsMonto,
    }
}