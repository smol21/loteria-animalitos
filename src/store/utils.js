import { fetchRequest } from '@/_helpers/fetchToolkit';
import { defineStore } from 'pinia'
import { useAuth } from './auth';

export const useUtils = defineStore('utils', {
    state: () => {
        return {
            loadingGeo: false,
            states: [],
            cities: [],
            municipalities: [],
            dfCities: [],
            dfMunicipalities: [],
            dgCities: [],
            dgMunicipalities: [],
        }
    },
    getters: {
        stateOptions: (state) => state.states.map(state_ => ({ label: window.atob(state_.nombreEstado), value: state_.codigoEstado })),
        ...[['cit','Ciudad'], ['municipalit','Municipio'], ['dfCit','Ciudad'], ['dfMunicipalit','Municipio'], ['dgCit','Ciudad'], ['dgMunicipalit','Municipio']]
            .reduce((gttrs, gttr) => ({
                ...gttrs,
                [gttr[0]+'yOptions']: (state) => state[gttr[0]+'ies'].map(geoOne => ({ label: window.atob(geoOne['nombre'+gttr[1]]), value: geoOne['codigo'+gttr[1]] }))
            }), {}),
    },
    actions: {
        async setGeo(...payload) {
            let msg = ['', ''];
            if ((payload.length === 1 && !this[payload[0]].length) || payload.length > 1) {
                const self = this;
                this.loadingGeo = true;
                const payl0 = payload[0];
                const payl0lw = payl0.toLowerCase();
                const { auth_tokenUsuario } = useAuth();
                const geoPath = (payl0lw.includes('states') ? 'ListarEstado' : payl0lw.includes('cities') ? 'ListarCiudad' : 'ListarMunicipio') + '/' + 
                                auth_tokenUsuario + (!payl0lw.includes('states') ? ('/' + payload[1]) : '');

                await fetchRequest({ 
                    api: this.$apiAdministrativo + geoPath, 
                    success: (rjson) => {
                        self.loadingGeo = false;
                        self[payl0] = rjson.datos;
                    }, 
                    error: (rjson) => {
                        self.loadingGeo = false;
                        console.error('error: ', rjson);
                        self[payl0] = [];
                        const wchGeo = !payl0lw.includes('states') ? (payl0lw.includes('cities') ? 'Ciudades' : 'Municipios') : 'Estados';
                        msg = [wchGeo, payl0]
                    },
                });
            }
            return msg
        },
        cleanGeo(...payload) {
            payload.forEach(geo => this[geo] = []);
        },
        cleanGeoAll(payload) {
            this.cities = [];
            this.municipalities = [];
            if (payload) {
                this.dfCities = [];
                this.dfMunicipalities = [];
                this.dgCities = [];
                this.dgMunicipalities = [];
            }
        },
    },
})