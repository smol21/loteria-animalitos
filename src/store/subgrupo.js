import { defineStore } from 'pinia';
import { subGrupos } from '@/_helpers/dataPruebas';
import { PuntoVenta } from '@/_helpers/classes';
import { usePuntoventa } from './puntoventa';

const saveToLocal = (subGrupos) => {
    localStorage.setItem('subgrupos', JSON.stringify(subGrupos));
};
const getFromLocal = () => {
    try {
        const localSubGrupos = JSON.parse(localStorage.getItem('subgrupos') || '[]');
        const subGrupos_ = localSubGrupos.length ? localSubGrupos : subGrupos;
        if (!localSubGrupos.length) saveToLocal(subGrupos_);
        return subGrupos_  
    } catch (er) {
        return null
    }
};

// MOMENTANEO
export const useSubGrupo = defineStore('subgrupo', {
    state: () => {
        return {
            subGrupos: [...getFromLocal()],
        }
    },
    actions: {
        addSubGrupo(subGrupo) {
            this.subGrupos = [...this.subGrupos, subGrupo];
            saveToLocal(this.subGrupos);
        },
        editSubGrupo(data) {
            const subGrupos_ = [...this.subGrupos];
            const ind = subGrupos_.findIndex(subGrupo => subGrupo.rif === data.rif);
            if (ind > -1) subGrupos_[ind] = {...subGrupos_[ind], ...data};
            this.subGrupos = subGrupos_;
            saveToLocal(this.subGrupos);

            if (ind > -1) {
                const subgrupos_ = this.subGrupos.map(subgrupo => ({rif: subgrupo.rif, lister: subgrupo.lister}));
                const { puntosVenta, editPuntoVenta } = usePuntoventa();

                // ACTUALIZAR DEPENDENCIAS EN RELACIONES
                puntosVenta.forEach(puntoVenta => {
                    const pos = new PuntoVenta(puntoVenta);
                    const [subgroups, toClean] = pos.sgSubGrupoData.reduce((ids, id_) => [
                        ...(subgrupos_.some(subg => subg.rif === id_ && subg.lister === pos.dgLister)
                            ? [[...(ids[0] || []), id_], [...(ids[1] || [])]] 
                            : [[...(ids[0] || [])], [...(ids[1] || []), id_]])
                    ], []);
                    if (toClean.length) editPuntoVenta({...pos, sgSubGrupoData: subgroups});
                });
            }
        },
    },
})