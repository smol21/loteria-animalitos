import { defineStore } from 'pinia';
import { puntosDeVenta } from '@/_helpers/dataPruebas';

const saveToLocal = (puntosDeVenta) => {
    localStorage.setItem('puntosDeVenta', JSON.stringify(puntosDeVenta));
};
const getFromLocal = () => {
    try {
        const localPOSs = JSON.parse(localStorage.getItem('puntosDeVenta') || '[]');
        const puntosDeVenta_ = localPOSs.length ? localPOSs : puntosDeVenta;
        if (!localPOSs.length) saveToLocal(puntosDeVenta_);
        return puntosDeVenta_ 
    } catch (er) {
        return null
    }
};

// MOMENTANEO
export const usePuntoventa = defineStore('puntoventa', {
    state: () => {
        return {
            puntosVenta: [...getFromLocal()],
        }
    },
    actions: {
        addPuntoVenta(puntoVenta) {
            this.puntosVenta = [...this.puntosVenta, puntoVenta];
            saveToLocal(this.puntosVenta);
        },
        editPuntoVenta(data) {
            const puntosVenta_ = [...this.puntosVenta];
            const ind = puntosVenta_.findIndex(puntoVenta => puntoVenta.dfRif === data.dfRif);
            if (ind > -1) puntosVenta_[ind] = {...puntosVenta_[ind], ...data};
            this.puntosVenta = puntosVenta_;
            saveToLocal(this.puntosVenta);
        },
        deletePuntoVenta(rif) {
            const puntosVenta_ = [...this.puntosVenta];
            const ind = puntosVenta_.findIndex(item_ => item_.dfRif === rif);
            if (ind > -1) puntosVenta_.splice(ind, 1);
            this.puntosVenta = [...puntosVenta_];
            saveToLocal(this.puntosVenta);
        },
    },
})