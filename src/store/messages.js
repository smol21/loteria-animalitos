import { fetchRequest } from '@/_helpers/fetchToolkit';
import { defineStore } from 'pinia'
import { useAuth } from './auth';

const saveToLocal = (name, value) => {
    localStorage.setItem(name, JSON.stringify(value));
};

const getFromLocal = (name) => {
    try {
        const data = localStorage.getItem(name) ? JSON.parse(localStorage.getItem(name)) : null;
        return data
    } catch (er) {
        return null
    }
};

export const useMessages = defineStore('messages', {
    state: () => {
        return {
			isNewMessage: false,
			loadingMessage: false,
            newMessage: getFromLocal('newMessage') ?? 0,
        }
    },
    getters: {
        stateMmesage: (state) => state.isNewMessage,
    },
    actions: {
        async setMessages(...payload) {
            let msg = ['', ''];
            //if ((payload.length === 1 && !this[payload[0]].length) || payload.length > 1) {
                const self = this;
                this.loadingMessage = true;
                const payl0 = payload[0];
                const { auth_tokenUsuario } = useAuth();
				
                await fetchRequest({ 
                    api: payl0 +'ConsultarBuzonMensaje/'+auth_tokenUsuario, 
                    success: (rjson) => {
                        self.loadingMessage = false;
						const countMessage = (rjson?.datos || []).filter(item => item.estatus === 'NO LEÍDO').length; 
						this.newMessage = countMessage;
                        saveToLocal('newMessage', countMessage);
                    }, 
                    error: (rjson) => {
                        self.loadingMessage = false;
                        console.error('error: ', rjson);
                        // self[payl0] = [];
                    },
                });
           //}
            return msg
        },

        cleanMessages() {
            this.newMessage = 0;
            saveToLocal('newMessage', 0);
        },

        setMessageNumber(...payload) {
            this.newMessage = +payload[0] ? payload[1] : 0;
            saveToLocal('newMessage', this.newMessage);
        },
    },
})