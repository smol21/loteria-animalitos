import { defineStore } from 'pinia'
import { testTaquillaData } from '@/_helpers/dataPruebas';

const saveToLocal = (taquillas) => {
    localStorage.setItem('taquillas', JSON.stringify(taquillas));
};
const getFromLocal = () => {
    try {
        const localTaquillas = JSON.parse(localStorage.getItem('taquillas') || '[]');
        const taquillas = localTaquillas.length ? localTaquillas : testTaquillaData();
        if (!localTaquillas.length) saveToLocal(taquillas);
        return taquillas
    } catch (er) {
        return null
    }
};

// MOMENTANEO
export const useTaquillasistadministrativo = defineStore('taquillasistadministrativo', {
    state: () => {
        return {
            taquillas: [...getFromLocal()],
        }
    },
    actions: {
        addTaquillaSistAdmin(taquilla) {
            this.taquillas = [...this.taquillas, taquilla];
            saveToLocal(this.taquillas);
        },
        editTaquillaSistAdmin(data) {
            const taquillas_ = [...this.taquillas];
            const ind = taquillas_.findIndex(esquema => esquema.guid === data.guid); // o serial
            if (ind > -1) taquillas_[ind] = {...taquillas_[ind], ...data};
            this.taquillas = taquillas_;
            saveToLocal(this.taquillas);
        },
        deleteTaquillaSistAdmin(id) {
            const taquillas_ = [...this.taquillas];
            const ind = taquillas_.findIndex(item_ => item_.guid === id); // o serial
            if (ind > -1) taquillas_.splice(ind, 1);
            this.taquillas = [...taquillas_];
            saveToLocal(this.taquillas);
        },
    },
})