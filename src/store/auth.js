import { defineStore } from 'pinia'
import { useGeneral } from './general';

const saveToLocal = (name, value) => {
    localStorage.setItem(name, JSON.stringify(value));
};

const getFromLocal = (name) => {
    try {
        const data = localStorage.getItem(name) ? JSON.parse(localStorage.getItem(name)) : null;
        return data   
    } catch (er) {
        return null
    }
};

export const useAuth = defineStore('auth', {
    state: () => {
        return {
            auth_columnaTicket: getFromLocal('columnaTicket') ?? '',
            auth_fechaSistema: getFromLocal('fechaSistema') ?? '',
            auth_login: getFromLocal('login') ?? '',
            auth_menu: getFromLocal('menu') ?? [],
            auth_nombrePuntoVenta: getFromLocal('nombrePuntoVenta') ?? '',
            auth_showBuzon: getFromLocal('showBuzon') ?? false,
            auth_taquilla: getFromLocal('taquilla') ?? '',
            auth_tiempoAlerta: getFromLocal('tiempoAlerta') ?? '',
            auth_tiempoSesion: getFromLocal('tiempoSesion') ?? '',
            auth_tiempoSistema: getFromLocal('tiempoSistema') ?? '',
            auth_tipoUsuario: getFromLocal('tipoUsuario') ?? '',
            auth_tokenSistema: getFromLocal('tokenSistema') ?? '',
            auth_tokenUsuario: getFromLocal('tokenUsuario') ?? '',
            auth_usuarioLogueado: getFromLocal('usuarioLogueado') ?? '',
        }
    },
    actions: {
        authSetLocalCustom(payload) {
            const self = this;
            const isArr = payload instanceof Array;
            (!isArr ? Object.entries(payload) : [payload])
            .forEach(pyld => {
                self['auth_'+pyld[0]] = pyld[1];
                saveToLocal(pyld[0], pyld[1]);
            });
        },
        authClearAll() {
            const generalStore = useGeneral();

            [
                'columnaTicket',
                'fechaSistema',
                'login',
                'menu',
                'nombrePuntoVenta',
                'showBuzon',
                'taquilla',
                'tiempoAlerta',
                'tiempoSesion',
                'tiempoSistema',
                'tipoUsuario',
                'tokenSistema',
                'tokenUsuario',
                'usuarioLogueado',        
            ]
            .forEach(name => this['auth_'+name] = '');
            
            generalStore.setGralValLocal(['dateOfTimeOut', null]);
            
            localStorage.clear();
        }
    },
})