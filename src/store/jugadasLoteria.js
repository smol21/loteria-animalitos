import { defineStore } from 'pinia'

export const useJugadasLoteria = defineStore('jugadasloteria', {
    state: () => {
        return {
            betsIncrementIndex: 0,
        }
    },
    actions: {
        increaseIndex(i) {
            const newLen = this.betsIncrementIndex + i;
            this.betsIncrementIndex = newLen;
        },
        resetIndex() {
            this.betsIncrementIndex = 0
        },
    },
})