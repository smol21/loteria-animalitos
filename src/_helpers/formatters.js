import { PuntoVenta } from "./classes";

export const theBreak = '\r\n';
const applyBreak = (text) => text + theBreak;
const blanksOut = (str) => str.replaceAll(' ','#');

const recur = (palabra, limite, lastIteration, boldOrdefs, isText = true, lineas = []) => {
    const bldfs = boldOrdefs, not1 = limite > 1;
    const partSlice = isText ? (not1 ? limite - 1 : 1) : limite;
    const sentence = palabra.slice(0, partSlice);
    lineas.push(isText 
        ? boldBreak(sentence + (not1 && sentence.length > (limite - 2) ? '-' : ''), bldfs)
        : `${bldfs}${sentence}`
    );
    const sentence2 = palabra.slice(partSlice);
    if (sentence2.length > limite) return recur(sentence2, limite, lastIteration, bldfs, isText, lineas);
    else {
        lineas.push(isText 
            ? boldBreak(sentence2, bldfs, lastIteration)
            : `${bldfs}${sentence2}`
        );
        return lineas;
    }
};

const linesShouldBreak = (lineas_, firstBlock, limite, lastVal, bldfs, isText, sep, lastIter, lastOne, word = null) => {
    const fb = firstBlock;
    const spcPos = !isText ? (limite - (lastVal ? (lastVal.length - bldfs.length) : 0)) : 0;
    if (!isText && spcPos < sep.length) {
        const lines = recur(lastVal.slice(bldfs.length) + arrow(spcPos, true, sep), limite, lastIter, bldfs, isText);
        const firstLine = lines.shift();
        lineas_[lastOne] = firstLine;
        lineas_.push(...lines);
        const lastLine = lineas_.pop();    

        if (fb) lineas_.push(lastLine + arrow(limite - (lastLine.length - bldfs.length), true, '-'));
        else if (lastLine && (lastLine + word).length > limite) {
            lineas_.push(lastLine + arrow(limite - (lastLine.length - bldfs.length), true, '-'));
            lineas_.push(`${bldfs}${word}`);
        }
        else lineas_.push(`${lastLine || bldfs}${word}`);
    }
    else if (fb) lineas_[lastOne] = lastVal + arrow(spcPos, true, sep);
    else {
        lineas_[lastOne] = isText 
            ? applyBreak(lineas_[lastOne]) 
            : (lineas_[lastOne] + arrow(spcPos, true, sep));
        lineas_.push(isText ? boldBreak(word, bldfs, lastIter) : `${bldfs}${word}`);
    }
    return lineas_;
};

export const objOrNotAtobrToDash = (obj = null, ...attrs) => attrs.reduce((byDash, attr) => `${byDash ? (byDash+'-') : ''}${window.atob(attr instanceof Array ? attr[0] : obj ? obj[attr] : attr)}`, '');
export const objectIdsToDash = (obj = null, ...attrs) => attrs.reduce((byDash, attr) => `${byDash ? (byDash+'-') : ''}${attr instanceof Array ? attr[0] : obj ? obj[attr] : attr}`, '');

export const dateDepocher = (dateEpoch, epoch = true, slash = true, alreadyDate = false) => {
    const date_ = !alreadyDate ? (new Date(dateEpoch * (epoch ? 1000 : 1))) : dateEpoch;
    const day = `${date_.getDate()}`;
    const month = `${date_.getMonth() + 1}`;
    const year = date_.getFullYear();
    const separator = `${slash && '/' || '-'}`;
    const dateDepoched = `${day.padStart(2,'0')}` + separator + `${month.padStart(2,'0')}` + separator + year;

    return dateDepoched
}

export const dateTimeOfTheDay = (dateEpoch, epoch = true, secs = false, replaceBlanks = '') => {
    const date_ = new Date(dateEpoch * (epoch ? 1000 : 1));
    const hour = `${date_.getHours()}`;
    const hourFormatted = +hour > 12 ? `${+hour - 12}` : hour;
    const minutes = `${date_.getMinutes()}`;
    const seconds = `${date_.getSeconds()}`;
    const dots = replaceBlanks ? '.' : ':';
    let timeStr = `${hourFormatted.padStart(2,'0')}${dots}${minutes.padStart(2,'0')}${secs ? (dots+seconds.padStart(2,'0')) : ''} ${+hour > 12 ? 'PM' : 'AM'}`;
    if (replaceBlanks) timeStr = timeStr.replaceAll(' ', replaceBlanks);

    return timeStr
}

export const epoched = (dateEpoch) => Math.round(dateEpoch * 1000);
export const depoched = (dateEpoch) => Math.round(dateEpoch / 1000);
export const nowEpoched = () => new Date().getTime();
export const modForJS = (n, d, m = 1000) => ((((n * m) % (d * m)) + (d * m)) % (d * m)) / m;
export const porcientoJS = (n, prc, m = 1000) => (((+n * m) * (prc * m)) / 100) / (m * m);
export const sumaJS = (n1, n2, m = 1000) => ((n1 * m) + (n2 * m)) / m;

export const numberAndComma = (num, replaceComma = false) => {
    const asNum = replaceComma ? num.replaceAll('.','').replace(',','.') 
        : Number(typeof num == 'string'
            ? (num.includes(',') ? num : num.replace('.',',')).split(',').map((n, i) => !i ? n.replaceAll('.','') : n).join(',').replace(',','.') 
            : num);
    const modifiedTxt = replaceComma ? Number(asNum) : (asNum || 0).toFixed(2).replace('.',','); 
    return modifiedTxt    
}

export const toMoney = (num, trunc = 4) => {
    const numGot = Number(num) || 0;
    const isInt = Number.isInteger(numGot);
    const modifiedTxt = numGot[isInt ? 'toFixed' : 'toString'](...(isInt ? [2] : [])).split('.'); 
    const dotModify = modifiedTxt[0].split('').reverse().reduce(
        (frmt, n, i, arr) => (arr.length - 1 !== i && !((i + 1) % 3) ? '.' : '') + n + frmt, ''
    ) + (modifiedTxt[1] ? (',' + modifiedTxt[1].slice(0,trunc).padEnd(2,'0')) : '');
    return dotModify;
}

export const roundr = (theSum, prec) => {
    // const roundedSum = +(theSum.toFixed(prec + 1)); // previous implementation
    const mult = '1'.padEnd(prec + 1, 0);
    const roundedSum = Math.round((theSum + Number.EPSILON) * mult) / mult; // workaround to solve floating-point precision errors
    return roundedSum
};

// THIS FUNCTION IS CALLED PLUS ONE DUE TO PREVIOUS ROUNDING IMPLEMENTATION
export const rounderPlusOne = (arrData, attr = undefined, precision = 2) => {
    const isArr = arrData instanceof Array;
    const prec = !isArr ? (attr || precision) : precision;
    const theSum = !isArr ? +arrData
        : arrData.reduce((sum,srt) => {
            sum += attr ? +srt[attr] : +srt;
            return sum
        }, 0);
    return roundr(theSum, prec)
};

export const rounderPlusOneLiquidacion = (arrData, attr = undefined, precision = 2) => {
    const isArr = arrData instanceof Array;
    const prec = !isArr ? (attr || precision) : precision;
    const theSum = !isArr ? parseFloat(arrData)
        : arrData.reduce((sum,srt) => {
            sum += attr ? parseFloat(srt[attr]) : parseFloat(srt);
            return sum
        }, 0);
        return roundr(theSum, prec)
    // return theSum
};

export const noStar = (str) => str.replaceAll('*', '');
export const unnullify = (str) => str.replace(/[*0]/g, '1');
export const hasStar = (str) => str.startsWith('*');

// numeros aleatorios dentro de un rango
export const randBetween = (max, min = 0) => Math.floor(Math.random() * (max - min + 1)) + min;
// numeros aleatorios expecificados por longitud
export const randNumbers = (len, asString = true) => {
    const mult = +'1'.padEnd(len,'0');
    return Math.floor(mult + Math.random() * (9 * mult)) + (asString ? '' : 0)
};

export const nameCode = (j, divider = '-', all = false) => `${j.codigo.replaceAll(' ','')}${divider}${j.nombreResumido || j.nombre.slice(0, 3)}${all ? (divider + blanksOut(j.nombre)) : ''}`;
export const nameDetailed  = (j, isStr = false) => `${(isStr ? j : j.nombre).charAt(0)}${(isStr ? j : j.nombre).slice(1).replaceAll(' ','-')}`;

export const boldBreak = (text, b, applyBreak_ = true) => b + text + (applyBreak_ ? theBreak : '');
export const arrow = (len, end = false, str = '>', filler = '-') => str['pad'+(end ? 'End' : 'Start')](len, filler);

export const getTruncated = (resultMultiplo, decimals = 2, round = false) => +(round 
    ? Number(resultMultiplo).toFixed(decimals)
    : Number(resultMultiplo).toFixed(100).split('.').map((n, i) => !i ? n : n.slice(0, decimals < 2 ? 1 : (decimals - 1))).join('.'));

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns true si se cumple alguna condicion, false si ninguna se cumple.
 */
export const objSome = (obj, func = () => {}, asValue = false) => {
    for (const objkey in obj) if (func(asValue ? obj[objkey] : objkey)) return true
    return false
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns true si se cumple la condicion para cada iteracion, false si al menos una no se cumple.
 */
export const objEvery = (obj, func = () => {}, asValue = false) => {
    for (const objkey in obj) if (!func(asValue ? obj[objkey] : objkey)) return false
    return true
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns el Objeto pasado con nuevos values.
 */
export const objMap = (obj, func = () => {}, asValue = false) => {
    const newObj = {};
    for (const objkey in obj) newObj[objkey] = typeof func == 'function' ? func(asValue ? obj[objkey] : objkey) : func
    return newObj
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns el Objeto filtrado.
 */
export const objFilter = (obj, func = () => {}, asValue = false, applyJSON = true) => {
    const newObj = {};
    for (const objkey in obj) if (func(asValue ? obj[objkey] : objkey)) newObj[objkey] = (applyJSON ? JSON.parse(JSON.stringify(obj[objkey])) : obj[objkey]);
    return newObj
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {string} as_ string que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value, o si se le pasa ambos ('v': para value, 'kv': para ambos, por defecto es 'k').
 */
export const objForEach = (obj, func = () => {}, as_ = 'k') => {
    let i = 0;
    for (const objkey in obj) {
        func(as_ === 'v' ? obj[objkey] : as_ === 'kv' ? [objkey, obj[objkey]] : objkey, i);
        i++;
    }
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {any} returnVal valor de retorno, puede ser de cualquier tipo.
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns retorna segun el tipo pasado en returnVal.
 */
export const objReduce = (obj, func = () => {}, returnVal = null, asValue = false) => {
    const arr = Object[asValue ? 'values' : 'keys'](obj);
    const simple = returnVal ?? 'yes-it-is-simple';
    let i = simple === 'yes-it-is-simple' ? -1 : 0;
    let rVal = returnVal;
    for (const objkey in obj) {
        if (i === -1) rVal = asValue ? obj[objkey] : objkey;
        else rVal = func(rVal, asValue ? obj[objkey] : objkey, i, arr);
        i++;
    }
    return rVal
}

/**
 * @param {object} obj Objeto a iterar.
 * @param {Function} func funcion que se ejecuta en cada iteracion del Objeto, la cual recibe como parametro el key del Objeto de la iteracion actual o su value.
 * @param {boolean} custom booleano que indica si se retornara un valor truty personalizado si hizo match o si se retornara lo que indique returnKey (true: para custom, false: para returnKey. Por defecto es false).
 * @param {boolean} returnKey booleano que indica si se retornara el key del key/value del objeto que hizo match o si se retornara su value (true: para key, false: value. Por defecto es true).
 * @param {boolean} asValue booleano que indica si se le pasa a la funcion el key del objeto en la iteracion actual, o si se le pasa el value (true: para value, por defecto es false).
 * @returns custom/key/value si se cumple alguna condicion, undefined si ninguna se cumple.
 */
export const objFind = (obj, func = () => {}, custom = false, returnKey = true, asValue = false) => {
    for (const objkey in obj) {
        const funcVal = func(asValue ? obj[objkey] : objkey);
        if (funcVal) return custom ? funcVal : returnKey ? objkey : obj[objkey];
    }
    return undefined
}

export const orientedSlice = (bet, ordSel, oldStyled = false) => {
    if (oldStyled) return bet.slice(...(ordSel.split(',')));
    const isRTL = ordSel.includes('-');
    let [side1, side2] = ordSel.split(',');
    if (!side2) side2 = `${bet.length}`;
    const ordArr = [side1, side2].map(n => n.replace('-', ''));
    return isRTL ? bet.split('').reverse().slice(...ordArr).reverse().join('') : bet.slice(...ordArr);
}

export const blanksShape = (str) => str.replaceAll('#','-');

// EJEMPLO DE USO:
// parrafr('Hola este es un ejemplo de como utilizar esta funcion que hace saltos de linea por parrafo. Tiene una longitud de 30 caracteres por línea y esta en negrita', false, 30, '1');
export const parrafr = (text, numeric, limit, boldOrdefs, isText = true, split = true, continued = false, isLast = false, preserveBetweenSeparator = true) => {
    const bldfs = boldOrdefs;
    const {lineas} = (isText || split ? text.split(isText ? /\s/ : ',') : text).reduce((lineasDta, word_, i, arr) => {
        let {lineas, limite} = lineasDta;
        const unsep = !isText && word_.includes('/');
        const splitted = !isText ? word_.split(':') : [];
        let word = word_.slice(!isText && numeric && splitted[1] && !splitted[1].replace(/[aA-zZ#]/g, '').length ? (splitted[0].length + 1) : 0);
        const lastOne = lineas.length - 1;
        const lastVal = lineas[lastOne], wordL = word.length;
        const lastIter = i == (arr.length - 1);
        const sep = (continued && word_.includes(':') ? (numeric && preserveBetweenSeparator ? (lastIter && isLast ? '-y-' : '--') : (unsep ? '' : (/* ';' */'-y-'))) : ',');
        if (wordL && wordL > limite) {
            const lastMenor = !isText && lastVal && ((lastVal.length - bldfs.length) < limite);
            if (i) {
                if (isText) lineas[lastOne] = applyBreak(lastVal);
                else if (lastMenor) 
                    lineas = linesShouldBreak(
                        lineas, 
                        true, 
                        limite, 
                        lastVal, 
                        bldfs, 
                        isText, 
                        sep, 
                        lastIter, 
                        lastOne
                    );
            }

            const linex = recur((!isText && i && !lastMenor ? sep : '') + word, limite, lastIter, bldfs, isText);
            lineas.push(...linex);
        }
        else if (wordL) {
            const sliceStart = isText ? (`${bldfs}` ? 1 : 0) : bldfs.length;
            const separador = isText ? ' ' : sep;
            const newWord = (i ? ((lastVal?.slice(sliceStart) || '') + separador) : '') + word;
            const pos = lastOne > -1 ? lastOne : 0;
            if (newWord.length > limite) 
                lineas = linesShouldBreak(
                    lineas, 
                    false, 
                    limite, 
                    lineas && lineas[pos] || null, 
                    bldfs, 
                    isText, 
                    sep, 
                    lastIter, 
                    pos, 
                    word
                );
            else lineas[pos] = isText ? boldBreak(newWord, bldfs, lastIter) : `${bldfs}${newWord}`;
        }
        return {...lineasDta, lineas}
    }, { lineas: [], limite: limit });

    return lineas
};

export const pv2json = (data) => {
    const jsn = {
        tokenUsuario: data.tokenUsuario,
        codigoPunto: data.betCenter,
        dfRif: window.btoa(data.dfRif),
        dfRazonSocial: window.btoa(data.dfBusinessName),
        dfnombrePunto: window.btoa(data.dfPointOfSaleName),
        dfEstado: data.dfState,
        dfCiudad: data.dfCity,
        dfMunicipio: data.dfMunicipality,
        dfDireccion: window.btoa(data.dfAddress),
        dfAvCalle: window.btoa(data.dfAvStreet),
        dfUrbanizacion: window.btoa(data.dfUrbanization),
        dfLocal: window.btoa(data.dfLocalOffice),
        dfReferencia: window.btoa(data.dfReferencePoint),
        dftipo: data.dfPointType,
        dgUrbanizacion: window.btoa(data.dgUbanization),
        dgAvCalle: window.btoa(data.dgAvStreet),
        dgReferencia: window.btoa(data.dgReferencePoint),
        dgEstado: data.dgState, 
        dgCiudad: data.dgCity,
        dgMunicipio: data.dgMunicipality,
        dgLocal: window.btoa(data.dgLocalOffice),
        dgTelefono1: window.btoa(data.dgPhone1),
        dgTelefono2: window.btoa(data.dgPhone2),
        dglimiteVenta: data.dgLimitOfSell,
        dglistero: data.dgLister,
        dgEstatus: window.btoa(data.dgEstatus),
        dgTipoFondo: window.btoa(data.dgTipoFondo),
        dgEmail : window.btoa(data.dgEmail),
        cNombreContacto: window.btoa(data.cName),
        cApellidoContacto: window.btoa(data.cLastName),
        cCedulaContacto: window.btoa(data.cIdetificationCard),
        cTelefono1: window.btoa(data.cPhone1),
        cTelefono2: window.btoa(data.cPhone2),
        cEmail : window.btoa(data.cEmail),
        listaEsquema: data.enEsquemaNegociacionData.map(esq => ({codigoEsquema: esq})),
        listaSubGrupo: data.sgSubGrupoData.map(sgr => ({codigoSubGrupo: sgr})),
      };

    return jsn
};

export const resp2pv = (rjson) => {
    const rdatos = rjson.datos[0];
    return new PuntoVenta({
        dfRif: window.atob(rdatos.dfRif),
        dfBusinessName: window.atob(rdatos.dfRazonSocial),
        dfPointOfSaleName: window.atob(rdatos.dfnombrePunto),
        dfState: rdatos.dfEstado,
        dfCity: rdatos.dfCiudad,
        dfMunicipality: rdatos.dfMunicipio,
        dfAddress: window.atob(rdatos.dfDireccion),
        dfAvStreet: window.atob(rdatos.dfAvCalle),
        dfUrbanization: window.atob(rdatos.dfUrbanizacion),
        dfLocalOffice: window.atob(rdatos.dfLocal),
        dfReferencePoint: window.atob(rdatos.dfReferencia),
        dfPointType: rdatos.dftipo,
        //
        dgUbanization: window.atob(rdatos.dgUrbanizacion),
        dgAvStreet: window.atob(rdatos.dgAvCalle),
        dgReferencePoint: window.atob(rdatos.dgReferencia),
        dgState: rdatos.dgEstado,
        dgCity: rdatos.dgCiudad,
        dgMunicipality: rdatos.dgMunicipio,
        dgLocalOffice: window.atob(rdatos.dgLocal),
        dgPhone1: window.atob(rdatos.dgTelefono1),
        dgPhone2: window.atob(rdatos.dgTelefono2),
        dgLimitOfSell: `${rdatos.dglimiteVenta}`,
        dgLister: rdatos.dglistero,
        dgEstatus: window.atob(rdatos.dgEstatus),
        dgTipoFondo: window.atob(rdatos.dgTipoFondo),
        dgEmail: rdatos.dgEmail && window.atob(rdatos.dgEmail) || '',
        //
        cName: window.atob(rdatos.cNombreContacto),
        cLastName: window.atob(rdatos.cApellidoContacto),
        cIdetificationCard: window.atob(rdatos.cCedulaContacto),
        cPhone1: window.atob(rdatos.cTelefono1),
        cPhone2: window.atob(rdatos.cTelefono2),
        cEmail: window.atob(rdatos.cEmail),
        // 
        enEsquemaNegociacionData: rjson.listaEsquema.map(esq => esq.Codigo),
        sgSubGrupoData: rjson.listaSubGrupo.map(subg => subg.Codigo),
    })
};