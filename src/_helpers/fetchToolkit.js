import { useAuth } from "@/store/auth";
import { logOutSession, msgError, resCodes } from "./general";

export const caseWrong = ({ self, error, passMsg = false, defaultMsg = '', loading = '', boolError = false, resetVals = false, touchVal = '', notAlert = false }) => {
    if (loading) self[loading] = false;
    if (!boolError || error) {
        console.error(error || defaultMsg);
        if ((!boolError || passMsg) && self && self.fireToastMsg && !notAlert) self.fireToastMsg(!boolError ? (passMsg ? error : defaultMsg) : error, { icon: 'info' });
    }
    if (resetVals) self.v$.$reset();
    if (touchVal) self.v$[touchVal].$touch();
    return false;
};

/**
 * si se utiliza fetchRequest(...) sin await, se ejecutaran las siguientes lineas de codigo sin esperar que este metodo finalice su ejecucion
 * si se utiliza await fetchRequest(...), las siguientes lineas de codigo se ejecutaran al terminar la ejecucion este metodo
 */
export const fetchRequest = async ({ 
    method = 'GET', 
    api, 
    mock = '',
    self = null,
    notAuthSist = false,
    success = () => {}, 
    error = () => {}, 
    done = () => {},
    simple = false, 
    body = {},
    headers = {},
    fromLogOut = false,
    isLogIn = false,
    promiseReturn = false,
    applyMock = false,
    appCode = false,
}) => {
    const doMock = false; // true para trabajar con data estatica, false para data dinamica
    const codigoSoftware = 'MDE=';
    const hasBody = !['GET','DELETE'].includes(method);

    if (self) self.doingFetch = true;
    const { auth_tokenSistema, auth_tiempoSistema } = useAuth();
    // for mocking
    const theMock = () => {
        if (mock && (doMock || applyMock)) {
            let theMockU;
            switch (mock) {
                case 'gralIniciarSesion': theMockU = require('@/assets/jsons-prueba/gralIniciarSesion.json'); break;
                case 'lotoIniciarBloqueo':
                case 'lotoIniciarLoteria': {
                    const { resLoto } = require('@/assets/jsons-prueba/lotoIniciarLoteria.js'); 
                    theMockU = resLoto(mock === 'lotoIniciarLoteria');
                    break;
                }
                case 'userCerrarSesion': theMockU = require('@/assets/jsons-prueba/userCerrarSesion.json'); break;
                case 'userIniciarSesion': theMockU = require('@/assets/jsons-prueba/userIniciarSesion.json'); break;
                case 'userMantenerSesion': theMockU = require('@/assets/jsons-prueba/userMantenerSesion.json'); break;
                default: theMockU = null; break;
            }
    
            return new Promise((resolve) => {
                setTimeout(() => {resolve(theMockU)}, 2000);
            });
        }
        else return null;
    };
    // 
    const theFetch = async () => {
        try {
            const res = await fetch(...[`${api}${appCode && !hasBody ? ('/'+codigoSoftware) : ''}`, {
                method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    ...(!notAuthSist ? {
                        'Authorization': 'Bearer ' + auth_tokenSistema,
                    } : {}),
                    ...headers
                },
                ...(hasBody ? {
                    body: JSON.stringify({...body, ...(appCode ? { codigoSoftware } : {})})
                } : {})
              }]);
            const resJson = await res.json();
            return resJson;
        } catch (error) {
            return error;
        }
    };

    const shouldLogOut = auth_tiempoSistema && ((new Date().getTime()) >= +auth_tiempoSistema);
    const rjson = shouldLogOut && !fromLogOut && !isLogIn ? null : (await theMock() || await theFetch());

    if (done) done();

    if ((shouldLogOut || (self && rjson && resCodes(rjson).includes('030'))) && !fromLogOut && !isLogIn) {
        const msg = (!shouldLogOut && msgError(rjson)) /* || 'Sesión no activa, inicie nuevamente.' */;
        let notAlert;
        if (self && self.fireToastMsg) {
            notAlert = true;
            if (msg) self.fireToastMsg(msg, {});
        }
        logOutSession(self, false, !shouldLogOut, true, notAlert);
        if (promiseReturn) return null;
    }
    else if ((simple && rjson) || (rjson && resCodes(rjson).includes('000')) || (rjson && rjson?.id)) {
        const rjson_ = { ...rjson, mock: mock && (doMock || applyMock) };
        if (promiseReturn && success) return success(rjson_) ?? null;
        else if (success) success(rjson_);
    }
    else {
        const rJson = { ...rjson, msg: msgError(rjson) };
        if (promiseReturn && error) return error(rJson) ?? null;
        else if (error) error(rJson);
    }
};
/* 
fetchRequest({ 
    api: '', 
    success: (rjson) => {
        //...
    }, 
    error: (rjson) => {
        //...
        const msg = rjson?.error;
        // const msg = rjson?.mensaje?.description || rjson?.message?.description;
        return caseWrong({
            self, error: msg || rjson, passMsg: Boolean(msg),
            defaultMsg: labels.defaultErrorMsg,
        });
    },
});
*/


// METODO GENERICO QUE PUEDE SERVIR COMO TOTALMENTE ENCAPSULADO, PARA PODER SER REUTILIZADO EN MAS PROYECTOS
/* const genericGetByParams = async ({
    params = {},
    url = '',
    execute = () => {}
}) => {
    const urlTrim = url.replaceAll(' ','');
    const uri = urlTrim.match(/[^\{]+(?=\})/g).reduce((newUri, param) => newUri.replace('{'+param+'}', params[param]), `${urlTrim}`);
    execute({ loading: true });
    const resp = await fetch(uri);
    const json = await resp.json();
    execute({ loading: false });
    return json
}; */
// EJEMPLO DE USO
/* await genericGetByParams({ 
    url: 'https://rickandmortyapi.com/api/character/{id}', 
    params: { id: '233' }, 
    execute: ({loading}) => { console.log('loading',loading) } 
}); */