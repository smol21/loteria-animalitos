import { EsquemaNegociacion, GrupoListero, PuntoVenta, SubGrupo, TaquillaSistAdministrativo } from "./classes";

export const testEsquemaData = () => {
    const rnd = Math.random().toString().split('.');
    const tableData = [['Beneficio','1',true], ['Beneficio 2','2',false], ['Beneficio 3','3',true]].map((item, i) => new EsquemaNegociacion({ 
        id: `${i}`,
        guid: `${i}`+(new Date().getTime())+(rnd[1] || rnd.join('')),
        description: item[0],
        applicate: item[1],
        status: item[2],
        sales: '5',
        // category: '1',
        // operation: '01',
        utility: null,
        participation: '1',
        utilityFreq: null,
        participationFreq: 'S',
    }));
    return tableData
};

export const testTaquillaData = () => {
    const rnd = Math.random().toString().split('.');
    const tableData = [['1',true,'01','1','Agencia 1'], ['2',false,'02','3','Agencia 2'], ['3',true,'03','1','Agencia 3']].map((item, i) => new TaquillaSistAdministrativo({ 
        serial: `${i}`+(new Date().getTime())+(rnd[1] || rnd.join('')),
        guid: `${i}`+(new Date().getTime())+(rnd[1] || rnd.join('')),
        agency: item[0],
        agencyName: item[4],
        status: item[1],
        ticketStore: item[2],
        betCenter: item[3],
    }));
    return tableData
};

// COMPRADOR COMERCIAL
export const listeros = [
    new GrupoListero({
        dfRif: 'Q0MwMDk2', // CC0096
        dfName: 'TELE RECARGAS JR C.A',
        dfAddress: 'Direccion Listero',
        dfPhone1: '04141234567',
        dfPhone2: '04241234567',
        // 
        cName: 'Contacto',
        cLastName: 'Prueba',
        cIdetificationCard: '87654321',
        cPhone1: '04147654321',
        cPhone2: '04247654321',
        // 
        dgState: '1',
        dgCity: '1',
        dgMunicipality: '1',
        dgEmail: 'email@mail.com',
        dgAssociatedType: '1',
        dgAssociated: '1',
        dgListerType: '1',
        dgStatus: '1',
        dgPaymentFrequency: '1',
        dgRiskFactor: '1',
        dgTicketCaducity: '2',
        dgValidationCaducity: '3',
        dgApprovalRequired: true,
        // 
        enEsquemaNegociacionData: ['1'],
    }),
];

export const puntosDeVenta = [
    new PuntoVenta({
        dfRif: 'V-23456789-1',
        dfBusinessName: 'business name',
        dfPointOfSaleName: 'Le point du vent',
        dfState: 'MTM=', // codigo de lara
        dfCity: 'OQ==', // codigo de barquisimeto
        dfMunicipality: 'Mw==', // codigo de iribarren
        dfAddress: 'direccion punto de venta',
        dfAvStreet: 'calle / avenida',
        dfUrbanization: 'la urbe',
        dfLocalOffice: 'local u oficina',
        dfReferencePoint: 'Punto de referencia',
        dfPointType: 'MDE=',
        //
        dgUbanization: 'la urbanizacion',
        dgAvStreet: 'avenida o calle',
        dgReferencePoint: 'Punto de referencia',
        dgState: 'MjI=', // codigo de yaracuy
        dgCity: 'NQ==', // codigo de cocorote
        dgMunicipality: 'NA==', // codigo de cocorote
        dgLocalOffice: 'local u oficina',
        dgPhone1: '04147654321',
        dgPhone2: '04247654321',
        dgLimitOfSell: '1',
        dgLister: 'Q0MwMDk2',
        dgEstatus: 'MQ==',
        dgTipoFondo: 'Mg==',
        dgEmail: 'email@mail.com',
        //
        cName: 'Contacto',
        cLastName: 'Prueba',
        cIdetificationCard: '87654321',
        cPhone1: '04147654321',
        cPhone2: '04247654321',
        cEmail: 'email@mail.com',
        // 
        enEsquemaNegociacionData: ['MDAx'],
        sgSubGrupoData: ['J-98765432-1'],
    }),
];

export const subGrupos = [
    new SubGrupo({
        rif: 'J-98765432-1',
        guid: 'J-98765432-1',
        name: 'Grupo Uno',
        phone1: '04148910111',
        phone2: '04142131415',
        rName: 'Responsable',
        rLastName: 'Uno',
        email: 'grupo@mail.com',
        state: 'MTM=', // codigo de lara
        city: 'OQ==', // codigo de barquisimeto
        municipality: 'Mw==', // codigo de iribarren
        status: '1',
        lister: 'Q0MwMDk2',
        // 
        esquemaNegociacionData: ['MDAz'],
    }),
];