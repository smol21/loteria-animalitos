#!/usr/bin/env bash
# si se trabaja con casafortuna --1-- va antes que --2--
sed -i -e 's\"https://maquinaazul.com:8443/";\"http://casafortuna.com.ve:8041/";\g' $(pwd)/src/main.js #--2--
sed -i -e 's\"https://maquinaazul.com:8443/";\"http://192.168.200.226:8041/";\g' $(pwd)/src/main.js #--1--
sed -i -e 's\blockDevs = true\blockDevs = false\g' $(pwd)/src/main.js
sed -i -e 's\A1: "eXJHeW95TWNYOFVYVkVmcA=="\A1: "MXFUV1htSEhRTExiUVBYUw=="\g' \
    -e 's\A2: "ZGY2WjNsZENHTmNsVjUyOGNjNTRwbnlvRWZud05EY0U="\A2: "TWpIZWsxSlpHcFdXOWFzS2dISUozS1pJUk9hMmxPRnk="\g' \
    -e 's\A3: "aHR0cHM6Ly9tYXF1aW5hYXp1bC5jb20="\A3: "bG9jYWxob3N0OjUxMzI2"\g' $(pwd)/src/views/pages/Login.vue