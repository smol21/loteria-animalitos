#!/usr/bin/env bash
# si se trabaja con casafortuna --2-- va antes que --1--
sed -i -e 's\"http://192.168.200.226:8041/";\"https://maquinaazul.com:8443/";\g' $(pwd)/src/main.js #--1--
sed -i -e 's\"http://casafortuna.com.ve:8041/";\"https://maquinaazul.com:8443/";\g' $(pwd)/src/main.js #--2--
sed -i -e 's\blockDevs = false\blockDevs = true\g' $(pwd)/src/main.js
sed -i -e 's\A1: "MXFUV1htSEhRTExiUVBYUw=="\A1: "eXJHeW95TWNYOFVYVkVmcA=="\g' \
    -e 's\A2: "TWpIZWsxSlpHcFdXOWFzS2dISUozS1pJUk9hMmxPRnk="\A2: "ZGY2WjNsZENHTmNsVjUyOGNjNTRwbnlvRWZud05EY0U="\g' \
    -e 's\A3: "bG9jYWxob3N0OjUxMzI2"\A3: "aHR0cHM6Ly9tYXF1aW5hYXp1bC5jb20="\g' $(pwd)/src/views/pages/Login.vue