﻿/// <reference path="./_imports.js" />


/*!
Uri.js
Manejo de uri para WebPrint
*/
var uri = (function (window, document, undefined) {

    function uri(uriString) {
        this.scheme = "";//1
        this.host = "";//2
        this.port = 0;//4
        this.path = "";//5
        this.query = "";//7
        this.fragment = "";//8

        this._init(uriString);
    }

    function _defaultPort(scheme) {
        return scheme === "http" ? 80 : scheme === "https" ? 443 : 0;
    }

    uri.prototype._init = function (uriString) {
        var patt = "^([a-z][a-z0-9\\+\\-\\.]*)://([a-z0-9\\-\\._~]+)(:([0-9]+))?($|((?:/[a-z0-9\\-\\._~]+)*/?))(\\?([a-z0-9\\-\\._~=&]+))?(\\#([a-z0-9\\-\\._~]+))?";
        var rx = RegExp(patt, "ig");
        var matches = rx.exec(uriString);

        this.scheme = matches[1] || "";
        this.host = matches[2] || "";
        this.port = parseInt(matches[4] || _defaultPort(this.scheme), 10);
        this.path = matches[5] || "";
        this.query = matches[7] || "";
        this.fragment = matches[9] || "";
    };

    uri.prototype.getUrl = function () {
        return this.scheme + "://" + this.host + ":" + this.port +
            (this.path !== "" ? this.path : "") +
            (this.query !== "" ? this.query : "") +
            (this.fragment !== "" ? this.fragment : "");
    };


    return uri;

})(document, window);

