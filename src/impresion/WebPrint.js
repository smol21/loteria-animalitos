/*!
Uri.js
Manejo de uri para WebPrint
*/
class Uri {
    constructor(uriString) {
      this.scheme = ""; //1
      this.host = ""; //2
      this.port = 0; //4
      this.path = ""; //5
      this.query = ""; //7
      this.fragment = ""; //8
      this._init(uriString);
    }
  
    _init(uriString) {
      const patt =
        "^([a-z][a-z0-9\\+\\-\\.]*)://([a-z0-9\\-\\._~]+)(:([0-9]+))?($|((?:/[a-z0-9\\-\\._~]+)*/?))(\\?([a-z0-9\\-\\._~=&]+))?(\\#([a-z0-9\\-\\._~]+))?";
      const rx = new RegExp(patt, "ig");
      const matches = rx.exec(uriString);
      this.scheme = matches[1] || "";
      this.host = matches[2] || "";
      this.port = parseInt(matches[4] || this._defaultPort(this.scheme), 10);
      this.path = matches[5] || "";
      this.query = matches[7] || "";
      this.fragment = matches[9] || "";
    }
  
    _defaultPort(scheme) {
      return scheme === "http" ? 80 : scheme === "https" ? 443 : 0;
    }
  
    getUrl() {
      return (
        this.scheme +
        "://" +
        this.host +
        ":" +
        this.port +
        (this.path !== "" ? this.path : "") +
        (this.query !== "" ? this.query : "") +
        (this.fragment !== "" ? this.fragment : "")
      );
    }
  }
  
  /*!
  WebPrint.Core.js
  Manejo de impresion
  */
  class WebPrint {
    constructor() {
      this.observers = [];
      this.interval = null;
      this.isUpdating = false;
      this.lastStatus = -2;
      this.statusUrl = "http://localhost:8887/Printer/Status";
      this.printUrl = "http://localhost:8887/Printer/Print";
      this.settings = {}; // Agregar esta línea
      this.messages = {};
    }
  
    tick() {
      if (this.isUpdating === true) return;
      this.isUpdating = true;
      if (this.lastStatus < 0)
        this.fire(new PrinterStatus(-1, this.messages.updating));
      this.getStatus((st) => {
        this.isUpdating = false;
        if (st.Status !== this.lastStatus) {
          this.lastStatus = st.Status;
          this.fire(st);
        }
      });
    }
  
    fire(o) {
      this.observers.forEach((item) => {
        item.call(this, o);
      });
    }
  
    init() {
      this.interval = setInterval(this.tick.bind(this), 500);
      return this;
    }
  
    getJson(url, callback, counter) {
      if (counter === 0) {
        clearInterval(this.interval);
        const result = new PrinterStatus(-1, this.messages.noWebPrint);
        callback(result);
        return;
      }
      const u = new Uri(url);
      $.ajax({
        url: u.getUrl(),
        crossDomain: true,
        dataType: "json",
      })
        .done((data) => {
          callback(data, u);
        })
        .fail(() => {
          u.port++;
          this.getJson(u.getUrl(), callback, this.settings.discover ? counter - 1 : 0);
        });
    }
  
    post(url, data, callback, counter) {
      if (counter === 0) return;
      const u = new Uri(url);
      const self = this;
      $.ajax({
        url: u.getUrl(),
        type: "POST",
        data: data,
        crossDomain: true,
      })
        .fail(() => {
          u.port++;
          self.post(u.getUrl(), data, callback, self.settings.discover ? counter - 1 : 0);
        })
        .done(() => {
          if (typeof callback === "function") callback(u);
        });
    }
  
    setStatusUrl(value) {
      this.statusUrl = value;
    }
  
    setPrintUrl(value) {
      this.printUrl = value;
    }
  
    onStatusChange(fn) {
      if (typeof fn !== "function") return;
      this.observers.push(fn);
    }
  
    unsuscribe(fn) {
      if (typeof fn !== "function") return;
      this.observers = this.observers.filter((item) => {
        if (item !== fn) return item;
      });
    }
  
    print(data) {
      data = decodeURIComponent(data);
      this.post(this.printUrl, data, (u) => {
        if (u !== undefined) this.printUrl = u.getUrl();
      }, this.settings.retryCount);
    }
  
    getStatus(callback) {
      if (callback === undefined) return;
      this.getJson(this.statusUrl, (st, u) => {
        if (u !== undefined) this.statusUrl = u.getUrl();
        callback(st);
      }, this.settings.retryCount);
    }
  }
  
  class PrinterStatus {
    constructor(status, description) {
      this.Status = status;
      this.Description = description;
    }
  }
  
  WebPrint.messages = {
    noWebPrint: "No se ha detectado WebPrint",
    updating: "Buscando WebPrint",
  };
  
  WebPrint.settings = {
    discover: true,
    retryCount: 10,
  };
  
  export { Uri, WebPrint, PrinterStatus };