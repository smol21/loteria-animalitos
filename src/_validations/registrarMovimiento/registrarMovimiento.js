import { cNotSelect, cRequired, cMaxLength, cMinLength, cNumberAndDecimals } from '@/_validations/validacionEspeciales';

export default function(val,pago,mov) {
        if(val == 24){
            return{
                movimiento: { required: cRequired },
                compradorc: { required: cRequired },
                fecha: { required: cRequired },
                formPago: { required: cRequired },
                moneda: { required: cRequired },
                referencia: { ...(pago != 'Mw==' ? {required: cRequired, maxlength: cMaxLength(20), minlength:cMinLength(1)} : {}) },
                monto: { required: cRequired, maxlength: cMaxLength(15), numberAndDecimals: cNumberAndDecimals(2) },
                observacion: { maxlength: cMaxLength(50), minlength:cMinLength(1) },
                }
        }else if(val === 23){
            return{
                movimiento: { required: cRequired, notSelect: cNotSelect('opción', true) },
                fecha: { required: cRequired },
                formPago: { required: cRequired, notSelect: cNotSelect('opción', true) },
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                referencia: { ...(pago != 'Mw==' ? {required: cRequired, maxlength: cMaxLength(20), minlength:cMinLength(1)} : {}) },
                monto: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
                observacion: { maxlength: cMaxLength(50), minlength:cMinLength(1) },
                // centroApuesta:{required: cRequired, notSelect: cNotSelect('opción', true)},
                centroApuesta:{...(mov === '3' ? {required: cRequired, notSelect: cNotSelect('opción', true)} : {})}
            }
        }
        else if(val === 22){
            return{
                movimiento: { required: cRequired, notSelect: cNotSelect('opción', true) },
                fecha: { required: cRequired },
                formPago: { required: cRequired, notSelect: cNotSelect('opción', true) },
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                referencia: { ...(pago != 'Mw==' ? {required: cRequired, maxlength: cMaxLength(20), minlength:cMinLength(1)} : {}) },
                monto: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
                observacion: { maxlength: cMaxLength(50), minlength:cMinLength(1) },
                grupo:{ ...(mov === '4' ? {required: cRequired, notSelect: cNotSelect('opción', true)} : {}) },
                centroApuesta:{...(mov === '3' ? {required: cRequired, notSelect: cNotSelect('opción', true)} : {})}
            }
        }else if(val === 0){
            return{
                movimiento: { required: cRequired, notSelect: cNotSelect('opción', true) },
                fecha: { required: cRequired },
                formPago: { required: cRequired, notSelect: cNotSelect('opción', true) },
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                referencia: { ...(pago != 'Mw==' ? {required: cRequired, maxlength: cMaxLength(20), minlength:cMinLength(1)} : {}) },
                monto: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
                observacion: { maxlength: cMaxLength(50), minlength:cMinLength(1) },
                // grupo:{ ...(mov === '4' ? {required: cRequired, notSelect: cNotSelect('opción', true)} : {}) },
            }
        }
    }
    
