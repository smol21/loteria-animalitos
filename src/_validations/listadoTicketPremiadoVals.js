import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (value) => ({
    fechaDesde: { required: cRequired },
    fechaHasta: { required: cRequired },
    taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
    tipoMoneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
    ...(value === 24 ? { comprador: {required: cRequired, notSelect: cNotSelect('opción', true)} } : {}),
    ...([22,23,24].includes(value) ? { puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)} } : {}),
})
