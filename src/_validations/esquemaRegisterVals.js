import {
  cRequired,
  cNotSelect,
  cNumeric,
	cMaxLength,
  cNumberLetterSpecialNegociacion,
} from '@/_validations/validacionEspeciales'
import { helpers } from '@vuelidate/validators'

export default function () {
  return {
    description: { 
      required: cRequired,
      maxlength: helpers.withMessage('Máximo de carácteres permitidos: 50', cMaxLength(50)),
      cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion) 
     },
    formaDePremiacion: {
      required: cRequired,
      notSelect: cNotSelect('opción', true),
    },
    entidad: { required: cRequired, notSelect: cNotSelect('opción', true) },
    utilityFreq: { required: cRequired, notSelect: cNotSelect('opción', true) },
    // operation: { required: cRequired, notSelect: cNotSelect('opción', true) },
    utility: {
      cNumeric: helpers.withMessage('Debe ser solo números enteros', cNumeric(true)),
			maxlength: helpers.withMessage('Máximo de carácteres permitidos: 2', cMaxLength(2)),                 
    },
    participation: {
      cNumeric: helpers.withMessage('Debe ser solo números enteros', cNumeric(true)),
			maxlength: helpers.withMessage('Máximo de carácteres permitidos: 2', cMaxLength(2)),           
    },
    sales: { 
			cNumeric: helpers.withMessage('Debe ser solo números enteros', cNumeric(true)),
			maxlength: helpers.withMessage('Máximo de carácteres permitidos: 2', cMaxLength(2)),            
		},
    //utilityFreq: { requiredUnlessParticipation: cRequiredUnless(participation), notSelect: cNotSelect('opción', true) },
    //participationFreq: { requiredUnlessUtility: cRequiredUnless(utility), notSelect: cNotSelect('opción', true) },
    // cc: {  required: cRequired, notSelect: cNotSelect('opción', true) },
    cc: { required: cRequired },
    tipoJugada: {},
  }
}
