import { required, helpers } from '@vuelidate/validators'
import { cNumeric,cMaxLength, cMinLength } from '../validacionEspeciales'
export default function () {
  return {
    serialInterno: {
      cNumeric: helpers.withMessage('Solo números', cNumeric(true)),
      required: helpers.withMessage('Campo requerido', required),
      maxLength: cMaxLength(5),
      minLength: cMinLength(5)
    },
    serial: {
      cNumeric: helpers.withMessage('Solo números', cNumeric(true)),
      required: helpers.withMessage('Campo requerido', required),
      maxLength: cMaxLength(12),
      minLength: cMinLength(12),
    },
  }
}

