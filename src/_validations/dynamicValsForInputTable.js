import { cRequired, cNumberAndDecimals } from '@/_validations/validacionEspeciales';
import { helpers } from '@vuelidate/validators';

export default (valsAttrs, attrVals) => ({
    tableData: {
        $each: helpers.forEach({
        ...valsAttrs.reduce((attrs, attr) => {
            const theVals = attrVals[attr] || attrVals.a_l_l || [];
            return {
                ...attrs, 
                [attr]: { 
                    ...(theVals.includes(1) ? {required: cRequired}:{}), 
                    ...(theVals.includes(2) ? {numberAndDecimals: cNumberAndDecimals(2)}:{}),
                }
            }
        }, {})
        })
    }
})