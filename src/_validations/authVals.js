import { cLogin, cPasswordRgx, cRequired,cMaxLength, cMinLength } from '@/_validations/validacionEspeciales';

export default {
    user: { required: cRequired, login: cLogin },
    password: { required: cRequired, passwordRgx: cPasswordRgx, maxlength: cMaxLength(12), minlength:cMinLength(6) },
}