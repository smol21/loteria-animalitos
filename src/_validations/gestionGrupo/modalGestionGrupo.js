import { cCredentials, cEmail, cNotSelect, cRequired, cNumeric, cMinLength, cMaxLength ,cAlphaNumSpace, cNumberAndDecimals, cNumberLetterSpecial, cOnlyLettersSpecial, cNumberLetterSpecialNegociacion } from '@/_validations/validacionEspeciales';
import { helpers } from "@vuelidate/validators";

export default function(value){
    if(value === 24){
        return{
            comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nombreCentro: {required: cRequired, maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			nombreGrupo: {required: cRequired, maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
            rif: {required: cRequired, credentials: cCredentials(8)},
            grupo: {notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 0412000000', cNumeric(true)), minLength: cMinLength(11), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail, maxlength: cMaxLength(60)},
            estatus: {notSelect: cNotSelect('opción', true)},
            direccion: {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)},
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
            tipoGrupo: {notSelect: cNotSelect('opción', true)},
            gradosLatitud: {numeric: cNumeric(true)},
            minutosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            gradosLongitud: {numeric: cNumeric(true)},
            minutosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(11)},
      
            condicion: {required: cRequired},
            
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ numberAndDecimals: cNumberAndDecimals(2),  maxlength: cMaxLength(11) },
      
            cupo:{required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11) },
      
            minimo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
            maximo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },

            aplicarTodos: { required: cRequired }
          }
    }else{
        return {
            nombreCentro: {required: cRequired, maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			nombreGrupo: {required: cRequired, maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
            rif: {required: cRequired, credentials: cCredentials(8)},
            grupo: {required: cRequired, notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 0412000000', cNumeric(true)), minLength: cMinLength(11), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail, maxlength: cMaxLength(60)},
            estatus: {notSelect: cNotSelect('opción', true)},
            direccion: {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)},
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
            tipoGrupo: {notSelect: cNotSelect('opción', true)},
            gradosLatitud: {numeric: cNumeric(true)},
            minutosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            gradosLongitud: {numeric: cNumeric(true)},
            minutosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(11)},
      
            condicion: {required: cRequired},
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ numberAndDecimals: cNumberAndDecimals(2),  maxlength: cMaxLength(11) },
      
            cupo:{required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11) },
      
            minimo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
            maximo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },

            aplicarTodos: { required: cRequired }
        }
    }
    
}