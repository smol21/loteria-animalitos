import { cNotSelect, cRequired, /*cMaxLength, cNumberAndDecimals*/ } from '@/_validations/validacionEspeciales';

export default function(val) {
        if(val == 24){
            return{
                compradorc: { required: cRequired },
				grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
				estatus: {required: cRequired, notSelect: cNotSelect('opción', true)}
                }
        }else if(val === 23){
            return{
                compradorc: { required: cRequired },
				grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
				estatus: {required: cRequired, notSelect: cNotSelect('opción', true)}
            }
        }
        else if(val === 22){
            return{
                compradorc: { required: cRequired },
				grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
				estatus: {required: cRequired, notSelect: cNotSelect('opción', true)}
            }
        }else if(val === 0){
            return{
                compradorc: { required: cRequired },
				grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
				estatus: {required: cRequired, notSelect: cNotSelect('opción', true)},
            }
        }
    }