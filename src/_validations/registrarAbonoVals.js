import { cNumeric,cRequired, cNotSelect } from '@/_validations/validacionEspeciales';
import { helpers ,required, maxLength , minLength} from '@vuelidate/validators'

export default function () {
    return {
        saldo: {
          cNumeric: helpers.withMessage('Solo números', cNumeric(true)),
          required: helpers.withMessage('Campo requerido', required),
        },
        tipoAbono: { required: cRequired, notSelect: cNotSelect('opción', true) },
        banco: { required: cRequired, notSelect: cNotSelect('opción', true) },
        cuentaDestino: { required: cRequired, notSelect: cNotSelect('opción', true) },
        referencia: {
          cNumeric: helpers.withMessage('Solo números', cNumeric(true)),
          required: helpers.withMessage('Campo requerido', required),
          minLength: helpers.withMessage('Mínimo 1 carácteres', minLength(1)),
          maxLength: helpers.withMessage('Máximo 20 carácteres', maxLength(20)),
      },
      fechaPago: { required: cRequired },
      montoAbono: {
        cNumeric: helpers.withMessage('Solo números', cNumeric(true)),
        required: helpers.withMessage('Campo requerido', required),
      },
      observacion: {
        required: helpers.withMessage('Campo requerido', required),
        minLength: helpers.withMessage('Mínimo 3 carácteres', minLength(3)),
      },
      nroTelefono: {
        minLength: helpers.withMessage('Mínimo 12 carácteres', minLength(12)),
        maxLength: helpers.withMessage('Mínimo 12 carácteres', maxLength(12)),
      },
      nroDocumento: {
        minLength: helpers.withMessage('Mínimo 8 carácteres', minLength(8)),
      }
    }
  }