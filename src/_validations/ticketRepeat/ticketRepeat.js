import { required, helpers , maxLength, numeric } from '@vuelidate/validators'
export default function (showItems) {
  return {
    serialInterno: {
      required: helpers.withMessage('Campo requerido', required),
      maxLength: helpers.withMessage('Mínimo 15 carácteres', maxLength(15)),
    },
    serial: {
      required: helpers.withMessage('Campo requerido', required),
      maxLength: helpers.withMessage('Mínimo 15 carácteres', maxLength(15)),
    },
    
    radioType: {
      required: helpers.withMessage('Campo requerido', required),
    },
    ...(showItems ? {
      triplebs: {
        required: helpers.withMessage('Campo requerido', required),
        numeric: helpers.withMessage('Solo número', numeric),
        maxLength: helpers.withMessage('Mínimo 10 carácteres', maxLength(10)),
      },
      terminales: {
        required: helpers.withMessage('Campo requerido', required),
        numeric: helpers.withMessage('Solo número', numeric),
        maxLength: helpers.withMessage('Mínimo 10 carácteres', maxLength(10)),
      }
    } : {}),
  }
}

