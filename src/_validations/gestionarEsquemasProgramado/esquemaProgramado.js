import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    entidad: { required: cRequired, notSelect: cNotSelect('opción', true) },
    cc: { required: cRequired, notSelect: cNotSelect('opción', true) },
    centroApuesta: { required: cRequired, notSelect: cNotSelect('opción', true) },
    producto: { required: cRequired, notSelect: cNotSelect('opción', true) },
    condicion_valor: {}
}