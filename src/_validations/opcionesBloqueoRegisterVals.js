import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (active = true) => ({
    ...(active ? {
        typeBlocking: { required: cRequired, notSelect: cNotSelect('opción', true) },
        blockedBy: { required: cRequired },
    } : {}),
})