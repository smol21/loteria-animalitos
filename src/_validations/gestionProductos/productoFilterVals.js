import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    loteria: { required: cRequired, notSelect: cNotSelect('opción', true) },
    operadora: { required: cRequired, notSelect: cNotSelect('opción', true) },
}