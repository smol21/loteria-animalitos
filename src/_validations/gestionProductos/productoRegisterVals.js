import { cMax, cMin, cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (modalVisible, validateSeccion = false, validateComponenteModalidad = false, componenteNumeric = true, minNum = 0, minSimb = 0) => ({
    ...(modalVisible ? {
        loteria: { required: cRequired, notSelect: cNotSelect('opción', true) },
        operadora: { required: cRequired, notSelect: cNotSelect('opción', true) },
        nombre: { required: cRequired },
        nombreResumido: { required: cRequired },
        caducidadTicket: { required: cRequired, min: cMin(1), max: cMax(30) },
        caducidadReintegro: { required: cRequired, min: cMin(1), max: cMax(30) },
        combinaSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
        status: { required: cRequired, notSelect: cNotSelect('opción', true) },
        // 
        ...(validateSeccion ? {
            seccion: {
                nombreSeccion: { required: cRequired },
                tipoSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
                tieneSimbolo: { required: cRequired, notSelect: cNotSelect('opción', true) },
                cantidadTriples: { required: cRequired, min: cMin(minNum) },
                cantidadSimbolos: { required: cRequired, min: cMin(minSimb) },
                statusSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
            }
        } : {}),
        // 
        ...(validateComponenteModalidad ? {
            componenteSeccion: {
                nombreSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
                componente: { required: cRequired, notSelect: cNotSelect('opción', true) },
                longitudCampo: { required: cRequired, min: cMin(1) },
                ordenJerarquia: { required: cRequired, min: cMin(0) },
                nombreEtiqueta: { required: cRequired },
                maxSimbolosLista: { ...(!componenteNumeric ? { required: cRequired } : {}), min: cMin(-1) },
            },
            // 
            modalidadSeccion: {
                nombreSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
                modalidad: { required: cRequired, notSelect: cNotSelect('opción', true) },
                montoMinimo: { required: cRequired, min: cMin(1) },
                multiplo: { required: cRequired, min: cMin(1) },
            },
        } : {}),
    } : {}),
})