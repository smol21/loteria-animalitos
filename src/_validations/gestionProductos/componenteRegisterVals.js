import { cMax, cMin, cNotSelect, cRequired, cUrlSimbolo } from '@/_validations/validacionEspeciales';

export default (modalVisible, isImg = false, max_ = 100) => ({ // max_ 100 por defecto
    ...(modalVisible ? {
        nombre: { required: cRequired },
        tipo: { required: cRequired, notSelect: cNotSelect('opción', true) },
        cantidadElementos: { ...(isImg ? { required: cRequired, min: cMin(1), max: cMax(max_) } : {}) },
        rutaLista: { ...(isImg ? { required: cRequired, urlSimbolo: cUrlSimbolo } : {}) },
    } : {}),
})