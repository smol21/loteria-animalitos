/* eslint-disable */
import { LABELS as labels } from "@/_helpers/labels";
import { helpers, required, integer, requiredUnless, alpha, or, maxLength, minLength } from "@vuelidate/validators";

const cCredentials = () => helpers.withMessage(
    labels.credentials,
    helpers.withParams(
        { type: 'ccredentials' },
        (value) => {
            const rgx = new RegExp(`^[vje]{1}\-[0-9]{7,8}\-[0-9]{1}$`, "gi");
            return rgx.test(value)
        }
    )
);

const cEmail = helpers.withMessage(labels.email, (value) => /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/.test(value));

const cLogin = helpers.withMessage(
    labels.login, 
    helpers.withParams(
        { type: 'clogin' },
        (value) => {
            const rgx = new RegExp("^[^ ][A-Za-z0-9]*$");
            return rgx.test(value)
        }
    )
);

const cMax = (max) => helpers.withMessage(
    ({ $params }) => labels.max($params.max),
    helpers.withParams(
        { type: 'cmax', max },
        (value) => value <= max && !/[,]|[.]/.test(`${value}`)
    )
);

const cMaxLength = (max) => helpers.withMessage(
    ({ $params }) => labels.mustHave(0, $params.max),
    helpers.withParams(
        { type: 'cmaxlength', max },
        maxLength(max)
    )
);

const cMin = (min) => helpers.withMessage(
    ({ $params }) => labels.min($params.min),
    helpers.withParams(
        { type: 'cmin', min },
        (value) => value >= min && !/[,]|[.]/.test(`${value}`)
    )
);

const cMinLength = (min) => helpers.withMessage(
    ({ $params }) => labels.mustHave($params.min),
    helpers.withParams(
        { type: 'cminlength', min },
        minLength(min)
    )
);

const cNotSelect = (item, f) => helpers.withMessage(
    ({ $params }) => labels.mustSelect($params.item, $params.f || false),
    helpers.withParams(
        { type: 'cnotSelect', item, f },
        (value) => value !== 'seleccione'
    )
);

const fNumberAndDecimals = (value, max = null) => { //(value) => /^\d{1,}(\,\d{1,2})?$/.test(value)
    const rgx = new RegExp(`^[0-9]{1,}([.][0-9]{1,${max || ''}})?$`);
    return rgx.test(value)
}

const cNumberAndDecimals = (max) => helpers.withMessage(
    ({ $params }) => labels.numberAndDecimals($params.max),
    helpers.withParams(
        { type: 'cnumberAndDecimals', max },
        (value) => fNumberAndDecimals(value, max)
    )
);

const cNumeric = (int = false) => helpers.withMessage(
    ({ $params }) => labels.numeric($params.int),
    helpers.withParams(
        { type: 'cnumeric', int },
        integer
    )
);

const cOnlyLettersSpecial = helpers.withMessage(labels.onlyLettersSpecial, or(alpha, (value) => /^[a-zA-ZÀ-ÿ\u00f1\u00d1\s]*$/.test(value)));

const caseSelPos = (val) => {
    const mSplit = val?.split(',');
    const val_ = val && !Number(val) && mSplit[1]?.length == 1 && +mSplit[0] >= 0 && +mSplit[1] <= +mSplit[0];
    return val_
};

const cOrdenSeleccion = helpers.withMessage(
    ({ $model }) => labels.ordenSeleccion(caseSelPos($model)),
    helpers.withParams(
        { type: 'cordenSeleccion' },
        (value) => {
            let tested = /^\-?[0-9]{1}\,{1}[1-9]{1}$/g.test(value);
            if (tested) tested = !caseSelPos(value);
            return tested
        }
    )
);

const cPasswordRgx = helpers.withMessage(
    labels.passwordRgx, 
    helpers.withParams(
        { type: 'cpasswordRgx' },
        (value) => {
            const rgx = new RegExp("^[^ ][A-Za-z0-9]*$");
            return rgx.test(value)
        }
    )
);

const cPhoneNumber = helpers.withMessage(
    labels.phoneNumber,
    helpers.withParams(
        { type: 'cphoneNumber' },
        (value) => /^[0-9]{4}\-[0-9]{7}$/.test(value)
    )
);

const cRequired = helpers.withMessage(labels.required, required);

const cRequiredUnless = (value) => helpers.withMessage(
    labels.required,
    requiredUnless(value)
);

const cUrlSimbolo = helpers.withMessage(
    labels.urlSimbolo,
    helpers.withParams(
        { type: 'curlSimbolo' },
        (value) => /^[^{}]+\/\{[^{}]+\}\.(png|jpg|jpeg)$/.test(value)
    )
);

const cAlphaNumSpace = helpers.withMessage(
    labels.alphaNumSpace,
    helpers.withParams(
        { type: 'calphaNumSpace' },
        (value) => {
            const rgx = new RegExp(/^[A-Za-z0-9\s]+$/g);
            return rgx.test(value)
        }
    )
);

const cAlphaNumSpaceYPorce = helpers.withMessage(
    labels.alphaNumSpace,
    helpers.withParams(
        { type: 'calphaNumSpace' },
        (value) => {
            const rgx = new RegExp(/^[A-Za-z0-9\s%.]+$/g);
            return rgx.test(value);
        }
    )
);

const cNumberLetterSpecial = helpers.withMessage(
    labels.alphaNumSpace,
    helpers.withParams(
        { type: 'calphaNumSpace' },
        (value) => {
            const rgx = new RegExp(/^[A-Za-z0-9\s.,)(#\/&+-ñ]+$/);
            const onlyNumbers = /^\d+$/.test(value);

            if(onlyNumbers){
                return false
            }else{
                return rgx.test(value)
            }
            
        }
    )
);

const cNumberLetterSpecialNegociacion = helpers.withMessage(
    labels.alphaNumSpace,
    helpers.withParams(
        { type: 'calphaNumSpace' },
        (value) => {
            const rgx = new RegExp(/^[A-Za-z0-9\s.%&)(]+$/);
            const onlyNumbers = /^\d+$/.test(value);
            const onlyRgx = /^[\s.%&)(]+$/.test(value);
            if(onlyNumbers){
                return false
            }else if(onlyRgx){
                return false
            }
            else{
                return rgx.test(value)
            }
            
        }
    )
);

export { 
    cCredentials,
    cEmail,
    cLogin,
    cMax,
    cMaxLength,
    cMin,
    cMinLength,
    cNotSelect,
    cNumberAndDecimals,
    cNumeric,
    cOnlyLettersSpecial,
    cOrdenSeleccion,
    cPasswordRgx,
    cPhoneNumber,
    cRequired,
    cRequiredUnless,
    cUrlSimbolo,
    cAlphaNumSpace,
    // 
    fNumberAndDecimals,
    cNumberLetterSpecial,
    cAlphaNumSpaceYPorce,
    cNumberLetterSpecialNegociacion,
};