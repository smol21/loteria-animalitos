import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function (modalVisible, mode) {
    return{
        ...(modalVisible && (mode === 'edit' || mode === 'view') ? {
            descripcionHorario: { required: cRequired },
            codigoTurno: { required: cRequired, notSelect: cNotSelect('opción', true) },
            estatusHorario: { required: cRequired, notSelect: cNotSelect('opción', true) },
            // horaDesde: { required: cRequired },
            horaSorteo: { required: cRequired },
            horaActivacion: { required: cRequired },
            horaCierre: { required: cRequired },
        } : {
            descripcionHorario: { required: cRequired },
            codigoTurno: { required: cRequired, notSelect: cNotSelect('opción', true) },
            // estatusHorario: { required: cRequired, notSelect: cNotSelect('opción', true) },
            // horaDesde: { required: cRequired },
            horaSorteo: { required: cRequired },
            horaActivacion: { required: cRequired },
            horaCierre: { required: cRequired },
        })
    }
}