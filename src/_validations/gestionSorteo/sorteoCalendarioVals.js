// import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (modalVisible) => ({ // max_ 100 por defecto
    ...(modalVisible ? {
        numSorteo: {},
        dia: {},
        fecha: {},
        producto: {},
        seccion: {},
        hora: {},
        status: {},
    } : {}),
})