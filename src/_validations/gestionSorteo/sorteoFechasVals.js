import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    mes: { required: cRequired, notSelect: cNotSelect('opción', true) },
    anno: { required: cRequired, notSelect: cNotSelect('opción', true) },
    producto: { required: cRequired, notSelect: cNotSelect('opción', true) },
}