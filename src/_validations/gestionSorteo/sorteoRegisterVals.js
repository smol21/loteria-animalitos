import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (modalVisible) => ({ // max_ 100 por defecto
    ...(modalVisible ? {
        producto: { required: cRequired, notSelect: cNotSelect('opción', true) },
        seccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
        dia: { required: cRequired, notSelect: cNotSelect('opción', true) },
        hora: { required: cRequired, notSelect: cNotSelect('opción', true) },
        turno: {},
        status: { required: cRequired, notSelect: cNotSelect('opción', true) },
    } : {}),
})