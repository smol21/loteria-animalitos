import { cRequired, cNotSelect, cMaxLength, cMinLength } from '@/_validations/validacionEspeciales';

export default function (modalVisible, aux) { // max_ 100 por defecto
    if(modalVisible){
        if(aux === 'edit'){
            return{
            login: { required: cRequired },
            taquilla: { required: cRequired },
            estatusUsuario: { required: cRequired, notSelect: cNotSelect('opción', true) },
            horaDesde: { required: cRequired },
            horaHasta: { required: cRequired },
            }
        }else{
            return{
                codigo: { required: cRequired, maxlength:cMaxLength(6), minLength: cMinLength(6), },
                
            }
        }
    }
}