import { cRequired, cMaxLength, cEmail, cMinLength,cNumeric} from '@/_validations/validacionEspeciales';
import { helpers } from "@vuelidate/validators";

export default function (modalVisible, aux) { // max_ 100 por defecto
    if(modalVisible){
        if(aux === 'edit'){
            return{
            entidad: { required: cRequired },
            login: { required: cRequired },
            telefono: { required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 04120000000', cNumeric(true)), maxlength: cMaxLength(11), minLength: cMinLength(11)},
            correo: { required: cRequired, email: cEmail },
            estatus: { required: cRequired },          
            }
        }else{
            return{                
                codigo: { required: cRequired, maxlength:cMaxLength(6), minLength: cMinLength(6), },
                
            }
        }
    }
}