import { cCredentials, cEmail, cNotSelect, cRequired, cNumeric, cMinLength, cMaxLength, cNumberAndDecimals, cNumberLetterSpecial, cOnlyLettersSpecial, cNumberLetterSpecialNegociacion } from '@/_validations/validacionEspeciales';
import { helpers } from "@vuelidate/validators";

export default function(value,valDire,valNomb){
    if(value === 24){
        return{
            comprador: {},
            nombreCentro: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
            rif: {required: cRequired, credentials: cCredentials()},
            grupo: {notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {...valNomb.length > 0 ? {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial} : {}},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 04120000000', cNumeric(true)), minLength: cMinLength(11), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail, maxlength: cMaxLength(60)},
            estatus: {notSelect: cNotSelect('opción', true)},
            direccion: {...(valDire.length > 0 ? {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)} : {}) },
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
      
            gradosLatitud: {numeric: cNumeric(true)},
            minutosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            gradosLongitud: {numeric: cNumeric(true)},
            minutosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(11)},
      
            condicion: {required: cRequired},
            
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ numberAndDecimals: cNumberAndDecimals(2),  maxlength: cMaxLength(11) },
      
            cupo:{ required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11) },
      
            minimo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
            maximo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },

            aplicarTodos: { required: cRequired },

            impuesto: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2) , maxlength: cMaxLength(11)},
          }
    }else{
        return {
            nombreCentro: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
            rif: {required: cRequired, credentials: cCredentials()},
            grupo: {required: cRequired, notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {...valNomb.length > 0 ? {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial} : {}},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 04120000000', cNumeric(true)), minLength: cMinLength(11), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail, maxlength: cMaxLength(60) },
            estatus: {notSelect: cNotSelect('opción', true)},
            direccion: {...(valDire.length > 0 ? {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)} : {}) },
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
      
            gradosLatitud: {numeric: cNumeric(true)},
            minutosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLatitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            gradosLongitud: {numeric: cNumeric(true)},
            minutosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
            segundosLongitud: {numberAndDecimals: cNumberAndDecimals(2)},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(11)},
      
            condicion: {required: cRequired},
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ numberAndDecimals: cNumberAndDecimals(2),  maxlength: cMaxLength(11) },
      
            cupo:{ required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11) },
      
            minimo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
            maximo:{ required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },

            aplicarTodos: { required: cRequired },
            impuesto: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2) , maxlength: cMaxLength(11)},
        }
    }
    
}