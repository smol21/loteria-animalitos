import {
  cRequired,
  cMaxLength,
  cMinLength,
  cEmail,
  cNumeric,
  cLogin,
} from '@/_validations/validacionEspeciales'
import { helpers, sameAs} from '@vuelidate/validators'

export default function (val, serial) {
  if(serial === '1'){
      return{
        login: {
          required: cRequired,
          maxlength: helpers.withMessage('Máximo de caracteres permitidos: 12', cMaxLength(12)),
          minLength: helpers.withMessage('Mínimo de caracteres permitidos: 6',  cMinLength(6)),
          login: cLogin 
        },
        email: {
          required: cRequired,
          email: helpers.withMessage('Formato Incorrecto', cEmail)
        },
        telefono: {
          required: cRequired,
          numeric: helpers.withMessage('Solo números', cNumeric(true)),
          maxlength: cMaxLength(11),
        }
      }
  }else{
    return {
      newPassword: { required:cRequired, 
        maxlength: helpers.withMessage('Máximo de caracteres permitidos: 12', cMaxLength(12)),
        minLength: helpers.withMessage('Mínimo de caracteres permitidos: 6',  cMinLength(6)),
      },
      passwordConfirmation: { required: cRequired, maxlength: cMaxLength(12), sameAsPassword: helpers.withMessage('Las contraseñas deben coincidir', sameAs(val)),  },
      caducidad: { required: cRequired }
    }
  }
}
