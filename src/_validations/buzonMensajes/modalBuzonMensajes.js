
import { cNotSelect, cRequired,  cMaxLength ,cAlphaNumSpace, cMinLength } from '@/_validations/validacionEspeciales';

export default function(value){
    if(value === 24){
        return{
            comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
			mensaje:  {required: cRequired, maxlength: cMaxLength(500), minlength:cMinLength(10), calphaNumSpace: cAlphaNumSpace},
			recibido:  {required: cRequired, maxlength: cMaxLength(500), minlength:cMinLength(10),  calphaNumSpace: cAlphaNumSpace},
			fecha: { required: cRequired },
			entidad: { },
			grupo: { },
			centroApuesta: { },
			codigoEntidad: { }
          }
    }else{
        return {
			comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
			mensaje:  {required: cRequired, maxlength: cMaxLength(500), minlength:cMinLength(10), calphaNumSpace: cAlphaNumSpace},
			recibido:  {required: cRequired, maxlength: cMaxLength(500), minlength:cMinLength(10),  calphaNumSpace: cAlphaNumSpace},
			fecha: { required: cRequired },
			entidad: { },
			grupo: { },
			centroApuesta: { },
			codigoEntidad: { }
        }
    }
    
}