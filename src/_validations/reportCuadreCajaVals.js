import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    fechaDesde: { required: cRequired },
    fechaHasta: { required: cRequired },
    taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
}