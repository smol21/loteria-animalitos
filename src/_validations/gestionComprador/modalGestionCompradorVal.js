import { cCredentials, cEmail, cNotSelect, cRequired, cNumeric, cMinLength, cMaxLength ,cAlphaNumSpace, cNumberAndDecimals, cNumberLetterSpecial, cOnlyLettersSpecial, cNumberLetterSpecialNegociacion } from '@/_validations/validacionEspeciales';
import { helpers } from "@vuelidate/validators";
export default function(value){
    if(value === 24){
        return{
            comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nombreCentro: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			
			nombreComprador: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			nombreGrupo: {required: cRequired, maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            rif: {required: cRequired, credentials: cCredentials()},
            grupo: {notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 0412000000', cNumeric(true)), minLength: cMinLength(11), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail, maxlength: cMaxLength(60)},
            estatusComprador: {notSelect: cNotSelect('opción', true)},
            direccion: {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)},
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
      
            gradosLatitud: {},
            minutosLatitud: {},
            segundosLatitud: {},
      
            gradosLongitud: {},
            minutosLongitud: {},
            segundosLongitud: {},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2)},
      
            condicion: {required: cRequired},
            
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ maxlength: cMaxLength(12), numberAndDecimals: cNumberAndDecimals(2)},
      
            cupo:{ required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11)  },
      
            minimo:{ required: cRequired },
            maximo:{ required: cRequired },

            aplicarTodos: { required: cRequired },
			cupoVenta: { numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(12) },
          }
    }else{
        return {
            nombreCentro: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			nombreComprador: {required: cRequired, minLength: cMinLength(3), maxlength: cMaxLength(50), cNumberLetterSpecialNegociacion: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .%&)(',cNumberLetterSpecialNegociacion)},
			nombreGrupo: {required: cRequired, maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            rif: {required: cRequired, credentials: cCredentials()},
            grupo: {required: cRequired, notSelect: cNotSelect('opción', true)},
            premiacion: {required: cRequired, notSelect: cNotSelect('opción', true)},
            nomCentroImpresion: {maxlength: cMaxLength(50), calphaNumSpace: cAlphaNumSpace},
            numeroTele: {required: cRequired, numeric: helpers.withMessage('Debe tener un formato correcto, ej: 0412000000', cNumeric(true)), maxlength: cMaxLength(11)},
            correo: {required: cRequired, email: cEmail},
            estatusComprador: {notSelect: cNotSelect('opción', true)},
            direccion: {minLength: cMinLength(3),maxlength: cMaxLength(100), cNumberLetterSpecial: helpers.withMessage('Permite letras, números, espacios en blanco y los caracteres: .,)(#/+-&',cNumberLetterSpecial)},
            responsable: {required: cRequired, maxlength: cMaxLength(50), onlyLettersSpecial: cOnlyLettersSpecial},
      
            gradosLatitud: {},
            minutosLatitud: {},
            segundosLatitud: {},
      
            gradosLongitud: {},
            minutosLongitud: {},
            segundosLongitud: {},
      
            limiteVal: {required: cRequired, numberAndDecimals: cNumberAndDecimals(2)},
      
            condicion: {required: cRequired},
            productoNegociacion:{ required: cRequired },
            productoLimite:{ required: cRequired },
            montoDelCupo:{ maxlength: cMaxLength(12), numberAndDecimals: cNumberAndDecimals(2) },
      
            cupo:{ required: cRequired, numeric: cNumeric(true), maxlength: cMaxLength(11) },
      
            minimo:{ required: cRequired },
            maximo:{ required: cRequired },

            aplicarTodos: { required: cRequired },
			cupoVenta: { numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(12)  }
        }
    }
    
}