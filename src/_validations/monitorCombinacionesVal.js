import { cNotSelect, cRequired, cMaxLength, cNumberAndDecimals } from '@/_validations/validacionEspeciales';

export default function(val) {
        if(val == 24){
            return{
                compradorc: { required: cRequired },
                moneda: { required: cRequired },
                rango: { required: cRequired, maxlength: cMaxLength(15), numberAndDecimals: cNumberAndDecimals(2) },
				producto: { required: cRequired },
				tickets: { required: cRequired, numberAndDecimals: cNumberAndDecimals(0), maxlength: cMaxLength(15) },
				porcentajeDesde: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
				porcentajeHasta: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
                }
        }else if(val === 23){
            return{
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                rango: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
                centroApuesta:{required: cRequired, notSelect: cNotSelect('opción', true)},
				producto: { required: cRequired },
				tickets: { required: cRequired, numberAndDecimals: cNumberAndDecimals(0), maxlength: cMaxLength(15) },
				porcentajeDesde: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
				porcentajeHasta: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
            }
        }
        else if(val === 22){
            return{
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                rango: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
                grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
                centroApuesta:{ required: cRequired, notSelect: cNotSelect('opción', true) },
				producto: { required: cRequired },
				tickets: { required: cRequired, numberAndDecimals: cNumberAndDecimals(0), maxlength: cMaxLength(15) },
				porcentajeDesde: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
				porcentajeHasta: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
            }
        }else if(val === 0){
            return{
                moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
                rango: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(15) },
				centroApuesta:{ required: cRequired, notSelect: cNotSelect('opción', true) },
                grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
				producto: { required: cRequired },
				tickets: { required: cRequired, numberAndDecimals: cNumberAndDecimals(0), maxlength: cMaxLength(15) },
				porcentajeDesde: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
				porcentajeHasta: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2), maxlength: cMaxLength(5) },
            }
        }
    }