import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    fechaDesde: { required: cRequired },
    fechaHasta: { required: cRequired },
    moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
    entidad: { required: cRequired, notSelect: cNotSelect('opción', true) },
    comprador: { required: cRequired, notSelect: cNotSelect('opción', true) },
    puntoVenta: { required: cRequired, notSelect: cNotSelect('opción', true) },
    grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
}