
import { cRequired, cMaxLength, cMinLength } from '@/_validations/validacionEspeciales';
import { helpers, sameAs} from '@vuelidate/validators'

export default function (val) {
    return {
        anterior: { required: cRequired, maxlength: cMaxLength(20) },
        password: { required:cRequired, minLength: cMinLength(6), maxLength: cMaxLength(12) },
        password_confirmation: { required: cRequired, maxlength: cMaxLength(12), sameAsPassword: helpers.withMessage('La clave debe coincidir con la nueva', sameAs(val)),  },
        caducidad: { required: cRequired }
    }
}