import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default {
    centro: { required: cRequired, notSelect: cNotSelect('opción', true) },
    taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
    compradorComercial: {required: cRequired}
}