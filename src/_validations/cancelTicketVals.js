import { cNumeric, cRequired, cMaxLength, cMinLength } from '@/_validations/validacionEspeciales';

export default {
    serial: { required: cRequired, numeric: cNumeric(), minLength: cMinLength(12),  maxlength: cMaxLength(12) },
    serialAlterno: { required: cRequired, numeric: cNumeric(), minLength: cMinLength(5), maxlength: cMaxLength(5) },
}