import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales'

export default function (value) {
  if (value === '0' || value === '20') {
    return {
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
      tipoMoneda: {
        required: cRequired,
        notSelect: cNotSelect('opción', true),
      },
      estatus: { required: cRequired, notSelect: cNotSelect('opción', true) },
    }
  } else if (value === '22' || value === '23') {
    return {
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
      tipoMoneda: {
        required: cRequired,
        notSelect: cNotSelect('opción', true),
      },
      estatus: { required: cRequired, notSelect: cNotSelect('opción', true) },
      puntoVenta: {
        required: cRequired,
        notSelect: cNotSelect('opción', true),
      },
    }
  } else if (value === '24') {
    return {
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      taquilla: { required: cRequired, notSelect: cNotSelect('opción', true) },
      tipoMoneda: {
        required: cRequired,
        notSelect: cNotSelect('opción', true),
      },
      estatus: { required: cRequired, notSelect: cNotSelect('opción', true) },
      puntoVenta: {
        required: cRequired,
        notSelect: cNotSelect('opción', true),
      },
      comprador: { required: cRequired, notSelect: cNotSelect('opción', true) },
    }
  }
}
