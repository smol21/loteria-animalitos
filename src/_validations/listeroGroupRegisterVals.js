import { cCredentials, cEmail, cNotSelect, cNumeric, cOnlyLettersSpecial, cRequired } from '@/_validations/validacionEspeciales';

export default {
    dfRif: { required: cRequired, credentials: cCredentials(8) },
    dfName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
    dfAddress: { required: cRequired },
    dfPhone1: { required: cRequired, numeric: cNumeric(true) },
    dfPhone2: { numeric: cNumeric(true) },
    // 
    cName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
    cLastName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
    cIdetificationCard: { required: cRequired, numeric: cNumeric(true) },
    cPhone1: { required: cRequired, numeric: cNumeric(true) },
    cPhone2: { numeric: cNumeric(true) },
    // 
    dgState: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgCity: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgMunicipality: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgApprovalRequired: { required: cRequired },
    dgEmail: { required: cRequired, email: cEmail },
    dgAssociatedType: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgAssociated: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgListerType: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgStatus: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgPaymentFrequency: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgRiskFactor: { required: cRequired, numeric: cNumeric(true) },
    dgTicketCaducity: { required: cRequired, numeric: cNumeric(true) },
    dgValidationCaducity: { required: cRequired, numeric: cNumeric(true) },
    // 
    // enGamblingType: {},
    enEsquemaNegociacion: {},
}