const decorativeFunc = (el, binding) => {
    const nodot = binding.arg && binding.arg === 'nodot';
    const dodot = binding.arg && binding.arg === 'dot';
    const truncLen = binding.value && typeof binding.value == 'object' && binding.value.truncate;
    const bindingValue = truncLen ? binding.value.val : binding.value;
    const resVal = bindingValue ?? 'nullUndef';
    const asNum = Number(typeof bindingValue == 'number' || !dodot ? bindingValue : (`${bindingValue}`.trim().replaceAll('.','').replace(',','.')));
    let numGot = asNum || 0;
    const lwrThan0 = numGot < 0;
    if (lwrThan0) numGot = -numGot;
    const isInt = Number.isInteger(numGot);
    const modifiedTxt = !binding.arg || binding.arg === 'no' ? bindingValue 
        : numGot[isInt ? 'toFixed' : 'toString'](...(isInt ? [2] : []))[nodot ? 'replace' : 'split'](...(nodot ? ['.',','] : ['.'])); 
    if (resVal != 'nullUndef' && dodot) {
        const theresMod1 = !!modifiedTxt[1];
        const modifiedTxt1 = theresMod1 ? (',' + ((+('0.'+modifiedTxt[1].slice(0,4))+'').split('.')[1] || '0').padEnd(2,'0')) : '';
        const dotModify = modifiedTxt[0].split('').reverse().reduce(
            (frmt, n, i, arr) => (arr.length - 1 !== i && !((i + 1) % 3) ? '.' : '') + n + frmt, ''
        ) + modifiedTxt1.slice(...(theresMod1 && truncLen ? [0, truncLen + 1] : [0]));
        el.innerText = (lwrThan0 ? '-' : '') +  dotModify;
    }
    else el.innerText = resVal != 'nullUndef' ? ((lwrThan0 ? '-' : '') + modifiedTxt) : '';
}

export const numberComma = {
    mounted: decorativeFunc,
    updated: decorativeFunc,
}